﻿namespace System
{
    using Xunit;

    public class TimeSpanExtensionsTests
    {
        [Fact]
        public void DefaultFractionsOk()
        {
            Assert.Equal(new TimeSpan(0, 5, 6, 7), new TimeSpan(0, 5, 6, 7, 123).Truncate());
            Assert.Equal(new TimeSpan(4, 5, 6, 7), new TimeSpan(4, 5, 6, 7, 123).Truncate());
            Assert.Equal(new TimeSpan(4, 5, 6, 7), new TimeSpan(4, 5, 6, 7, 999).Truncate());
        }

        [Fact]
        public void ExplicitFractionsOk()
        {
            Assert.Equal(new TimeSpan(4, 5, 6, 7, 100), new TimeSpan(4, 5, 6, 7, 123).Truncate(10));
            Assert.Equal(new TimeSpan(4, 5, 6, 7, 120), new TimeSpan(4, 5, 6, 7, 123).Truncate(100));
            Assert.Equal(new TimeSpan(4, 5, 6, 7, 123), new TimeSpan(4, 5, 6, 7, 123).Truncate(1000));
        }

        [Fact]
        public void NanosecondsTruncatedToo()
        {
            Assert.Equal(TimeSpan.Zero, new TimeSpan(123).Truncate(10));
        }
    }
}

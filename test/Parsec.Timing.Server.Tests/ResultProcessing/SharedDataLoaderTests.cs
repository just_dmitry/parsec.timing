﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class SharedDataLoaderTests
    {
        [Fact]
        public async Task LoadsOnlyOneCompetitionPool()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();
            var id4 = Guid.NewGuid();
            var markerId = Guid.NewGuid();

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.Competitions.Add(new Model.Competition { Id = id3, CompetitionPoolId = id1, Name = id3.ToString() });
                db.Competitions.Add(new Model.Competition { Id = id4, CompetitionPoolId = id2, Name = id4.ToString() });

                db.FinAdjustments.Add(new Model.FinAdjustment { Id = markerId, CompetitionPoolId = id1 });
                db.FinAdjustments.Add(new Model.FinAdjustment { Id = Guid.NewGuid(), CompetitionPoolId = id3 });

                db.Splits.Add(new Model.Split { Id = markerId, CompetitionId = id3, InstantArrivalCheckpointId = Guid.NewGuid() });
                db.Splits.Add(new Model.Split { Id = Guid.NewGuid(), CompetitionId = id3, InstantArrivalCheckpointId = null });
                db.Splits.Add(new Model.Split { Id = Guid.NewGuid(), CompetitionId = id4, InstantArrivalCheckpointId = Guid.NewGuid() });

                await db.SaveChangesAsync();
            }

            var scopeFactoryMock = new Mock<IServiceScopeFactory>(MockBehavior.Strict);
            var scopeMock = new Mock<IServiceScope>(MockBehavior.Strict);

            scopeFactoryMock.Setup(x => x.CreateScope()).Returns(scopeMock.Object);

            SharedData result = null;

            using (var db = new TimingDb(options))
            {
                scopeMock.Setup(x => x.ServiceProvider.GetService(typeof(TimingDb))).Returns(db);
                scopeMock.Setup(x => x.Dispose());

                // do
                result = await SharedDataLoader.LoadAsync(id1, db);
            }

            // verify
            Assert.NotNull(result);

            Assert.Single(result.FinAdjustments);
            Assert.Equal(markerId, result.FinAdjustments[0].Id);

            Assert.Single(result.InstantArrivalSplits);
            Assert.Equal(markerId, result.InstantArrivalSplits[0].Id);
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_GenerateResults
    {
        [Fact]
        public void ItWorks()
        {
            var checkpointId1 = Guid.NewGuid();
            var checkpointId2 = Guid.NewGuid();

            var finGroup1 = new FinGroup { Id = 1, CheckpointId = checkpointId1 };
            var finGroup2 = new FinGroup { Id = 2, CheckpointId = checkpointId2 };

            var lastResult = new Result { ParticipantId = Guid.NewGuid() };

            var marks = new List<FinMark>
            {
                new FinMark { FinGroup = finGroup1, OriginalResult = new TimeOrLength(new TimeSpan(0, 11, 11)) },
                new FinMark { FinGroup = finGroup2, OriginalResult = new TimeOrLength(10000M) },
                new FinMark { FinGroup = finGroup1, OriginalResult = new TimeOrLength(new TimeSpan(0, 44, 44)) },
            };

            var adjustments = new List<FinAdjustment>
            {
                new FinAdjustment
                {
                    CheckpointId = checkpointId1,
                    Mode = FinAdjustmentMode.AssignValue,
                    Value = new TimeOrLength(500M),
                },
                new FinAdjustment
                {
                    CheckpointId = checkpointId1,
                    Mode = FinAdjustmentMode.ApplyAbsoluteAddLengthSetTime,
                },
                new FinAdjustment
                {
                    CheckpointId = checkpointId2,
                    Mode = FinAdjustmentMode.AssignValue,
                    Value = new TimeOrLength(new TimeSpan(0, 50, 50)),
                },
                new FinAdjustment
                {
                    CheckpointId = checkpointId2,
                    Mode = FinAdjustmentMode.ApplyAbsoluteSetBoth,
                },
            };

            var results = FinMarksProcessor.GenerateResults(marks, lastResult, new List<Split>(), adjustments);

            Assert.Equal(3, results.Count);

            var result = results[0];
            Assert.Equal(1, result.Index);
            Assert.Equal(new TimeSpan(0, 11, 11), result.TotalTime);
            Assert.Equal(500, result.TotalLength);

            result = results[1];
            Assert.Equal(2, result.Index);
            Assert.Equal(new TimeSpan(0, 44, 44), result.TotalTime);
            Assert.Equal(1000, result.TotalLength);

            result = results[2];
            Assert.Equal(3, result.Index);
            Assert.Equal(new TimeSpan(0, 50, 50), result.TotalTime);
            Assert.Equal(10000, result.TotalLength);
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Model;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class FinMarksProcessorTests_Process_LikeSevenHills
    {
        /// <summary>
        /// Файл финиша - на единственном чекпоинте.
        /// На него же (чекпоинт) завязан финишный split.
        /// Должно работыть.
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task Default()
        {
            const string bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var pi = new ProcessingItem
            {
                ParticipantId = Guid.NewGuid(),
                IsLocked = false,
                CompetitionPoolId = Guid.NewGuid(),
                Boundary = TimeSpan.Zero,
            };

            var pd = new ParticipantData
            {
                CompetitionId = Guid.NewGuid(),
                Bib = bib,
                Categories = 0,
                LastResults = new List<Result>(),
            };

            var finishFinGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };

            var marks = new List<FinMark>()
            {
                new FinMark { Bib = bib, FinGroup = finishFinGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 45, 12)) },
            };

            var sd = new SharedData()
            {
                CompetitionPoolId = pi.CompetitionPoolId,
                InstantArrivalSplits = new List<Split>()
                {
                    new Split
                    {
                        CompetitionId = pd.CompetitionId,
                        InstantArrivalCheckpointId = finishFinGroup.CheckpointId,
                        Result = new TimeOrLength(10000M),
                    },
                },
                FinAdjustments = new List<FinAdjustment>(),
            };

            using (var db = new TimingDb(options))
            {
                await FinMarksProcessor.Process(pi, pd, marks, sd, db);
            }

            using (var db = new TimingDb(options))
            {
                var results = await db.Results.AsNoTracking()
                    .Where(x => x.ParticipantId == pi.ParticipantId)
                    .OrderBy(x => x.Index)
                    .ToListAsync();

                Assert.Single(results);

                var result = results[0];

                Assert.Equal(1, result.Index);
                Assert.Equal(sd.InstantArrivalSplits[0].Result.Length, result.TotalLength);
                Assert.Equal(marks[0].EffectiveResult.Time, result.TotalTime);

                Assert.Equal(0, result.FullLapsCount);
                Assert.Equal(0, result.FullLapsLength);
                Assert.Equal(TimeSpan.Zero, result.FullLapsTime);
            }
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_PredictResult
    {
        /// <summary>
        /// Применяются необходимые (подходящие) Split-ы
        /// </summary>
        [Fact]
        public void SplitsAreApplied()
        {
            var finGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };

            var mark = new FinMark { FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 0, 45)) };

            var splits = new List<Split>()
            {
                new Split
                {
                    InstantArrivalCheckpointId = finGroup.CheckpointId,
                    Result = new TimeOrLength(400),
                },
            };

            var (newResult, newSplits) = FinMarksProcessor.PredictResult(mark, new Result(), splits, new List<FinAdjustment>());

            Assert.Equal(1, newResult.Index);
            Assert.Equal(400, newResult.TotalLength);
            Assert.Equal(mark.EffectiveResult.Time, newResult.TotalTime);

            Assert.Empty(newSplits);
        }

        /// <summary>
        /// Если не передан <see cref="FinMark"/>, то не должно подать. Надо лишь возващать (null, _any_)
        /// </summary>
        [Fact]
        public void ReturnNullsIfNoFinMark()
        {
            var (newResult, newSplits) = FinMarksProcessor.PredictResult(null, new Result(), new List<Split>(), new List<FinAdjustment>());

            Assert.Null(newResult);
        }

        /// <summary>
        /// Информацию на SPLIT-ах уменьшают при "попадании"
        /// </summary>
        [Fact]
        public void SplitsAreModified()
        {
            var finGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };
            var mark = new FinMark { FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 7, 0)) };

            var splits = new List<Split>()
            {
                new Split
                {
                    InstantArrivalCheckpointId = finGroup.CheckpointId,
                    Result = new TimeOrLength(400),
                },
                new Split
                {
                    InstantArrivalCheckpointId = finGroup.CheckpointId,
                    Result = new TimeOrLength(2000),
                },
            };

            var splitsDecoded = splits.GetEnumerator();
            splitsDecoded.MoveNext();

            var (newResult, newSplits) = FinMarksProcessor.PredictResult(mark, new Result(), splits, new List<FinAdjustment>());

            Assert.Equal(1, newResult.Index);
            Assert.Equal(400, newResult.TotalLength);
            Assert.Equal(mark.EffectiveResult.Time, newResult.TotalTime);

            // Было два, один применили - остался один
            Assert.Single(newSplits);
        }

        [Fact]
        public void AdjustmentsAreApplied()
        {
            var finGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };

            var mark = new FinMark { FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 0, 45)) };

            var splits = new List<Split>()
            {
                new Split
                {
                    InstantArrivalCheckpointId = finGroup.CheckpointId,
                    Result = new TimeOrLength(400),
                },
            };

            var adjustments = new List<FinAdjustment>()
            {
                new FinAdjustment { Mode = FinAdjustmentMode.AssignValue, Value = new TimeOrLength(500) },
                new FinAdjustment { Mode = FinAdjustmentMode.ApplyAbsoluteSetBoth },
            };

            var (newResult, newSplits) = FinMarksProcessor.PredictResult(mark, new Result(), splits, adjustments);

            Assert.NotNull(newResult);
            Assert.Equal(adjustments[0].Value.Length, newResult.TotalLength);
            Assert.Equal(mark.OriginalResult.Time, newResult.TotalTime);
        }

        [Fact]
        public void ReturnNullsOnReject()
        {
            var finGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };

            var mark = new FinMark { FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 0, 45)) };

            var splits = new List<Split>();

            var adjustments = new List<FinAdjustment>()
            {
                new FinAdjustment { Mode = FinAdjustmentMode.Reject },
            };

            var (newResult, newSplits) = FinMarksProcessor.PredictResult(mark, new Result(), splits, adjustments);

            Assert.Null(newResult);
            Assert.Null(newSplits);
        }

        [Fact]
        public void ReturnNullsIfNothingFound()
        {
            var finGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };
            var mark = new FinMark { FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 7, 0)) };

            var (newResult, newSplits) = FinMarksProcessor.PredictResult(mark, new Result(), new List<Split>(), new List<FinAdjustment>());

            Assert.Null(newResult);
            Assert.Null(newSplits);
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class ParticipantDataEraserTests
    {
        [Fact]
        public async Task ItWorks()
        {
            var pid1 = Guid.NewGuid();
            var pid2 = Guid.NewGuid();

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.Results.Add(new Model.Result { Index = 1, ParticipantId = pid1, TotalTimeTicks = TimeSpan.FromMinutes(1).Ticks });
                db.Results.Add(new Model.Result { Index = 2, ParticipantId = pid1, TotalTimeTicks = TimeSpan.FromMinutes(2).Ticks });
                db.Results.Add(new Model.Result { Index = 3, ParticipantId = pid1, TotalTimeTicks = TimeSpan.FromMinutes(3).Ticks });
                db.Results.Add(new Model.Result { Index = 4, ParticipantId = pid1, TotalTimeTicks = TimeSpan.FromMinutes(4).Ticks });

                db.Results.Add(new Model.Result { ParticipantId = pid2, TotalTimeTicks = TimeSpan.FromMinutes(4).Ticks });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { ParticipantId = pid1, Boundary = TimeSpan.FromSeconds((2 * 60) + 30) };
                await ParticipantDataEraser.Erase(pi, db);
            }

            using (var db = new TimingDb(options))
            {
                var list = await db.Results.OrderBy(x => x.TotalTime).ToListAsync();

                Assert.Equal(3, list.Count);
                Assert.Equal(TimeSpan.FromMinutes(1), list[0].TotalTime);
                Assert.Equal(TimeSpan.FromMinutes(2), list[1].TotalTime);
                Assert.Equal(TimeSpan.FromMinutes(4), list[2].TotalTime);
            }
        }
    }
}

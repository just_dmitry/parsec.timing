﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class ParticipantDataLoaderTests
    {
        [Fact]
        public async Task ItWorks()
        {
            var pid = Guid.NewGuid();
            var cid = Guid.NewGuid();

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.Participants.Add(new Model.Participant { Id = pid, Bib = "123", Categories = 17, CompetitionId = cid });

                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 0 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 1 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 2 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 3 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 4 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 5 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 6 });
                db.Results.Add(new Model.Result { ParticipantId = pid, Index = 7 });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pd = await ParticipantDataLoader.Load(pid, db);

                Assert.NotNull(pd);

                Assert.Equal("123", pd.Bib);
                Assert.Equal(17, pd.Categories);
                Assert.Equal(cid, pd.CompetitionId);

                Assert.Equal(3, pd.LastResults.Count); // ParticipantData.LastResultMaxCount
                Assert.Equal(7, pd.LastResults[0].Index);
                Assert.Equal(6, pd.LastResults[1].Index);
                Assert.Equal(5, pd.LastResults[2].Index);
            }
        }
    }
}

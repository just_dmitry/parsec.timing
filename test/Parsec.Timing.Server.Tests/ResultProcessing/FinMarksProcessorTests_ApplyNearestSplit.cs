﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_ApplyNearestSplit
    {
        [Fact]
        public void DoNothingOnEmptyCheckpointId()
        {
            var tal = new TimeAndLength();
            var splits = new List<Split>();

            Assert.False(FinMarksProcessor.ApplyNearestSplit(tal, null, ref splits));
        }

        [Fact]
        public void DoNothingWhenNoSplits()
        {
            var tal = new TimeAndLength();
            var checkpointId = Guid.NewGuid();
            var splits = new List<Split>();

            Assert.False(FinMarksProcessor.ApplyNearestSplit(tal, checkpointId, ref splits));
        }

        [Fact]
        public void DoNothingWhenSplitNotFound()
        {
            var tal = new TimeAndLength();
            var checkpointId = Guid.NewGuid();
            var splits = new List<Split>()
            {
                new Split { InstantArrivalCheckpointId = Guid.NewGuid() },
            };

            Assert.False(FinMarksProcessor.ApplyNearestSplit(tal, checkpointId, ref splits));
        }

        [Fact]
        public void UseFirstMatchingSplit()
        {
            var tal = new TimeAndLength();
            var checkpointId1 = Guid.NewGuid();
            var checkpointId2 = Guid.NewGuid();
            var splits = new List<Split>()
            {
                new Split { InstantArrivalCheckpointId = checkpointId2 },
                new Split { InstantArrivalCheckpointId = checkpointId1, Result = new TimeOrLength(100) },
                new Split { InstantArrivalCheckpointId = checkpointId1 },
            };

            Assert.True(FinMarksProcessor.ApplyNearestSplit(tal, checkpointId1, ref splits));

            Assert.Equal(100, tal.Length);
        }

        [Fact]
        public void RemovesMatchingSplitAndBefore()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();

            var tal = new TimeAndLength();
            var checkpointId1 = Guid.NewGuid();
            var checkpointId2 = Guid.NewGuid();
            var splits = new List<Split>()
            {
                new Split { Id = id1, InstantArrivalCheckpointId = checkpointId2 },
                new Split { Id = id2, InstantArrivalCheckpointId = checkpointId1, Result = new TimeOrLength(100) },
                new Split { Id = id3, InstantArrivalCheckpointId = checkpointId1 },
            };

            Assert.True(FinMarksProcessor.ApplyNearestSplit(tal, checkpointId1, ref splits));

            Assert.Single(splits);
            Assert.Equal(id3, splits[0].Id);
        }
    }
}

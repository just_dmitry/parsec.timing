﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using Parsec.Timing.Model;
    using Xunit;

    public class FinMarksProcessorTests_ApplyAdjustment
    {
        public Result GetBaseResult()
        {
            return new Result
            {
                Index = 1,
                FullLapsCount = 3,
                FullLapsLength = 1200,
                FullLapsTime = new TimeSpan(0, 1, 30),
                TotalLength = 1500,
                TotalTime = new TimeSpan(0, 2, 0),
            };
        }

        [Fact]
        public void Reject()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.Reject };

            Assert.Throws<InvalidOperationException>(() => FinMarksProcessor.ApplyAdjustment(fa, null, null));
        }

        [Fact]
        public void StartNextLap()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.StartNextLap };

            var tal = new TimeAndLength(); // not used

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(baseline.TotalLength, res1.TotalLength);
            Assert.Equal(baseline.TotalTime, res1.TotalTime);
            Assert.Equal(res1.TotalLength, res1.FullLapsLength);
            Assert.Equal(res1.TotalTime, res1.FullLapsTime);
            Assert.Equal(baseline.FullLapsCount + 1, res1.FullLapsCount);
        }

        [Fact]
        public void AssignValue()
        {
            var fa1 = new FinAdjustment { Mode = FinAdjustmentMode.AssignValue, Value = new TimeOrLength(1000) };
            var fa2 = new FinAdjustment { Mode = FinAdjustmentMode.AssignValue, Value = new TimeOrLength(new TimeSpan(3, 2, 1)) };

            var tal1 = new TimeAndLength { Time = new TimeSpan(1, 2, 3), Length = 123.45M };
            var tal2 = new TimeAndLength { Time = new TimeSpan(1, 2, 3), Length = 123.45M };

            FinMarksProcessor.ApplyAdjustment(fa1, tal1, null);
            FinMarksProcessor.ApplyAdjustment(fa2, tal2, null);

            Assert.Equal(fa1.Value.Length, tal1.Length); // from fa1
            Assert.Equal(new TimeSpan(1, 2, 3), tal1.Time); // original

            Assert.Equal(123.45M, tal2.Length); // original
            Assert.Equal(fa2.Value.Time, tal2.Time); // from fa2
        }

        [Fact]
        public void ApplyAbsoluteSetBoth()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyAbsoluteSetBoth };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(tal.Length, res1.TotalLength);
            Assert.Equal(tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }

        [Fact]
        public void ApplyAbsoluteAddLengthSetTime()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyAbsoluteAddLengthSetTime };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(baseline.TotalLength + tal.Length, res1.TotalLength);
            Assert.Equal(tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }

        [Fact]
        public void ApplyAbsoluteAddTimeSetLength()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyAbsoluteAddTimeSetLength };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(tal.Length, res1.TotalLength);
            Assert.Equal(baseline.TotalTime + tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }

        [Fact]
        public void ApplyAbsoluteAddBoth()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyAbsoluteAddBoth };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(baseline.TotalLength + tal.Length, res1.TotalLength);
            Assert.Equal(baseline.TotalTime + tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }

        [Fact]
        public void ApplyLastLapAddLengthSetTime()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyLastLapAddLengthSetTime };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(baseline.FullLapsLength + tal.Length, res1.TotalLength);
            Assert.Equal(tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }

        [Fact]
        public void ApplyLastLapAddTimeSetLength()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyLastLapAddTimeSetLength };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(tal.Length, res1.TotalLength);
            Assert.Equal(baseline.FullLapsTime + tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }

        [Fact]
        public void ApplyLastLapAddBoth()
        {
            var fa = new FinAdjustment { Mode = FinAdjustmentMode.ApplyLastLapAddBoth };

            var tal = new TimeAndLength { Time = new TimeSpan(3, 2, 1), Length = 123.45M };

            var res1 = GetBaseResult();
            var baseline = GetBaseResult();

            FinMarksProcessor.ApplyAdjustment(fa, tal, res1);

            Assert.Equal(baseline.FullLapsLength + tal.Length, res1.TotalLength);
            Assert.Equal(baseline.FullLapsTime + tal.Time, res1.TotalTime);
            Assert.Equal(baseline.FullLapsCount, res1.FullLapsCount);
            Assert.Equal(baseline.FullLapsLength, res1.FullLapsLength);
            Assert.Equal(baseline.FullLapsTime, res1.FullLapsTime);
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_FindNextMarkSkipDuplicates
    {
        [Fact]
        public void FindSomething()
        {
            var marks = new List<FinMark>
            {
                new FinMark { OriginalResult = new TimeOrLength(10) },
                new FinMark { OriginalResult = new TimeOrLength(18) },
                new FinMark { OriginalResult = new TimeOrLength(26) },
            };

            var lastResult = new Result { TotalLength = 14 };

            var mark = FinMarksProcessor.FindNextMarkSkipDuplicates(marks.GetEnumerator(), lastResult);

            // Должны получить третью, так как первые две дубль
            Assert.NotNull(mark);
            Assert.Same(marks[2], mark);
        }

        [Fact]
        public void DoesNotReset()
        {
            var marks = new List<FinMark>
            {
                new FinMark { OriginalResult = new TimeOrLength(10) },
                new FinMark { OriginalResult = new TimeOrLength(20) },
                new FinMark { OriginalResult = new TimeOrLength(30) },
            };

            var lastResult = new Result { TotalLength = 16 };

            var enumerator = marks.GetEnumerator();
            enumerator.MoveNext();

            var mark = FinMarksProcessor.FindNextMarkSkipDuplicates(enumerator, lastResult);

            // Должны получить третью, так как первая была сразу пропущена, а вторая дубль
            Assert.NotNull(mark);
            Assert.Same(marks[2], mark);
        }

        [Fact]
        public void ReturnFirstIfLastResultIsNull()
        {
            var marks = new List<FinMark>
            {
                new FinMark { OriginalResult = new TimeOrLength(10) },
                new FinMark { OriginalResult = new TimeOrLength(20) },
                new FinMark { OriginalResult = new TimeOrLength(30) },
            };

            var mark = FinMarksProcessor.FindNextMarkSkipDuplicates(marks.GetEnumerator(), null);

            // Должны получить первую, так как LastResult == null
            Assert.NotNull(mark);
            Assert.Same(marks[0], mark);
        }

        [Fact]
        public void ReturnNullIfNothingFound()
        {
            var marks = new List<FinMark>
            {
                new FinMark { OriginalResult = new TimeOrLength(10) },
                new FinMark { OriginalResult = new TimeOrLength(18) },
            };

            var lastResult = new Result { TotalLength = 17 };

            var enumerator = marks.GetEnumerator();
            enumerator.MoveNext();

            var mark = FinMarksProcessor.FindNextMarkSkipDuplicates(enumerator, lastResult);

            // Должны ничего не получить - первый FinMark пропущено изначально, второй - дубль
            Assert.Null(mark);
        }

        [Fact]
        public void ReturnNullIfPositionedAfterEnd()
        {
            var marks = new List<FinMark>
            {
                new FinMark { OriginalResult = new TimeOrLength(10) },
                new FinMark { OriginalResult = new TimeOrLength(18) },
            };

            var lastResult = new Result { TotalLength = 17 };

            var enumerator = marks.GetEnumerator();
            enumerator.MoveNext();
            enumerator.MoveNext();

            Assert.False(enumerator.MoveNext());
            Assert.Null(enumerator.Current);

            var mark = FinMarksProcessor.FindNextMarkSkipDuplicates(enumerator, lastResult);

            // Должны ничего не получить
            Assert.Null(mark);
        }
    }
}

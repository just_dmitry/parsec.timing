﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_IsDuplicate
    {
        private readonly TimeSpan timeThreshold = TimeSpan.FromSeconds(10);

        private readonly decimal lengthThreshold = 5;

        [Fact]
        public void CompareTime()
        {
            var time = new TimeSpan(1, 2, 3);
            var mark = new FinMark { OriginalResult = new TimeOrLength(time), RevisedResult = new TimeOrLength(time) };

            var result1 = new Result { TotalTime = time + (timeThreshold / 2) };
            var result2 = new Result { TotalTime = time + timeThreshold };
            var result3 = new Result { TotalTime = time + timeThreshold + timeThreshold };

            Assert.True(FinMarksProcessor.IsDuplicate(mark, result1));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result2));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result3));
        }

        [Fact]
        public void CompareTimeNegative()
        {
            var time = new TimeSpan(1, 2, 3);
            var mark = new FinMark { OriginalResult = new TimeOrLength(time), RevisedResult = new TimeOrLength(time) };

            var result1 = new Result { TotalTime = time - (timeThreshold / 2) };
            var result2 = new Result { TotalTime = time - timeThreshold };
            var result3 = new Result { TotalTime = time - timeThreshold - timeThreshold };

            Assert.True(FinMarksProcessor.IsDuplicate(mark, result1));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result2));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result3));
        }

        [Fact]
        public void CompareLength()
        {
            var length = 123M;
            var mark = new FinMark { OriginalResult = new TimeOrLength(length), RevisedResult = new TimeOrLength(length) };

            var result1 = new Result { TotalLength = length + (lengthThreshold / 2) };
            var result2 = new Result { TotalLength = length + lengthThreshold };
            var result3 = new Result { TotalLength = length + lengthThreshold + lengthThreshold };

            Assert.True(FinMarksProcessor.IsDuplicate(mark, result1));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result2));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result3));
        }

        [Fact]
        public void CompareLengthNegative()
        {
            var length = 123M;
            var mark = new FinMark { OriginalResult = new TimeOrLength(length), RevisedResult = new TimeOrLength(length) };

            var result1 = new Result { TotalLength = length - (lengthThreshold / 2) };
            var result2 = new Result { TotalLength = length - lengthThreshold };
            var result3 = new Result { TotalLength = length - lengthThreshold - lengthThreshold };

            Assert.True(FinMarksProcessor.IsDuplicate(mark, result1));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result2));
            Assert.False(FinMarksProcessor.IsDuplicate(mark, result3));
        }
    }
}

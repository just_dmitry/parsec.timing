﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Model;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class FinMarksProcessorTests_Process
    {
        /// <summary>
        /// При поправке "Reject" отсечка не должна генерировать результат
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task RejectIfRequired()
        {
            const string bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var pi = new ProcessingItem
            {
                ParticipantId = Guid.NewGuid(),
                IsLocked = false,
                CompetitionPoolId = Guid.NewGuid(),
                Boundary = TimeSpan.Zero,
            };

            var pd = new ParticipantData
            {
                CompetitionId = Guid.NewGuid(),
                Bib = bib,
                Categories = 0,
                LastResults = new List<Result>(),
            };

            var finGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };

            var marks = new List<FinMark>()
            {
                new FinMark { Bib = bib, FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 0, 45)) },
                new FinMark { Bib = bib, FinGroup = finGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 1, 30)) },
            };

            var sd = new SharedData()
            {
                CompetitionPoolId = pi.CompetitionPoolId,
                InstantArrivalSplits = new List<Split>(),
                FinAdjustments = new List<FinAdjustment>
                {
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = finGroup.CheckpointId,
                        Mode = FinAdjustmentMode.Reject,
                        NotLaterThanResult = new TimeOrLength(TimeSpan.FromMinutes(1)),
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = finGroup.CheckpointId,
                        Mode = FinAdjustmentMode.AssignValue,
                        Value = new TimeOrLength(1000),
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = finGroup.CheckpointId,
                        Mode = FinAdjustmentMode.ApplyLastLapAddLengthSetTime,
                    },
                },
            };

            using (var db = new TimingDb(options))
            {
                await FinMarksProcessor.Process(pi, pd, marks, sd, db);
            }

            using (var db = new TimingDb(options))
            {
                var results = await db.Results.AsNoTracking()
                    .Where(x => x.ParticipantId == pi.ParticipantId)
                    .OrderBy(x => x.Index)
                    .ToListAsync();

                Assert.Single(results);

                var result = results[0];
                Assert.Equal(1, result.Index);
                Assert.Equal(1000, result.TotalLength);
                Assert.Equal(marks[1].EffectiveResult.Time, result.TotalTime);
            }
        }

        [Fact]
        public async Task MarksCorrectlyOrdered()
        {
            const string bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var pi = new ProcessingItem
            {
                ParticipantId = Guid.NewGuid(),
                IsLocked = false,
                CompetitionPoolId = Guid.NewGuid(),
                Boundary = TimeSpan.Zero,
            };

            var pd = new ParticipantData
            {
                CompetitionId = Guid.NewGuid(),
                Bib = bib,
                Categories = 0,
                LastResults = new List<Result>(),
            };

            var finGroup1 = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };
            var finGroup2 = new FinGroup { Id = 2, CheckpointId = Guid.NewGuid() };

            var marks = new List<FinMark>()
            {
                new FinMark { Bib = bib, FinGroup = finGroup1, OriginalResult = new TimeOrLength(new TimeSpan(0, 0, 45)) },
                new FinMark { Bib = bib, FinGroup = finGroup1, OriginalResult = new TimeOrLength(new TimeSpan(0, 7, 0)) },
                new FinMark { Bib = bib, FinGroup = finGroup2, OriginalResult = new TimeOrLength(1000) },
            };

            var sd = new SharedData()
            {
                CompetitionPoolId = pi.CompetitionPoolId,
                InstantArrivalSplits = new List<Split>()
                {
                    new Split
                    {
                        CompetitionId = pd.CompetitionId,
                        InstantArrivalCheckpointId = finGroup1.CheckpointId,
                        Result = new TimeOrLength(400),
                    },
                    new Split
                    {
                        CompetitionId = pd.CompetitionId,
                        InstantArrivalCheckpointId = finGroup2.CheckpointId,
                        Result = new TimeOrLength(new TimeSpan(0, 3, 15)),
                    },
                    new Split
                    {
                        CompetitionId = pd.CompetitionId,
                        InstantArrivalCheckpointId = finGroup1.CheckpointId,
                        Result = new TimeOrLength(2000),
                    },
                },
                FinAdjustments = new List<FinAdjustment>(),
            };

            using (var db = new TimingDb(options))
            {
                await FinMarksProcessor.Process(pi, pd, marks, sd, db);
            }

            using (var db = new TimingDb(options))
            {
                var results = await db.Results.AsNoTracking()
                    .Where(x => x.ParticipantId == pi.ParticipantId)
                    .OrderBy(x => x.Index)
                    .ToListAsync();

                Assert.Equal(3, results.Count);

                var result = results[0];
                Assert.Equal(1, result.Index);
                Assert.Equal(400, result.TotalLength);
                Assert.Equal(marks[0].EffectiveResult.Time, result.TotalTime);

                result = results[1];
                Assert.Equal(2, result.Index);
                Assert.Equal(1000, result.TotalLength);
                Assert.Equal(sd.InstantArrivalSplits[1].Result.Time, result.TotalTime);

                result = results[2];
                Assert.Equal(3, result.Index);
                Assert.Equal(2000, result.TotalLength);
                Assert.Equal(marks[1].EffectiveResult.Time, result.TotalTime);
            }
        }
    }
}

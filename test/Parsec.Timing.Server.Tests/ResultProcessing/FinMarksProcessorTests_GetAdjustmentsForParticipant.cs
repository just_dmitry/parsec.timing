﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_GetAdjustmentsForParticipant
    {
        [Fact]
        public void FilterByBib()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();

            var adjustments = new List<FinAdjustment>
            {
                new FinAdjustment { Id = id1, BibArray = new[] { "123", "345", "789" } },
                new FinAdjustment { Id = id2, BibArray = new string[0] },
                new FinAdjustment { Id = id3, BibArray = new[] { "789" } }
            };

            var data = new ParticipantData
            {
                Bib = "345",
            };

            var result = FinMarksProcessor.GetAdjustmentsForParticipant(adjustments, data).ToList();

            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.Equal(id1, result[0].Id);
            Assert.Equal(id2, result[1].Id);
        }

        [Fact]
        public void FilterByCompetition()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();

            var adjustments = new List<FinAdjustment>
            {
                new FinAdjustment { Id = id1, CompetitionId = id1, BibArray = new[] { "123", "345", "789" } },
                new FinAdjustment { Id = id2, CompetitionId = id2, BibArray = new[] { "123", "345", "789" } },
                new FinAdjustment { Id = id3, CompetitionId = null, BibArray = new[] { "123", "345", "789" } },
            };

            var data = new ParticipantData
            {
                Bib = "345",
                CompetitionId = id2,
            };

            var result = FinMarksProcessor.GetAdjustmentsForParticipant(adjustments, data).ToList();

            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.Equal(id2, result[0].Id);
            Assert.Equal(id3, result[1].Id);
        }

        [Fact]
        public void FilterByCategory()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();

            var adjustments = new List<FinAdjustment>
            {
                new FinAdjustment { Id = id1, Category = 1, BibArray = new[] { "123", "345", "789" } },
                new FinAdjustment { Id = id2, Category = 4, BibArray = new[] { "123", "345", "789" } },
                new FinAdjustment { Id = id3, Category = 0, BibArray = new[] { "123", "345", "789" } },
            };

            var data = new ParticipantData
            {
                Bib = "345",
                Categories = 6,
            };

            var result = FinMarksProcessor.GetAdjustmentsForParticipant(adjustments, data).ToList();

            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.Equal(id2, result[0].Id);
            Assert.Equal(id3, result[1].Id);
        }
    }
}

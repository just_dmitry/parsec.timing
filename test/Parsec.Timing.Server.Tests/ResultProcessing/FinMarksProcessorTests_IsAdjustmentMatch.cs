﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;
    using Xunit;

    public class FinMarksProcessorTests_IsAdjustmentMatch
    {
        [Fact]
        public void FilterByCheckpoint()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();

            var finGroupCheckpointId = id2;

            var adjustments = new[]
            {
                new FinAdjustment { CheckpointId = id1 },
                new FinAdjustment { CheckpointId = id2 },
                new FinAdjustment { CheckpointId = null },
            };

            var tal = new TimeAndLength();

            Assert.False(FinMarksProcessor.IsAdjustmentMatch(adjustments[0], tal, finGroupCheckpointId));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[1], tal, finGroupCheckpointId));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[2], tal, finGroupCheckpointId));
        }

        [Fact]
        public void FilterByNotEarlierThanTime()
        {
            var adjustments = new[]
            {
                new FinAdjustment { NotEarlierThanResult = new TimeOrLength(TimeSpan.FromMinutes(1)) },
                new FinAdjustment { NotEarlierThanResult = new TimeOrLength(TimeSpan.FromMinutes(3)) },
                new FinAdjustment { NotEarlierThanResult = TimeOrLength.Zero },
            };

            var tal = new TimeAndLength()
            {
                Time = TimeSpan.FromMinutes(2),
            };

            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[0], tal, null));
            Assert.False(FinMarksProcessor.IsAdjustmentMatch(adjustments[1], tal, null));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[2], tal, null));
        }

        [Fact]
        public void FilterByNotLaterThanTime()
        {
            var adjustments = new[]
            {
                new FinAdjustment { NotLaterThanResult = new TimeOrLength(TimeSpan.FromMinutes(1)) },
                new FinAdjustment { NotLaterThanResult = new TimeOrLength(TimeSpan.FromMinutes(3)) },
                new FinAdjustment { NotLaterThanResult = TimeOrLength.Zero },
            };

            var tal = new TimeAndLength()
            {
                Time = TimeSpan.FromMinutes(2),
            };

            Assert.False(FinMarksProcessor.IsAdjustmentMatch(adjustments[0], tal, null));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[1], tal, null));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[2], tal, null));
        }

        [Fact]
        public void FilterByNotEarlierThanLength()
        {
            var adjustments = new List<FinAdjustment>
            {
                new FinAdjustment { NotEarlierThanResult = new TimeOrLength(1) },
                new FinAdjustment { NotEarlierThanResult = new TimeOrLength(3) },
                new FinAdjustment { NotEarlierThanResult = TimeOrLength.Zero },
            };

            var tal = new TimeAndLength()
            {
                Length = 2,
            };

            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[0], tal, null));
            Assert.False(FinMarksProcessor.IsAdjustmentMatch(adjustments[1], tal, null));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[2], tal, null));
        }

        [Fact]
        public void FilterByNotLaterThanLength()
        {
            var adjustments = new List<FinAdjustment>
            {
                new FinAdjustment { NotLaterThanResult = new TimeOrLength(1) },
                new FinAdjustment { NotLaterThanResult = new TimeOrLength(3) },
                new FinAdjustment { NotLaterThanResult = TimeOrLength.Zero },
            };

            var tal = new TimeAndLength()
            {
                Length = 2,
            };

            Assert.False(FinMarksProcessor.IsAdjustmentMatch(adjustments[0], tal, null));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[1], tal, null));
            Assert.True(FinMarksProcessor.IsAdjustmentMatch(adjustments[2], tal, null));
        }
    }
}

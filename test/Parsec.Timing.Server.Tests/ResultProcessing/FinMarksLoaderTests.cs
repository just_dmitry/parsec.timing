﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class FinMarksLoaderTests
    {
        [Fact]
        public async Task DoNotReadDisabedFinGroups()
        {
            var cpid = Guid.NewGuid();
            var bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(new Model.FinGroup { Id = 1, CompetitionPoolId = cpid, IsDisabled = false });
                db.FinGroups.Add(new Model.FinGroup { Id = 2, CompetitionPoolId = cpid, IsDisabled = true });

                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 2,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { CompetitionPoolId = cpid, Boundary = TimeSpan.Zero };
                var pd = new ParticipantData { Bib = bib };

                var marks = await FinMarksLoader.Load(pi, pd, db);

                Assert.NotNull(marks);
                Assert.Single(marks);
                Assert.Equal(1, marks[0].FinGroupId);
                Assert.Equal(TimeSpan.FromMinutes(1), marks[0].RevisedResult.Time);
            }
        }

        [Fact]
        public async Task ReadAllEnabledFinGroups()
        {
            var cpid = Guid.NewGuid();
            var bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(new Model.FinGroup { Id = 1, CompetitionPoolId = cpid, IsDisabled = false });
                db.FinGroups.Add(new Model.FinGroup { Id = 2, CompetitionPoolId = cpid, IsDisabled = false });

                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 2,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { CompetitionPoolId = cpid, Boundary = TimeSpan.Zero };
                var pd = new ParticipantData { Bib = bib };

                var marks = await FinMarksLoader.Load(pi, pd, db);

                Assert.NotNull(marks);
                Assert.Equal(2, marks.Count);
            }
        }

        [Fact]
        public async Task ReadOnlyOneCompetitionPool()
        {
            var cpid = Guid.NewGuid();
            var cpid2 = Guid.NewGuid();
            var bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(new Model.FinGroup { Id = 1, CompetitionPoolId = cpid, IsDisabled = false });
                db.FinGroups.Add(new Model.FinGroup { Id = 2, CompetitionPoolId = cpid2, IsDisabled = false });

                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 2,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { CompetitionPoolId = cpid, Boundary = TimeSpan.Zero };
                var pd = new ParticipantData { Bib = bib };

                var marks = await FinMarksLoader.Load(pi, pd, db);

                Assert.NotNull(marks);
                Assert.Single(marks);
            }
        }

        [Fact]
        public async Task ReadOnlyOneBib()
        {
            var cpid = Guid.NewGuid();
            var bib = "123";
            var bib2 = "1234";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(new Model.FinGroup { Id = 1, CompetitionPoolId = cpid, IsDisabled = false });

                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib2,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { CompetitionPoolId = cpid, Boundary = TimeSpan.Zero };
                var pd = new ParticipantData { Bib = bib };

                var marks = await FinMarksLoader.Load(pi, pd, db);

                Assert.NotNull(marks);
                Assert.Single(marks);
                Assert.Equal(bib, marks[0].Bib);
            }
        }

        [Fact]
        public async Task MarksAreOrdered()
        {
            var cpid = Guid.NewGuid();
            var bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(new Model.FinGroup { Id = 1, CompetitionPoolId = cpid, IsDisabled = false });

                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(3)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(3)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { CompetitionPoolId = cpid, Boundary = TimeSpan.Zero };
                var pd = new ParticipantData { Bib = bib };

                var marks = await FinMarksLoader.Load(pi, pd, db);

                Assert.NotNull(marks);
                Assert.Equal(3, marks.Count);
                Assert.Equal(TimeSpan.FromMinutes(1), marks[0].RevisedResult.Time);
                Assert.Equal(TimeSpan.FromMinutes(2), marks[1].RevisedResult.Time);
                Assert.Equal(TimeSpan.FromMinutes(3), marks[2].RevisedResult.Time);
            }
        }

        [Fact]
        public async Task BoundaryHonoured()
        {
            var cpid = Guid.NewGuid();
            var bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(new Model.FinGroup { Id = 1, CompetitionPoolId = cpid, IsDisabled = false });

                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(1)),
                });
                db.FinMarks.Add(new Model.FinMark
                {
                    FinGroupId = 1,
                    Bib = bib,
                    OriginalResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                    RevisedResult = new Model.TimeOrLength(TimeSpan.FromMinutes(2)),
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var pi = new ProcessingItem { CompetitionPoolId = cpid, Boundary = TimeSpan.FromMinutes(1) };
                var pd = new ParticipantData { Bib = bib };

                var marks = await FinMarksLoader.Load(pi, pd, db);

                Assert.NotNull(marks);
                Assert.Single(marks);
                Assert.Equal(TimeSpan.FromMinutes(2), marks[0].RevisedResult.Time);
            }
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Model;
    using Parsec.Timing.Server.Services;
    using Xunit;

    public class FinMarksProcessorTests_Process_LikeMoscowNight
    {
        /// <summary>
        /// Сначала (до 0:02:00) круги по 228.5 метров, потом по 200м, и в конце добавляем "хвост".
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task Default()
        {
            const string bib = "123";

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var pi = new ProcessingItem
            {
                ParticipantId = Guid.NewGuid(),
                IsLocked = false,
                CompetitionPoolId = Guid.NewGuid(),
                Boundary = TimeSpan.Zero,
            };

            var pd = new ParticipantData
            {
                CompetitionId = Guid.NewGuid(),
                Bib = bib,
                Categories = 0,
                LastResults = new List<Result>(),
            };

            var lapFinGroup = new FinGroup { Id = 1, CheckpointId = Guid.NewGuid() };
            var tailFinGroup = new FinGroup { Id = 2, CheckpointId = Guid.NewGuid() };

            var marks = new List<FinMark>()
            {
                new FinMark { Bib = bib, FinGroup = lapFinGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 0, 45)) },
                new FinMark { Bib = bib, FinGroup = lapFinGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 1, 35)) },
                new FinMark { Bib = bib, FinGroup = lapFinGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 2, 30)) },
                new FinMark { Bib = bib, FinGroup = lapFinGroup, OriginalResult = new TimeOrLength(new TimeSpan(0, 3, 20)) },
                new FinMark { Bib = bib, FinGroup = tailFinGroup, OriginalResult = new TimeOrLength(50.50M) },
            };

            var sd = new SharedData()
            {
                CompetitionPoolId = pi.CompetitionPoolId,
                InstantArrivalSplits = new List<Split>(),
                FinAdjustments = new List<FinAdjustment>
                {
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = lapFinGroup.CheckpointId,
                        Mode = FinAdjustmentMode.AssignValue,
                        Value = new TimeOrLength(228.5M),
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = lapFinGroup.CheckpointId,
                        NotEarlierThanResult = new TimeOrLength(new TimeSpan(0, 2, 0)),
                        Mode = FinAdjustmentMode.AssignValue,
                        Value = new TimeOrLength(200M),
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = lapFinGroup.CheckpointId,
                        Mode = FinAdjustmentMode.ApplyLastLapAddLengthSetTime,
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = lapFinGroup.CheckpointId,
                        Mode = FinAdjustmentMode.StartNextLap,
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = tailFinGroup.CheckpointId,
                        Mode = FinAdjustmentMode.AssignValue,
                        Value = new TimeOrLength(new TimeSpan(6, 0, 0)),
                    },
                    new FinAdjustment
                    {
                        CompetitionPoolId = pi.CompetitionPoolId,
                        CheckpointId = tailFinGroup.CheckpointId,
                        Mode = FinAdjustmentMode.ApplyLastLapAddLengthSetTime,
                    },
                },
            };

            using (var db = new TimingDb(options))
            {
                await FinMarksProcessor.Process(pi, pd, marks, sd, db);
            }

            using (var db = new TimingDb(options))
            {
                var results = await db.Results.AsNoTracking()
                    .Where(x => x.ParticipantId == pi.ParticipantId)
                    .OrderBy(x => x.Index)
                    .ToListAsync();

                Assert.Equal(5, results.Count);

                var result = results[0];
                Assert.Equal(1, result.Index);
                Assert.Equal(228.5M, result.TotalLength);
                Assert.Equal(marks[0].EffectiveResult.Time, result.TotalTime);
                Assert.Equal(1, result.FullLapsCount);
                Assert.Equal(result.TotalLength, result.FullLapsLength);
                Assert.Equal(result.TotalTime, result.FullLapsTime);

                result = results[1];
                Assert.Equal(2, result.Index);
                Assert.Equal(228.5M + 228.5M, result.TotalLength);
                Assert.Equal(marks[1].EffectiveResult.Time, result.TotalTime);
                Assert.Equal(2, result.FullLapsCount);
                Assert.Equal(result.TotalLength, result.FullLapsLength);
                Assert.Equal(result.TotalTime, result.FullLapsTime);

                result = results[2];
                Assert.Equal(3, result.Index);
                Assert.Equal(228.5M + 228.5M + 200M, result.TotalLength);
                Assert.Equal(marks[2].EffectiveResult.Time, result.TotalTime);
                Assert.Equal(3, result.FullLapsCount);
                Assert.Equal(result.TotalLength, result.FullLapsLength);
                Assert.Equal(result.TotalTime, result.FullLapsTime);

                result = results[3];
                Assert.Equal(4, result.Index);
                Assert.Equal(228.5M + 228.5M + 200M + 200M, result.TotalLength);
                Assert.Equal(marks[3].EffectiveResult.Time, result.TotalTime);
                Assert.Equal(4, result.FullLapsCount);
                Assert.Equal(result.TotalLength, result.FullLapsLength);
                Assert.Equal(result.TotalTime, result.FullLapsTime);

                result = results[4];
                Assert.Equal(5, result.Index);
                Assert.Equal(results[3].TotalLength + 50.50M, result.TotalLength);
                Assert.Equal(new TimeSpan(6, 0, 0), result.TotalTime);

                // а эти значения не изменились
                Assert.Equal(results[3].FullLapsCount, result.FullLapsCount);
                Assert.Equal(results[3].FullLapsLength, result.FullLapsLength);
                Assert.Equal(results[3].FullLapsTime, result.FullLapsTime);
            }
        }
    }
}

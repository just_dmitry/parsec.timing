﻿namespace Parsec.Timing.Server.Dto
{
    using System;
    using Xunit;

    public class FinDataTests
    {
        [Fact]
        public void ParseMuravey()
        {
            var sample1 = "3666  0 39 59";
            var sample2 = "7163  1  2  7";
            var sample3 = "      0 35  7";

            Assert.True(FinData.TryParse(sample1, out FinData fd1));
            Assert.Equal("3666", fd1.Bib);
            Assert.Equal(new TimeSpan(0, 39, 59), fd1.Result);

            Assert.True(FinData.TryParse(sample2, out FinData fd2));
            Assert.Equal("7163", fd2.Bib);
            Assert.Equal(new TimeSpan(1, 2, 7), fd2.Result);

            Assert.True(FinData.TryParse(sample3, out FinData fd3));
            Assert.Null(fd3.Bib);
            Assert.Equal(new TimeSpan(0, 35, 7), fd3.Result);
        }

        [Fact]
        public void ParseFinCatch()
        {
            var sample1 = "00:04:55\t36";
            var sample2 = "10:02:54\t33333333";
            var sample3 = "02:04:22\t";

            Assert.True(FinData.TryParse(sample1, out FinData fd1));
            Assert.Equal("36", fd1.Bib);
            Assert.Equal(new TimeSpan(0, 4, 55), fd1.Result);

            Assert.True(FinData.TryParse(sample2, out FinData fd2));
            Assert.Equal("33333333", fd2.Bib);
            Assert.Equal(new TimeSpan(10, 2, 54), fd2.Result);

            Assert.True(FinData.TryParse(sample3, out FinData fd3));
            Assert.Null(fd3.Bib);
            Assert.Equal(new TimeSpan(2, 4, 22), fd3.Result);
        }

        [Theory]
        [InlineData(" 01:04:55 36 // abc")]
        [InlineData("01:04:55 36//abc")]
        [InlineData("01:04:55\t36//abc")]
        [InlineData("01:04:55\t 36//abc")]
        [InlineData(" 36 \t 01:04:55 // abc")]
        [InlineData("36\t01:04:55//abc")]
        [InlineData("36 01:04:55//abc")]
        public void AllThreeParts(string value)
        {
            Assert.True(FinData.TryParse(value, out FinData fd));
            Assert.Equal("36", fd.Bib);
            Assert.Equal(new TimeSpan(1, 4, 55), fd.Result);
            Assert.Equal("abc", fd.Comment);
        }

        [Theory]
        [InlineData("01ж04Ж55 36 // abcжЖ;:.")]
        [InlineData("01;04:55 36 // abcжЖ;:.")]
        public void MistakesAreOk(string value)
        {
            Assert.True(FinData.TryParse(value, out FinData fd));
            Assert.Equal("36", fd.Bib);
            Assert.Equal(new TimeSpan(1, 4, 55), fd.Result);
            Assert.Equal("abcжЖ;:.", fd.Comment);
        }

        [Theory]
        [InlineData("3.01:04:55 36 // abc")]
        [InlineData("36 3.01:04:55 // abc")]
        public void WithDays(string value)
        {
            Assert.True(FinData.TryParse(value, out FinData fd));
            Assert.Equal("36", fd.Bib);
            Assert.Equal(new TimeSpan(3, 1, 4, 55), fd.Result);
            Assert.Equal("abc", fd.Comment);
        }

        [Theory]
        [InlineData("3.01:04:55.6 36")]
        public void WithDaysAndMilliseconds(string value)
        {
            Assert.True(FinData.TryParse(value, out FinData fd));
            Assert.Equal("36", fd.Bib);
            Assert.Equal(new TimeSpan(3, 1, 4, 55, 600), fd.Result);
            Assert.Null(fd.Comment);
        }

        [Theory]
        [InlineData("3.01:04:55.6")]
        public void ResultOnly(string value)
        {
            Assert.True(FinData.TryParse(value, out FinData fd));
            Assert.Null(fd.Bib);
            Assert.Equal(new TimeSpan(3, 1, 4, 55, 600), fd.Result);
            Assert.Null(fd.Comment);
        }

        [Theory]
        [InlineData("3.01:04:55.6 // abc")]
        public void ResultAndComment(string value)
        {
            Assert.True(FinData.TryParse(value, out FinData fd));
            Assert.Null(fd.Bib);
            Assert.Equal(new TimeSpan(3, 1, 4, 55, 600), fd.Result);
            Assert.Equal("abc", fd.Comment);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("// abc")]
        [InlineData("abc")]
        [InlineData("1 2 3 4 5")]
        public void Unparsable(string value)
        {
            Assert.False(FinData.TryParse(value, out FinData fd));
        }
    }
}

﻿namespace Parsec.Timing.Server
{
    using System;
    using System.Reflection;
    using Parsec.Timing.Server.Areas.Something;
    using Xunit;

    public class AreaModelConventionTests
    {
        [Fact]
        public void GetFeatureNameWorks()
        {
            Assert.Null(AreaModelConvention.GetAreaName(typeof(Class1)));
            Assert.Equal("Something", AreaModelConvention.GetAreaName(typeof(Class2)));
        }
    }

#pragma warning disable

    public class Class1
    {
        // Nothing
    }

    namespace Areas.Something
    {
        public class Class2
        {
            // Nothing
        }
    }

#pragma warning restore
}

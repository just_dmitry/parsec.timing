﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Parsec.Timing.Model;
    using Xunit;

    public class CacheServiceTests
    {
        private readonly Guid metId = Guid.NewGuid();

        private readonly Guid cpId = Guid.NewGuid();

        private readonly Mock<ILogger<CacheService>> loggerMock
            = new Mock<ILogger<CacheService>>(MockBehavior.Loose);

        private Mock<IServiceProvider> serviceProviderMock;

        private MemoryCache memoryCache;

        public CacheServiceTests()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            using (var db = new TimingDb(options))
            {
                db.MultiEventTournaments.Add(new MultiEventTournament { Id = metId, Name = metId.ToString() });
                db.CompetitionPools.Add(new CompetitionPool { Id = cpId, Name = cpId.ToString() });
                db.SaveChanges();
            }

            serviceProviderMock = new Mock<IServiceProvider>(MockBehavior.Strict);
            var serviceScopeFactoryMock = new Mock<IServiceScopeFactory>(MockBehavior.Strict);
            var serviceScopeMock = new Mock<IServiceScope>(MockBehavior.Strict);
            serviceScopeMock
                .Setup(x => x.ServiceProvider)
                .Returns(serviceProviderMock.Object);
            serviceScopeMock
                .Setup(x => x.Dispose());
            serviceScopeFactoryMock
                .Setup(x => x.CreateScope())
                .Returns(serviceScopeMock.Object);
            serviceProviderMock
                .Setup(x => x.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactoryMock.Object);
            serviceProviderMock
                .Setup(x => x.GetService(typeof(TimingDb)))
                .Returns(() => new TimingDb(options));

            var memoryOpts = new Mock<Microsoft.Extensions.Options.IOptions<MemoryCacheOptions>>();
            memoryOpts.Setup(x => x.Value).Returns(new MemoryCacheOptions());
            memoryCache = new MemoryCache(memoryOpts.Object);
        }

        [Fact]
        public async Task GetMultiEventTournamentInfo_Works()
        {
            var service = new CacheService(memoryCache, serviceProviderMock.Object, loggerMock.Object);

            // TryGet sould return null
            Assert.False(service.TryGetMultiEventTournamentInfo(metId, out Dto.MultiEventTournamentInfo info1));
            Assert.Null(info1);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Never);

            // GET will call to database
            var info2 = await service.GetMultiEventTournamentInfoAsync(metId);
            Assert.NotNull(info2);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Once);
            serviceProviderMock.ResetCalls();

            // Second GET should do nothing and return cached value
            var info3 = await service.GetMultiEventTournamentInfoAsync(metId);
            Assert.Same(info2, info3);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Never);

            // Should return value (previously cached)
            Assert.True(service.TryGetMultiEventTournamentInfo(metId, out Dto.MultiEventTournamentInfo info4));
            Assert.Same(info2, info4);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Never);
        }

        [Fact]
        public async Task RemoveMultiEventTournamentInfo_Works()
        {
            var service = new CacheService(memoryCache, serviceProviderMock.Object, loggerMock.Object);

            // GET will call to database
            var info1 = await service.GetMultiEventTournamentInfoAsync(metId);
            Assert.NotNull(info1);

            // TryGet sould return same value
            Assert.True(service.TryGetMultiEventTournamentInfo(metId, out Dto.MultiEventTournamentInfo info2));
            Assert.Same(info1, info2);

            // Now remove value from cache
            service.RemoveMultiEventTournamentInfo(metId);

            // TryGet should return null
            Assert.False(service.TryGetMultiEventTournamentInfo(metId, out Dto.MultiEventTournamentInfo info3));
            Assert.Null(info3);
        }

        [Fact]
        public async Task GetCompetitionPoolFinishInfo_Works()
        {
            var service = new CacheService(memoryCache, serviceProviderMock.Object, loggerMock.Object);

            // TryGet sould return null
            Assert.False(service.TryGetCompetitionPoolFinishInfo(cpId, out Dto.CompetitionPoolFinishInfo info1));
            Assert.Null(info1);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Never);

            // GET will call to database
            var info2 = await service.GetCompetitionPoolFinishInfoAsync(cpId);
            Assert.NotNull(info2);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Once);
            serviceProviderMock.ResetCalls();

            // Second GET should do nothing and return cached value
            var info3 = await service.GetCompetitionPoolFinishInfoAsync(cpId);
            Assert.Same(info2, info3);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Never);

            // Should return value (previously cached)
            Assert.True(service.TryGetCompetitionPoolFinishInfo(cpId, out Dto.CompetitionPoolFinishInfo info4));
            Assert.Same(info2, info4);

            serviceProviderMock.Verify(x => x.GetService(typeof(TimingDb)), Times.Never);
        }

        [Fact]
        public async Task RemoveCompetitionPoolFinishInfo_Works()
        {
            var service = new CacheService(memoryCache, serviceProviderMock.Object, loggerMock.Object);

            // GET will call to database
            var info1 = await service.GetCompetitionPoolFinishInfoAsync(cpId);
            Assert.NotNull(info1);

            // TryGet sould return same value
            Assert.True(service.TryGetCompetitionPoolFinishInfo(cpId, out Dto.CompetitionPoolFinishInfo info2));
            Assert.Same(info1, info2);

            // Now remove from cache
            service.RemoveCompetitionPoolFinishInfo(cpId);

            // TryGet should return NULL
            Assert.False(service.TryGetCompetitionPoolFinishInfo(cpId, out Dto.CompetitionPoolFinishInfo info3));
            Assert.Null(info3);
        }

        [Fact]
        public async Task RemovingMultiEventTournament_Also_Removes_CompetitionPools()
        {
            var cpId2 = Guid.NewGuid();

            using (var db = (TimingDb)serviceProviderMock.Object.GetService(typeof(TimingDb)))
            {
                db.CompetitionPools.Add(new CompetitionPool { Id = cpId2, Name = cpId2.ToString() });
                await db.SaveChangesAsync();
            }

            var service = new CacheService(memoryCache, serviceProviderMock.Object, loggerMock.Object);

            var info1 = await service.GetCompetitionPoolFinishInfoAsync(cpId);
            var info2 = await service.GetMultiEventTournamentInfoAsync(metId);
            var info3 = await service.GetCompetitionPoolFinishInfoAsync(cpId2);
            Assert.NotNull(info1);
            Assert.NotNull(info2);
            Assert.NotNull(info3);

            // Now remove from cache
            service.RemoveMultiEventTournamentInfo(metId);

            // TryGet for CompetitionPool should return NULL
            Assert.False(service.TryGetCompetitionPoolFinishInfo(cpId, out Dto.CompetitionPoolFinishInfo info4));
            Assert.Null(info4);
            Assert.False(service.TryGetCompetitionPoolFinishInfo(cpId2, out Dto.CompetitionPoolFinishInfo info5));
            Assert.Null(info5);
        }
    }
}

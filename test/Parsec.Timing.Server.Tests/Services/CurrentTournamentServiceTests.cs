﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;
    using Moq;
    using Xunit;

    public class CurrentTournamentServiceTests
    {
        private readonly Mock<ILogger<CurrentTournamentService>> loggerMock
            = new Mock<ILogger<CurrentTournamentService>>(MockBehavior.Loose);

        private readonly MultiEventTournament met = new MultiEventTournament
        {
            Id = Guid.NewGuid(),
            IsLocked = false,
            Name = "Test tournament"
        };

        [Fact]
        public async Task Set_Works()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();
            var id4 = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                db.MultiEventTournaments.Add(met);
                db.Tournaments.Add(new Tournament { Id = id1, Name = "dummy 1", MultiEventTournamentId = met.Id });
                db.CompetitionPools.Add(new CompetitionPool { Id = id2, Name = "dummy 2", MultiEventTournamentId = met.Id });
                db.Competitions.Add(new Competition { Id = id3, Name = "dummy 3", MultiEventTournamentId = met.Id });
                db.Checkpoints.Add(new Checkpoint { Id = id4, Name = "dummy 4", MultiEventTournamentId = met.Id });
                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var srv = new CurrentTournamentService(loggerMock.Object);
                await srv.SetAsync(met.Id, db);

                Assert.True(srv.IsSet);
                Assert.Equal(met.IsLocked, srv.IsLocked);
                Assert.Equal(met.Id, srv.Id);
                Assert.Equal(met.Name, srv.Title);

                Assert.Equal(1, srv.Tournaments.Count);
                Assert.Equal(id1, srv.Tournaments[0]);

                Assert.Equal(1, srv.CompetitionPools.Count);
                Assert.Equal(id2, srv.CompetitionPools[0]);

                Assert.Equal(1, srv.Competitions.Count);
                Assert.Equal(id3, srv.Competitions[0]);

                Assert.Equal(1, srv.Checkpoints.Count);
                Assert.Equal(id4, srv.Checkpoints[0]);
            }
        }

        [Fact]
        public async Task Reset_Works()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();
            var id4 = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                db.MultiEventTournaments.Add(met);
                db.Tournaments.Add(new Tournament { Id = id1, Name = "dummy 1", MultiEventTournamentId = met.Id });
                db.CompetitionPools.Add(new CompetitionPool { Id = id2, Name = "dummy 2", MultiEventTournamentId = met.Id });
                db.Competitions.Add(new Competition { Id = id3, Name = "dummy 3", MultiEventTournamentId = met.Id });
                db.Checkpoints.Add(new Checkpoint { Id = id4, Name = "dummy 4", MultiEventTournamentId = met.Id });
                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var srv = new CurrentTournamentService(loggerMock.Object);
                await srv.SetAsync(met.Id, db);

                Assert.True(srv.IsSet);

                srv.Reset();
                Assert.False(srv.IsSet);
                Assert.True(srv.IsLocked);
                Assert.Equal(Guid.Empty, srv.Id);
                Assert.False(string.IsNullOrEmpty(srv.Title));

                Assert.Equal(0, srv.Tournaments.Count);
                Assert.Equal(0, srv.CompetitionPools.Count);
                Assert.Equal(0, srv.Competitions.Count);
                Assert.Equal(0, srv.Checkpoints.Count);
            }
        }

        [Fact]
        public void Initially_NotSet()
        {
            var loggerMock = new Mock<ILogger<CurrentTournamentService>>(MockBehavior.Loose);

            var srv = new CurrentTournamentService(loggerMock.Object);

            Assert.False(srv.IsSet);
            Assert.True(srv.IsLocked);
            Assert.Equal(Guid.Empty, srv.Id);
            Assert.False(string.IsNullOrEmpty(srv.Title));

            Assert.Equal(0, srv.Tournaments.Count);
            Assert.Equal(0, srv.CompetitionPools.Count);
            Assert.Equal(0, srv.Competitions.Count);
            Assert.Equal(0, srv.Checkpoints.Count);
        }
    }
}

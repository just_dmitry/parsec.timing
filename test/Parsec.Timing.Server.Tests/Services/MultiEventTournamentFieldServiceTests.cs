﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Xunit;

    public class MultiEventTournamentFieldServiceTests
    {
        [Fact]
        public async Task RemovingFieldKeepItWithoutUsedFlag()
        {
            var metId = Guid.NewGuid();
            var fieldIds = new[] { Guid.NewGuid(), Guid.NewGuid() };

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(MultiEventTournamentFieldServiceTests))
                .Options;

            // prepare db
            using (var db = new TimingDb(options))
            {
                db.MultiEventTournaments.Add(new Model.MultiEventTournament
                {
                    Id = metId,
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now.Date,
                    Name = nameof(RemovingFieldKeepItWithoutUsedFlag)
                });
                db.Fields.Add(new Model.Field { Id = fieldIds[0], Name = "Field 1" });
                db.Fields.Add(new Model.Field { Id = fieldIds[1], Name = "Field 2" });

                db.MultiEventTournamentFields.Add(new Model.MultiEventTournamentField
                {
                    Id = 10,
                    MultiEventTournamentId = metId,
                    FieldId = fieldIds[0],
                    IsUsed = true,
                    SortOrder = 10
                });

                db.MultiEventTournamentFields.Add(new Model.MultiEventTournamentField
                {
                    Id = 11,
                    MultiEventTournamentId = metId,
                    FieldId = fieldIds[1],
                    IsUsed = true,
                    SortOrder = 11
                });

                await db.SaveChangesAsync();
            }

            // test !
            using (var db = new TimingDb(options))
            {
                var ids = new[] { fieldIds[1] };
                var service = new MultiEventTournamentFieldService(db);
                await service.SetAsync(metId, ids);

                // validate
                var list = await db.MultiEventTournamentFields
                    .OrderByDescending(x => x.IsUsed)
                    .ThenBy(x => x.SortOrder)
                    .ToListAsync();

                var item = list[0];
                Assert.Equal(11, item.Id);
                Assert.Equal(fieldIds[1], item.FieldId);
                Assert.Equal(0, item.SortOrder);
                Assert.True(item.IsUsed);

                item = list[1];
                Assert.Equal(10, item.Id);
                Assert.Equal(fieldIds[0], item.FieldId);
                Assert.False(item.IsUsed);
            }
        }

        [Fact]
        public async Task ReAddedFieldKeepsKey()
        {
            var metId = Guid.NewGuid();
            var fieldIds = new[] { Guid.NewGuid(), Guid.NewGuid() };

            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(ReAddedFieldKeepsKey))
                .Options;

            using (var db = new TimingDb(options))
            {
                db.MultiEventTournaments.Add(new Model.MultiEventTournament
                {
                    Id = metId,
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now.Date,
                    Name = nameof(RemovingFieldKeepItWithoutUsedFlag)
                });
                db.Fields.Add(new Model.Field { Id = fieldIds[0], Name = "Field 1" });
                db.Fields.Add(new Model.Field { Id = fieldIds[1], Name = "Field 2" });

                db.MultiEventTournamentFields.Add(new Model.MultiEventTournamentField
                {
                    Id = 17,
                    MultiEventTournamentId = metId,
                    FieldId = fieldIds[0],
                    IsUsed = false,
                    SortOrder = 111
                });

                await db.SaveChangesAsync();
            }

            // test !
            using (var db = new TimingDb(options))
            {
                var ids = new[] { fieldIds[1], fieldIds[0] };
                var service = new MultiEventTournamentFieldService(db);
                await service.SetAsync(metId, ids);

                // validate
                var list = await db.MultiEventTournamentFields
                    .OrderByDescending(x => x.IsUsed)
                    .ThenBy(x => x.SortOrder)
                    .ToListAsync();

                var item = list[0];
                Assert.Equal(fieldIds[1], item.FieldId);
                Assert.Equal(0, item.SortOrder);
                Assert.True(item.IsUsed);
                Assert.NotEqual(0, item.Id); // something was generated

                item = list[1];
                Assert.Equal(fieldIds[0], item.FieldId);
                Assert.Equal(1, item.SortOrder);
                Assert.True(item.IsUsed);
                Assert.Equal(17, item.Id); // unchanged!!!
            }
        }
    }
}

﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Xunit;

    public class StartProcessingJobTests_LoadNormalizedOverridesAsync
    {
        private ILoggerFactory loggerFactory;
        private ILogger<ParticipationProcessingJob> logger;

        public StartProcessingJobTests_LoadNormalizedOverridesAsync()
        {
            loggerFactory = new LoggerFactory();
            loggerFactory.AddConsole();
            logger = loggerFactory.CreateLogger<ParticipationProcessingJob>();
        }

        [Fact]
        public async Task ItWorks()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(ItWorks))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var guid1 = Guid.NewGuid();
            var guid2 = Guid.NewGuid();
            var guid3 = Guid.NewGuid();

            ParticipationProcessingJob.BatchSize = 15;

            using (var db = new TimingDb(options))
            {
                await db.ParticipantOverrides.AddAsync(new Model.ParticipantOverride
                {
                    Id = guid1,
                    CompetitionPoolId = competitionPoolId,
                    TournamentId = Guid.NewGuid(),
                    ParticipationOverride = true,
                });

                await db.ParticipantOverrides.AddAsync(new Model.ParticipantOverride
                {
                    Id = guid2,
                    CompetitionPoolId = competitionPoolId,
                    TournamentId = Guid.NewGuid(),
                    BibArray = new[] { "123" },
                });

                await db.ParticipantOverrides.AddAsync(new Model.ParticipantOverride
                {
                    Id = guid3,
                    CompetitionPoolId = competitionPoolId,
                    TournamentId = Guid.NewGuid(),
                    BibArray = new[] { "456", "789" },
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test name");

                var res = await service.LoadNormalizedOverridesAsync(db);

                Assert.NotNull(res);
                Assert.Equal(4, res.Count);

                Assert.True(res.Exists(x => x.Id == guid1));
                Assert.True(res.Exists(x => x.BibString == "123"));
                Assert.True(res.Exists(x => x.BibString == "456"));
                Assert.True(res.Exists(x => x.BibString == "789"));
            }
        }
    }
}

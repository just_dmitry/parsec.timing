﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using Xunit;

    public class AgeHelperTests
    {
        [Fact]
        public void YearComputationsAreValid()
        {
            // неважно какой день и месяц - считаться должно только по году
            Assert.Equal(5, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), false, 2005, null, null));
            Assert.Equal(5, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), false, 2005, 1, 1));
            Assert.Equal(5, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), false, 2005, 10, 10));
        }

        [Fact]
        public void YearComputationsUnknownOnNegativeAge()
        {
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), false, 2011, null, null));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), false, 2011, 1, 1));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), false, 2011, 10, 10));
        }

        [Fact]
        public void YearComputationsUnknownOnNullBirthYear()
        {
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), false, null, null, null));
        }

        [Fact]
        public void YearComputationsUnknownOnVeryOldAge()
        {
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2005, 2, 23), false, 1900, null, null));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2005, 2, 23), false, 1900, 1, 1));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2005, 2, 23), false, 1900, 10, 10));
        }

        [Fact]
        public void DayComputationsAreValid()
        {
            Assert.Equal(5, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), true, 2005, 1, 27));
            Assert.Equal(5, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), true, 2005, 2, 22));
            Assert.Equal(5, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), true, 2005, 2, 23));
            Assert.Equal(4, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), true, 2005, 2, 24));
            Assert.Equal(4, AgeHelper.ComputeAge(new DateTime(2010, 2, 23), true, 2005, 3, 10));
        }

        [Fact]
        public void DayComputationsUnknownOnNullValues()
        {
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, null, null, null));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 2005, null, null));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, null, 2, null));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, null, null, 20));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 2005, 2, null));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 2005, null, 20));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, null, 2, 20));
        }

        [Fact]
        public void DayComputationsUnknownOnVeryOldAge()
        {
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2005, 2, 23), true, 1900, 10, 10));
        }

        [Fact]
        public void DayComputationsUnknownOnNegativeAge()
        {
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 2001, 1, 10));
            Assert.Equal(AgeHelper.UnknownAge, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 2000, 2, 24));
        }

        [Fact]
        public void ZeroAgeIsPossible()
        {
            Assert.Equal(0, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), false, 2000, null, null));
            Assert.Equal(0, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 2000, 2, 23));
            Assert.Equal(0, AgeHelper.ComputeAge(new DateTime(2000, 2, 23), true, 1999, 2, 24));
        }
    }
}

﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Xunit;

    public class StartProcessingJobTests_CreateParticipantRankingsAsync
    {
        private ILoggerFactory loggerFactory;
        private ILogger<ParticipationProcessingJob> logger;

        public StartProcessingJobTests_CreateParticipantRankingsAsync()
        {
            loggerFactory = new LoggerFactory();
            loggerFactory.AddConsole();
            logger = loggerFactory.CreateLogger<ParticipationProcessingJob>();
        }

        /// <summary>
        /// Issue #3
        /// </summary>
        [Fact]
        public void DefaultOk()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(DefaultOk))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var tournamentId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();

            var tcId = Guid.NewGuid();

            var participantId = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                    },
                    new Model.Participant
                    {
                        Id = participantId,
                        CompetitionId = competitionId,
                        PersonId = personId,
                    },
                    new Model.Person { Id = personId },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "some name");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        new List<Model.ParticipantOverride>(),
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();

                Assert.Single(pr);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId));
            }
        }

        /// <summary>
        /// Issue #3
        /// </summary>
        [Fact]
        public void MultipleTournaments()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(MultipleTournaments))
                .Options;

            var competitionPoolId = Guid.NewGuid();

            var tournamentId1 = Guid.NewGuid();
            var tournamentId2 = Guid.NewGuid();
            var competitionId = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();
            var tcId2 = Guid.NewGuid();

            var participantId1 = Guid.NewGuid();
            var participantId2 = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId1, Name = tournamentId1.ToString() },
                    new Model.Tournament { Id = tournamentId2, Name = tournamentId2.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId1,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                    },
                    new Model.TournamentCompetition
                    {
                        Id = tcId2,
                        TournamentId = tournamentId2,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                    },
                    new Model.Participant
                    {
                        Id = participantId1,
                        CompetitionId = competitionId,
                        PersonId = personId,
                    },
                    new Model.Participant
                    {
                        Id = participantId2,
                        CompetitionId = competitionId,
                        PersonId = personId,
                    },
                    new Model.Person { Id = personId },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        new List<Model.ParticipantOverride>(),
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();

                Assert.Equal(4, pr.Count);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId1 && x.TournamentCompetitionId == tcId1));
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId1 && x.TournamentCompetitionId == tcId2));
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId2 && x.TournamentCompetitionId == tcId1));
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId2 && x.TournamentCompetitionId == tcId2));
            }
        }

        /// <summary>
        /// Issue #4
        /// </summary>
        [Fact]
        public void MultipleTournamentsWithOverrides()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(MultipleTournamentsWithOverrides))
                .Options;

            var competitionPoolId = Guid.NewGuid();

            var tournamentId1 = Guid.NewGuid();
            var tournamentId2 = Guid.NewGuid();
            var competitionId = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();
            var tcId2 = Guid.NewGuid();

            var participantId1 = Guid.NewGuid();
            var participantId2 = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId1, Name = tournamentId1.ToString() },
                    new Model.Tournament { Id = tournamentId2, Name = tournamentId2.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId1,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                    },
                    new Model.TournamentCompetition
                    {
                        Id = tcId2,
                        TournamentId = tournamentId2,
                        CompetitionId = competitionId,
                        AutoParticipation = false,
                    },
                    new Model.Participant
                    {
                        Id = participantId1,
                        CompetitionId = competitionId,
                        PersonId = personId,
                        Bib = "1",
                    },
                    new Model.Participant
                    {
                        Id = participantId2,
                        CompetitionId = competitionId,
                        PersonId = personId,
                        Bib = "2",
                    },
                    new Model.Person { Id = personId },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var normalizedOverrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                    {
                        CompetitionPoolId = competitionPoolId,
                        TournamentId = tournamentId1,
                        BibString = "1",
                        ParticipationOverride = true,
                    },
                    new Model.ParticipantOverride
                    {
                        CompetitionPoolId = competitionPoolId,
                        TournamentId = tournamentId2,
                        BibString = "1",
                        ParticipationOverride = true,
                    }
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        normalizedOverrides,
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();

                Assert.Equal(2, pr.Count);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId2 && x.TournamentCompetitionId == tcId1));
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId1 && x.TournamentCompetitionId == tcId2));
            }
        }

        /// <summary>
        /// Issue #5
        /// </summary>
        [Fact]
        public void TournamentWithOverridesByGender()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(TournamentWithOverridesByGender))
                .Options;

            var competitionPoolId = Guid.NewGuid();

            var tournamentId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();

            var participantId1 = Guid.NewGuid();
            var participantId2 = Guid.NewGuid();
            var personId1 = Guid.NewGuid();
            var personId2 = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                    },
                    new Model.Participant
                    {
                        Id = participantId1,
                        CompetitionId = competitionId,
                        PersonId = personId1,
                        Bib = "1",
                    },
                    new Model.Participant
                    {
                        Id = participantId2,
                        CompetitionId = competitionId,
                        PersonId = personId2,
                        Bib = "2",
                    },
                    new Model.Person { Id = personId1, Gender = Model.Gender.Male },
                    new Model.Person { Id = personId2, Gender = Model.Gender.Female },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var normalizedOverrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                    {
                        CompetitionPoolId = competitionPoolId,
                        TournamentId = tournamentId,
                        Gender = Model.Gender.Male,
                        ParticipationOverride = true,
                    },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        normalizedOverrides,
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();

                Assert.Single(pr);

                // Номер 1 отсутствует, так как попал под коррекцию
                Assert.Null(pr.FirstOrDefault(x => x.ParticipantId == participantId1 && x.TournamentCompetitionId == tcId1));

                // Номер 2 присутствует - пол "не совпал"
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId2 && x.TournamentCompetitionId == tcId1));
            }
        }

        /// <summary>
        /// Issue #6
        /// </summary>
        [Fact]
        public void TournamentWithOverridesByCompetition()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(TournamentWithOverridesByCompetition))
                .Options;

            var competitionPoolId = Guid.NewGuid();

            var tournamentId = Guid.NewGuid();
            var competitionId1 = Guid.NewGuid();
            var competitionId2 = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();
            var tcId2 = Guid.NewGuid();

            var participantId1 = Guid.NewGuid();
            var participantId2 = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId1, CompetitionPoolId = competitionPoolId, Name = competitionId1.ToString() },
                    new Model.Competition { Id = competitionId2, CompetitionPoolId = competitionPoolId, Name = competitionId2.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId1,
                        AutoParticipation = true,
                    },
                    new Model.TournamentCompetition
                    {
                        Id = tcId2,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId2,
                        AutoParticipation = true,
                    },
                    new Model.Participant
                    {
                        Id = participantId1,
                        CompetitionId = competitionId1,
                        PersonId = personId,
                        Bib = "1",
                    },
                    new Model.Participant
                    {
                        Id = participantId2,
                        CompetitionId = competitionId2,
                        PersonId = personId,
                        Bib = "2",
                    },
                    new Model.Person { Id = personId },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var normalizedOverrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                    {
                        CompetitionPoolId = competitionPoolId,
                        CompetitionId = competitionId1,
                        ParticipationOverride = true,
                    },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        normalizedOverrides,
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();
                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.Last(),
                        new int[0],
                        normalizedOverrides,
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();

                Assert.Single(pr);

                // В Competition1 никого, так как 1 отсутствует (попал под коррекцию)
                Assert.Null(pr.FirstOrDefault(x => x.ParticipantId == participantId1 && x.TournamentCompetitionId == tcId1));

                // В Competition2 присутствует №2
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId2 && x.TournamentCompetitionId == tcId2));
            }
        }

        /// <summary>
        /// Issue #7
        /// </summary>
        [Fact]
        public void TournamentWithOverridesByCategory()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(TournamentWithOverridesByCategory))
                .Options;

            var metId = Guid.NewGuid();
            var competitionPoolId = Guid.NewGuid();

            var tournamentId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();

            var participantId1 = Guid.NewGuid();
            var participantId2 = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test", MultiEventTournamentId = metId },
                    new Model.Tournament { Id = tournamentId, MultiEventTournamentId = metId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString(), MultiEventTournamentId = metId },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                    },
                    new Model.Participant
                    {
                        Id = participantId1,
                        CompetitionId = competitionId,
                        PersonId = personId,
                        Categories = 1,
                        Bib = "1",
                    },
                    new Model.Participant
                    {
                        Id = participantId2,
                        CompetitionId = competitionId,
                        PersonId = personId,
                        Categories = 2,
                        Bib = "2",
                    },
                    new Model.Person { Id = personId },
                    new Model.CategoryFlag { Id = 1, MultiEventTournamentId = metId },
                    new Model.CategoryFlag { Id = 2, MultiEventTournamentId = metId },
                    new Model.CategoryFlag { Id = 4, MultiEventTournamentId = metId },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var normalizedOverrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                    {
                        CompetitionPoolId = competitionPoolId,
                        Category = 2,
                        ParticipationOverride = true,
                    },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        db.CategoryFlags.Select(x => x.Id).ToArray(),
                        normalizedOverrides,
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();

                Assert.Single(pr);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId1 && x.TournamentCompetitionId == tcId1));
                Assert.Null(pr.FirstOrDefault(x => x.ParticipantId == participantId2 && x.TournamentCompetitionId == tcId1));
            }
        }

        /// <summary>
        /// Issue #3
        /// </summary>
        [Fact]
        public void DefaultWithAgeOk()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(DefaultWithAgeOk))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var tournamentId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();
            var ageGroupSetId = Guid.NewGuid();
            var ageGroupId1 = Guid.NewGuid();
            var ageGroupId2 = Guid.NewGuid();
            var ageGroupId3 = Guid.NewGuid();

            var tcId = Guid.NewGuid();

            var participantId = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                        AgeGroupSetId = ageGroupSetId,
                        AgeGroupReferenceDate = new DateTime(2000, 4, 1)
                    },
                    new Model.Participant
                    {
                        Id = participantId,
                        CompetitionId = competitionId,
                        PersonId = personId,
                    },
                    new Model.Person { Id = personId, BirthYear = 1950 },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var ageGroupContents = new List<Model.AgeGroupSetContent>
            {
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId1,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId1, MinAge = 49, MaxAge = 49 },
                },
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId2,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId2, MinAge = 50, MaxAge = 50 },
                },
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId3,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId3, MinAge = 51, MaxAge = 51 },
                },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        new List<Model.ParticipantOverride>(),
                        ageGroupContents,
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();
                var par = db.ParticipantAgeRankings.ToList();

                Assert.Single(pr);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId && x.Age == 50));

                Assert.Single(par);
                Assert.NotNull(par.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId && x.AgeGroupId == ageGroupId2));
            }
        }

        [Fact]
        public void AgeOverridesOk()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(AgeOverridesOk))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var tournamentId1 = Guid.NewGuid();
            var tournamentId2 = Guid.NewGuid();
            var competitionId = Guid.NewGuid();
            var ageGroupSetId = Guid.NewGuid();
            var ageGroupId1 = Guid.NewGuid();
            var ageGroupId2 = Guid.NewGuid();
            var ageGroupId3 = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();
            var tcId2 = Guid.NewGuid();

            var participantId = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId1, Name = tournamentId1.ToString() },
                    new Model.Tournament { Id = tournamentId2, Name = tournamentId2.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId1,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                        AgeGroupSetId = ageGroupSetId,
                        AgeGroupReferenceDate = new DateTime(2000, 4, 1),
                    },
                    new Model.TournamentCompetition
                    {
                        Id = tcId2,
                        TournamentId = tournamentId2,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                        AgeGroupSetId = ageGroupSetId,
                        AgeGroupReferenceDate = new DateTime(2000, 4, 1),
                    },
                    new Model.Participant
                    {
                        Id = participantId,
                        CompetitionId = competitionId,
                        PersonId = personId,
                        Bib = "1",
                    },
                    new Model.Person { Id = personId, BirthYear = 1950 },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var ageGroupContents = new List<Model.AgeGroupSetContent>
            {
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId1,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId1, MinAge = 49, MaxAge = 49 },
                },
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId2,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId2, MinAge = 50, MaxAge = 50 },
                },
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId3,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId3, MinAge = 51, MaxAge = 51 },
                },
            };

            var normalizedOverrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                    {
                        CompetitionPoolId = competitionPoolId,
                        TournamentId = tournamentId2,
                        BibString = "1",
                        AgeOverride = 51,
                    },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        normalizedOverrides,
                        ageGroupContents,
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();
                var par = db.ParticipantAgeRankings.ToList();

                Assert.Equal(2, pr.Count);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId1 && x.Age == 50));
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId2 && x.Age == 51));

                Assert.Equal(2, par.Count);
                Assert.NotNull(par.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId1 && x.AgeGroupId == ageGroupId2));
                Assert.NotNull(par.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId2 && x.AgeGroupId == ageGroupId3));
            }
        }

        [Fact]
        public void NoExceptionIfNoAgeGroup()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(NoExceptionIfNoAgeGroup))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var tournamentId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();
            var ageGroupSetId = Guid.NewGuid();
            var ageGroupId1 = Guid.NewGuid();
            var ageGroupId2 = Guid.NewGuid();
            var ageGroupId3 = Guid.NewGuid();

            var tcId = Guid.NewGuid();

            var participantId = Guid.NewGuid();
            var personId = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                        AgeGroupSetId = ageGroupSetId,
                        AgeGroupReferenceDate = new DateTime(2000, 4, 1),
                    },
                    new Model.Participant
                    {
                        Id = participantId,
                        CompetitionId = competitionId,
                        PersonId = personId,
                    },
                    new Model.Person { Id = personId, BirthYear = 1950 },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var ageGroupContents = new List<Model.AgeGroupSetContent>
            {
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId1,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId1, MinAge = 49, MaxAge = 49 },
                },
                //// SURPRISE! No such group! "Hole"!
                ////new Model.AgeGroupSetContent
                ////{
                ////    AgeGroupSetId = ageGroupSetId,
                ////    AgeGroupId = ageGroupId2,
                ////    AgeGroup = new Model.AgeGroup { Id = ageGroupId2, MinAge = 50, MaxAge = 50 },
                ////},
                new Model.AgeGroupSetContent
                {
                    AgeGroupSetId = ageGroupSetId,
                    AgeGroupId = ageGroupId3,
                    AgeGroup = new Model.AgeGroup { Id = ageGroupId3, MinAge = 51, MaxAge = 51 },
                },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        new List<Model.ParticipantOverride>(),
                        ageGroupContents,
                        logger)
                    .Wait();

                var pr = db.ParticipantRankings.ToList();
                var par = db.ParticipantAgeRankings.ToList();

                Assert.Single(pr);
                Assert.NotNull(pr.FirstOrDefault(x => x.ParticipantId == participantId && x.TournamentCompetitionId == tcId && x.Age == 50));

                Assert.Empty(par);
            }
        }

        /// <summary>
        /// Issue #9
        /// </summary>
        [Fact]
        public void UnusedOverridesReturned()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(UnusedOverridesReturned))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var tournamentId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();

            var tcId1 = Guid.NewGuid();

            var participantId = Guid.NewGuid();
            var personId = Guid.NewGuid();

            var overrideId1 = Guid.NewGuid();
            var overrideId2 = Guid.NewGuid();

            using (var db = new TimingDb(options))
            {
                var data = new object[]
                {
                    new Model.CompetitionPool { Id = competitionPoolId, Name = "Test" },
                    new Model.Tournament { Id = tournamentId, Name = tournamentId.ToString() },
                    new Model.Competition { Id = competitionId, CompetitionPoolId = competitionPoolId, Name = competitionId.ToString() },
                    new Model.TournamentCompetition
                    {
                        Id = tcId1,
                        TournamentId = tournamentId,
                        CompetitionId = competitionId,
                        AutoParticipation = true,
                        AgeGroupReferenceDate = new DateTime(2000, 4, 1),
                    },
                    new Model.Participant
                    {
                        Id = participantId,
                        CompetitionId = competitionId,
                        PersonId = personId,
                        Bib = "1",
                    },
                    new Model.Person { Id = personId, BirthYear = 1950 },
                };

                foreach (var item in data)
                {
                    db.Entry(item).State = EntityState.Added;
                }

                db.SaveChanges();
            }

            var normalizedOverrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                    {
                        Id = overrideId1,
                        CompetitionPoolId = competitionPoolId,
                        TournamentId = tournamentId,
                        BibString = "1",
                        ParticipationOverride = true,
                    },
                new Model.ParticipantOverride
                    {
                        Id = overrideId2,
                        CompetitionPoolId = competitionPoolId,
                        TournamentId = tournamentId,
                        BibString = "111", // not exist in Participant!
                        AgeOverride = 51,
                    },
            };

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test");

                var unused = service
                    .CreateParticipantRankingsAsync(
                        db,
                        db.Competitions.First(),
                        new int[0],
                        normalizedOverrides,
                        new List<Model.AgeGroupSetContent>(),
                        logger)
                    .Result;

                Assert.NotNull(unused);
                Assert.Single(unused);
                Assert.Contains(overrideId1, unused);
            }
        }
    }
}

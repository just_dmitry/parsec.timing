﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Xunit;

    public class StartProcessingJobTests_ReportUnusedOverrides
    {
        private ILoggerFactory loggerFactory;
        private ILogger<ParticipationProcessingJob> logger;

        public StartProcessingJobTests_ReportUnusedOverrides()
        {
            loggerFactory = new LoggerFactory();
            loggerFactory.AddConsole();
            logger = loggerFactory.CreateLogger<ParticipationProcessingJob>();
        }

        /// <summary>
        /// Issue #9
        /// </summary>
        [Fact]
        public async Task ItWorks()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(ItWorks))
                .Options;

            var competitionPoolId = Guid.NewGuid();

            var guid1 = Guid.NewGuid();
            var guid2 = Guid.NewGuid();
            var guid3 = Guid.NewGuid();

            ParticipationProcessingJob.BatchSize = 15;

            var overrides = new List<Model.ParticipantOverride>
            {
                new Model.ParticipantOverride
                {
                    Id = guid1,
                    BibString = "1",
                },
                new Model.ParticipantOverride
                {
                    Id = guid2,
                    BibString = "2",
                },
                new Model.ParticipantOverride
                {
                    Id = guid3,
                },
            };

            var used = new HashSet<Guid>();
            used.Add(guid1);

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test name");

                var res = await service.ReportUnusedOverrides(db, overrides, used, logger);

                var created = await db.CompetitionPoolErrors.ToListAsync();

                Assert.Equal(1, res);
                Assert.Single(created);
                Assert.Equal("2", created[0].Bib);
            }
        }
    }
}

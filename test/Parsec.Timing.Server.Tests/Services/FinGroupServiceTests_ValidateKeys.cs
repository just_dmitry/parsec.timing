﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Model;
    using Moq;
    using Xunit;

    public class FinGroupServiceTests_ValidateKeys
    {
        private readonly Mock<ICurrentTournamentService> currentTournamentServiceMock
            = new Mock<ICurrentTournamentService>(MockBehavior.Strict);

        private readonly Mock<ILogger<FinGroupService>> loggerMock
            = new Mock<ILogger<FinGroupService>>(MockBehavior.Loose);

        private readonly Guid metId = Guid.NewGuid();
        private readonly Guid cpId = Guid.NewGuid();
        private readonly Guid cId = Guid.NewGuid();

        [Fact]
        public void Return_False_On_Null()
        {
            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            Assert.False(service.ValidateKeys(null));
        }

        [Fact]
        public void Return_False_Without_CurrentMET()
        {
            currentTournamentServiceMock.Setup(x => x.IsSet).Returns(false);

            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            var fg = new FinGroup
            {
                CompetitionPoolId = null,
                CheckpointId = null,
            };

            Assert.False(service.ValidateKeys(fg));
        }

        [Fact]
        public void Return_True_WhenBothMatches()
        {
            currentTournamentServiceMock.Setup(x => x.IsSet).Returns(true);
            currentTournamentServiceMock.Setup(x => x.Id).Returns(metId);
            currentTournamentServiceMock.Setup(x => x.Checkpoints).Returns(new[] { cId });
            currentTournamentServiceMock.Setup(x => x.CompetitionPools).Returns(new[] { cpId });

            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            var fg = new FinGroup
            {
                CompetitionPoolId = cpId,
                CheckpointId = cId,
            };

            Assert.True(service.ValidateKeys(fg));
        }

        [Fact]
        public void Return_False_WhenAnyMismatch()
        {
            currentTournamentServiceMock.Setup(x => x.IsSet).Returns(true);
            currentTournamentServiceMock.Setup(x => x.Id).Returns(metId);
            currentTournamentServiceMock.Setup(x => x.Checkpoints).Returns(new[] { cId });
            currentTournamentServiceMock.Setup(x => x.CompetitionPools).Returns(new[] { cpId });

            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            var fg1 = new FinGroup
            {
                CompetitionPoolId = cpId,
                CheckpointId = Guid.NewGuid(),
            };

            Assert.False(service.ValidateKeys(fg1));

            var fg2 = new FinGroup
            {
                CompetitionPoolId = Guid.NewGuid(),
                CheckpointId = cId,
            };

            Assert.False(service.ValidateKeys(fg2));
        }

        [Fact]
        public void Return_True_WhenAnyNull()
        {
            currentTournamentServiceMock.Setup(x => x.IsSet).Returns(true);
            currentTournamentServiceMock.Setup(x => x.Id).Returns(metId);
            currentTournamentServiceMock.Setup(x => x.Checkpoints).Returns(new[] { cId });
            currentTournamentServiceMock.Setup(x => x.CompetitionPools).Returns(new[] { cpId });

            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            var fg1 = new FinGroup
            {
                CompetitionPoolId = cpId,
                CheckpointId = null,
            };

            Assert.True(service.ValidateKeys(fg1));

            var fg2 = new FinGroup
            {
                CompetitionPoolId = null,
                CheckpointId = cId,
            };

            Assert.True(service.ValidateKeys(fg2));
        }
    }
}

﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Xunit;

    public class StartProcessingJobTests_ClearParticipantsAsync
    {
        private ILoggerFactory loggerFactory;
        private ILogger<ParticipationProcessingJob> logger;

        public StartProcessingJobTests_ClearParticipantsAsync()
        {
            loggerFactory = new LoggerFactory();
            loggerFactory.AddConsole();
            logger = loggerFactory.CreateLogger<ParticipationProcessingJob>();
        }

        [Fact]
        public async Task DataAreRemoved()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(DataAreRemoved))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();
            var tournamentCompetitionId = Guid.NewGuid();
            var participantId = Guid.NewGuid();

            ParticipationProcessingJob.BatchSize = 15;

            using (var db = new TimingDb(options))
            {
                await db.CompetitionPools.AddAsync(new Model.CompetitionPool
                {
                    Id = competitionPoolId,
                    Name = "test",
                });

                await db.Competitions.AddAsync(new Model.Competition
                {
                    Id = competitionId,
                    CompetitionPoolId = competitionPoolId,
                    Name = "some name",
                });

                await db.TournamentCompetitions.AddAsync(new Model.TournamentCompetition
                {
                    Id = tournamentCompetitionId,
                    CompetitionId = competitionId,
                });

                await db.ParticipantRankings.AddAsync(new Model.ParticipantRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                });

                await db.ParticipantAgeRankings.AddRangeAsync(Enumerable.Range(1, 20).Select(_ => new Model.ParticipantAgeRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                    AgeGroupId = Guid.NewGuid(),
                }));

                await db.ParticipantSplitRankings.AddAsync(new Model.ParticipantSplitRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                    SplitId = Guid.NewGuid(),
                });

                await db.ParticipantSplitAgeRankings.AddAsync(new Model.ParticipantSplitAgeRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                    SplitId = Guid.NewGuid(),
                    AgeGroupId = Guid.NewGuid(),
                });

                await db.CompetitionPoolErrors.AddAsync(new Model.CompetitionPoolError
                {
                    CompetitionPoolId = competitionPoolId,
                    Type = Model.ProcessingErrorType.ParticipantOverrideBibNotFound,
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(competitionPoolId, "Test name");

                await service.ClearParticipantsAsync(db, logger);

                Assert.Empty(db.ParticipantRankings);
                Assert.Empty(db.ParticipantAgeRankings);
                Assert.Empty(db.ParticipantSplitRankings);
                Assert.Empty(db.ParticipantSplitAgeRankings);
                Assert.Empty(db.CompetitionPoolErrors);
            }
        }

        [Fact]
        public async Task OtherDataNotRemoved()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: nameof(OtherDataNotRemoved))
                .Options;

            var competitionPoolId = Guid.NewGuid();
            var otherCompetitionPoolId = Guid.NewGuid();
            var competitionId = Guid.NewGuid();
            var tournamentCompetitionId = Guid.NewGuid();
            var participantId = Guid.NewGuid();

            ParticipationProcessingJob.BatchSize = 15;

            using (var db = new TimingDb(options))
            {
                await db.CompetitionPools.AddAsync(new Model.CompetitionPool
                {
                    Id = competitionPoolId,
                    Name = "test",
                });

                await db.CompetitionPools.AddAsync(new Model.CompetitionPool
                {
                    Id = otherCompetitionPoolId,
                    Name = "test 2",
                });

                await db.Competitions.AddAsync(new Model.Competition
                {
                    Id = competitionId,
                    CompetitionPoolId = competitionPoolId,
                    Name = "some name",
                });

                await db.TournamentCompetitions.AddAsync(new Model.TournamentCompetition
                {
                    Id = tournamentCompetitionId,
                    CompetitionId = competitionId,
                });

                await db.ParticipantRankings.AddAsync(new Model.ParticipantRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                });

                await db.ParticipantAgeRankings.AddRangeAsync(Enumerable.Range(1, 20).Select(_ => new Model.ParticipantAgeRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                    AgeGroupId = Guid.NewGuid(),
                }));

                await db.ParticipantSplitRankings.AddAsync(new Model.ParticipantSplitRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                    SplitId = Guid.NewGuid(),
                });

                await db.ParticipantSplitAgeRankings.AddAsync(new Model.ParticipantSplitAgeRanking
                {
                    ParticipantId = participantId,
                    TournamentCompetitionId = tournamentCompetitionId,
                    SplitId = Guid.NewGuid(),
                    AgeGroupId = Guid.NewGuid(),
                });

                await db.CompetitionPoolErrors.AddAsync(new Model.CompetitionPoolError
                {
                    CompetitionPoolId = competitionPoolId,
                    Type = Model.ProcessingErrorType.ParticipantOverrideBibNotFound,
                });

                await db.SaveChangesAsync();
            }

            using (var db = new TimingDb(options))
            {
                var service = ParticipationProcessingJob.Create(otherCompetitionPoolId, "Test name 2");

                await service.ClearParticipantsAsync(db, logger);

                Assert.NotEmpty(db.ParticipantRankings);
                Assert.NotEmpty(db.ParticipantAgeRankings);
                Assert.NotEmpty(db.ParticipantSplitRankings);
                Assert.NotEmpty(db.ParticipantSplitAgeRankings);
                Assert.NotEmpty(db.CompetitionPoolErrors);
            }
        }
    }
}

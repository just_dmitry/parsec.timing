﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;
    using Moq;
    using Xunit;

    public class FinGroupServiceTests_UpdateMarksRevisedResult
    {
        private readonly Mock<ICurrentTournamentService> currentTournamentServiceMock
            = new Mock<ICurrentTournamentService>(MockBehavior.Strict);

        private readonly Mock<ILogger<FinGroupService>> loggerMock
            = new Mock<ILogger<FinGroupService>>(MockBehavior.Loose);

        [Fact]
        public async Task ItWorks()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var g1 = new FinGroup { Id = 1, ResultShift = new TimeOrLength(TimeSpan.FromMinutes(1)) };
            var count = 10;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(g1);
                for (var i = 0; i < count; i++)
                {
                    db.FinMarks.Add(new FinMark
                    {
                        FinGroupId = g1.Id,
                        OriginalResult = new TimeOrLength(TimeSpan.FromMinutes(i)),
                    });
                }

                await db.SaveChangesAsync();
            }

            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            using (var db = new TimingDb(options))
            {
                var updated = await service.UpdateMarksRevisedResult(g1, db);

                Assert.Equal(count, updated);

                foreach (var item in await db.FinMarks.ToListAsync())
                {
                    Assert.Equal(item.OriginalResult.Time.Add(g1.ResultShift.Time), item.RevisedResult.Time);
                }
            }
        }

        [Fact]
        public async Task ResultTypeHonored()
        {
            var options = new DbContextOptionsBuilder<TimingDb>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var g1 = new FinGroup { Id = 1, ResultShift = new TimeOrLength(TimeSpan.FromMinutes(1)) };
            var count = 10;

            using (var db = new TimingDb(options))
            {
                db.FinGroups.Add(g1);
                for (var i = 0; i < count; i++)
                {
                    db.FinMarks.Add(new FinMark
                    {
                        FinGroupId = g1.Id,
                        OriginalResult = i % 2 == 1
                            ? new TimeOrLength(TimeSpan.FromMinutes(i))
                            : new TimeOrLength(i),
                        RevisedResult = i % 2 == 1
                            ? new TimeOrLength(TimeSpan.FromMinutes(i))
                            : new TimeOrLength(i),
                    });
                }

                await db.SaveChangesAsync();
            }

            var service = new FinGroupService(currentTournamentServiceMock.Object, loggerMock.Object);

            using (var db = new TimingDb(options))
            {
                var updated = await service.UpdateMarksRevisedResult(g1, db);

                Assert.Equal(count / 2, updated);

                foreach (var item in await db.FinMarks.ToListAsync())
                {
                    if (item.ResultType == g1.ResultShiftType)
                    {
                        Assert.Equal(item.OriginalResult.Time.Add(g1.ResultShift.Time), item.RevisedResult.Time);
                    }
                    else
                    {
                        Assert.Equal(item.OriginalResult, item.RevisedResult);
                    }
                }
            }
        }
    }
}

﻿namespace Parsec.Timing.Readers.MyLaps.V21
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Xunit;

    public class MylapsMessageHandlerTests
    {
        [Fact]
        public async Task HandlePong()
        {
            var loggerMock = new Mock<ILogger<MylapsMessageHandler>>();

            var responses = new List<string>();

            var handler = new MylapsMessageHandler(loggerMock.Object);

            var args = new NewMessageEventArgs
            {
                Message = "Hello@Pong@",
                RemoteEndpoint = null,
                SendResponseAsync = async (x) => responses.Add(x),
            };

            await handler.HandleMessageAsync(null, args);

            Assert.Contains("Parsec@AckPong@Version2.1@c|n|t|d|dv|dm|prof|l@", responses);
        }

        [Fact]
        public async Task HandlePassing()
        {
            var storeMessage = "Hello@Passing@" +
                "c=KF06288|n=KF06288|t=19:00:15.518|d=180221|dv=1|dm=003056A8F84A@" +
                "c=CX57131|n=CX57131|t=19:00:15.580|d=180222|dv=1|dm=003056A8F84A@" +
                "c=TF62802|n=TF62802|t=19:00:16.238|d=180222|dv=1|dm=003056A8F84A@" +
                "c=FG65344|n=FG65344|t=19:00:31.773|d=180222|dv=1|dm=003056A8F84A@" +
                "c=TG11532|n=TG11532|t=19:00:32.513|d=180222|dv=1|dm=003056A8F84A@47@";

            var loggerMock = new Mock<ILogger<MylapsMessageHandler>>();

            var responses = new List<string>();
            var args = new List<NewTagVisitsEventArgs>();

            var handler = new MylapsMessageHandler(loggerMock.Object);
            handler.NewTagVisits += async (src, ea) => args.Add(ea);

            var a = new NewMessageEventArgs
            {
                Message = storeMessage,
                RemoteEndpoint = null,
                SendResponseAsync = async (x) => responses.Add(x),
            };

            await handler.HandleMessageAsync(null, a);

            Assert.Contains("Parsec@AckPassing@47@", responses);
            Assert.Single(args);
            Assert.Equal("Hello", args[0].Name);
            Assert.Equal(47, args[0].Sequence);
            Assert.Equal(5, args[0].TagVisits.Length);

            Assert.Equal("KF06288", args[0].TagVisits[0].Tag);
            Assert.Equal(new DateTimeOffset(2018, 2, 21, 19, 00, 15, 518, TimeSpan.Zero), args[0].TagVisits[0].Time);

            Assert.Equal("CX57131", args[0].TagVisits[1].Tag);
            Assert.Equal(new DateTimeOffset(2018, 2, 22, 19, 00, 15, 580, TimeSpan.Zero), args[0].TagVisits[1].Time);

            Assert.Equal("TF62802", args[0].TagVisits[2].Tag);
            Assert.Equal(new DateTimeOffset(2018, 2, 22, 19, 00, 16, 238, TimeSpan.Zero), args[0].TagVisits[2].Time);

            Assert.Equal("FG65344", args[0].TagVisits[3].Tag);
            Assert.Equal(new DateTimeOffset(2018, 2, 22, 19, 00, 31, 773, TimeSpan.Zero), args[0].TagVisits[3].Time);

            Assert.Equal("TG11532", args[0].TagVisits[4].Tag);
            Assert.Equal(new DateTimeOffset(2018, 2, 22, 19, 00, 32, 513, TimeSpan.Zero), args[0].TagVisits[4].Time);
        }
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;
    using Xunit;

    public class TimeOrLengthTests
    {
        [Fact]
        public void StoreDecimal()
        {
            var sample = 1234.56M;

            var rv = new TimeOrLength(sample);

            Assert.True(rv.IsLength);
            Assert.Equal(sample, rv.Length);
            Assert.Equal(ResultType.Length1000, rv.Type);
        }

        [Fact]
        public void StoreTime()
        {
            var sample = new TimeSpan(1, 2, 3, 4, 5);

            var rv = new TimeOrLength(sample);

            Assert.True(rv.IsTime);
            Assert.Equal(sample, rv.Time);
            Assert.Equal(ResultType.Time1000, rv.Type);
        }

        [Fact]
        public void ThrowExceptionOnWrongType1()
        {
            var sample = new TimeSpan(1, 2, 3, 4, 5);

            var rv = new TimeOrLength(sample);

            Assert.False(rv.IsLength);
            Assert.Throws<InvalidOperationException>(() => rv.Length);
        }

        [Fact]
        public void ThrowExceptionOnWrongType2()
        {
            var sample = 1234.56M;

            var rv = new TimeOrLength(sample);

            Assert.False(rv.IsTime);
            Assert.Throws<InvalidOperationException>(() => rv.Time);
        }

        [Fact]
        public void InternalConstructorWorks()
        {
            var sample1 = new TimeOrLength(ResultType.Length1000, 1234567);
            var sample2 = new TimeOrLength(ResultType.Time1000, 61234);

            Assert.True(sample1.IsLength);
            Assert.True(sample2.IsTime);
            Assert.Equal(1234.567M, sample1.Length);
            Assert.Equal(new TimeSpan(0, 0, 1, 1, 234), sample2.Time);
        }

        [Fact]
        public void ZeroConstruction()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new TimeOrLength(ResultType.Unknown, 1));

            var sample = new TimeOrLength(ResultType.Unknown, 0);

            Assert.Equal(TimeOrLength.Zero.Type, sample.Type);
            Assert.Equal(TimeOrLength.Zero.EncodedValue, sample.EncodedValue);
        }

        [Fact]
        public void ComparisonOperators()
        {
            var sample1 = new TimeOrLength(123.45M);
            var sample2 = new TimeOrLength(123.456M);
            var sample3 = new TimeOrLength(TimeSpan.FromMinutes(1));
            var sample4 = new TimeOrLength(TimeSpan.FromMinutes(2));

            Assert.True(sample1 < sample2);
            Assert.False(sample1 > sample2);
            Assert.True(sample1 == new TimeOrLength(123.45M));
            Assert.False(sample1 == sample2);
            Assert.True(sample1 != sample2);

            Assert.False(new TimeOrLength(1M) == new TimeOrLength(TimeSpan.FromSeconds(1)));

            Assert.True(sample3 < sample4);
            Assert.False(sample3 > sample4);
            Assert.True(sample3 == new TimeOrLength(TimeSpan.FromMinutes(1)));
            Assert.False(sample3 == sample4);
            Assert.True(sample3 != sample4);
        }

        [Fact]
        public void EqualsTests()
        {
            var val1 = new TimeOrLength(1M);
            var val2 = new TimeOrLength(1M);
            var val3 = new TimeOrLength(3M);

            var val4 = new TimeOrLength(TimeSpan.FromMinutes(1));
            var val5 = new TimeOrLength(TimeSpan.FromMinutes(1));
            var val6 = new TimeOrLength(TimeSpan.FromMinutes(2));

            Assert.True(val1.Equals(val2));
            Assert.True(val1.Equals((object)val2));
            Assert.False(val1.Equals(val3));
            Assert.False(val1.Equals((object)val3));

            Assert.True(val4.Equals(val5));
            Assert.True(val4.Equals((object)val5));
            Assert.False(val4.Equals(val6));
            Assert.False(val4.Equals((object)val6));

            Assert.False(val1.Equals(val4));

            Assert.False(val1.Equals(TimeOrLength.Zero));
            Assert.False(val4.Equals(TimeOrLength.Zero));
            Assert.True(TimeOrLength.Zero.Equals(TimeOrLength.Zero));
        }

        [Fact]
        public void CompareTests()
        {
            var val1 = new TimeOrLength(1M);
            var val2 = new TimeOrLength(1M);
            var val3 = new TimeOrLength(3M);

            var val4 = new TimeOrLength(TimeSpan.FromMinutes(1));
            var val5 = new TimeOrLength(TimeSpan.FromMinutes(1));
            var val6 = new TimeOrLength(TimeSpan.FromMinutes(2));

            Assert.Equal(0, val1.CompareTo(val2));
            Assert.Equal(0, val1.CompareTo((object)val2));
            Assert.Equal(-1, val1.CompareTo(val3));
            Assert.Equal(-1, val1.CompareTo((object)val3));
            Assert.Equal(1, val3.CompareTo(val1));
            Assert.Equal(1, val3.CompareTo((object)val1));

            Assert.Equal(0, val4.CompareTo(val5));
            Assert.Equal(0, val4.CompareTo((object)val5));
            Assert.Equal(-1, val4.CompareTo(val6));
            Assert.Equal(-1, val4.CompareTo((object)val6));
            Assert.Equal(1, val6.CompareTo(val4));
            Assert.Equal(1, val6.CompareTo((object)val4));

            Assert.NotEqual(0, val1.CompareTo(val4));

            Assert.Equal(1, val1.CompareTo(TimeOrLength.Zero));
            Assert.Equal(1, val4.CompareTo(TimeOrLength.Zero));
            Assert.Equal(0, TimeOrLength.Zero.CompareTo(TimeOrLength.Zero));
        }
    }
}

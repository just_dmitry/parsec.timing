﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Команда компьютерной обработки
    /// </summary>
    public class ServicingTeam
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        /// <summary>
        /// Рекламный текст (для протокола)
        /// </summary>
        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string MarketingText { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServicingTeam>(entity =>
            {
                entity.HasAlternateKey(x => x.Name);
            });
        }
    }
}

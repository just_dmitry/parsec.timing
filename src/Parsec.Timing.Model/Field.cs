﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Description("Поле")]
    public class Field
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        [Display(Name = "Код")]
        public string Key { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Источник")]
        public FieldSource Source { get; set; }

        [Display(Name = "Вычисляемый")]
        public bool IsComputed { get; set; }

        [Display(Name = "Выравнивание")]
        public HorizontalAlign HorizontalAlign { get; set; }

        [Display(Name = "Тип (элемента управления)")]
        public FieldType Type { get; set; }

        [Display(Name = "Порядок", Description = "Порядок сортировки (меньшие значения раньше)")]
        public byte SortOrder { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            // Nothing
        }
    }
}

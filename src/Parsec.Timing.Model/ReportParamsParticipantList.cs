﻿namespace Parsec.Timing.Model
{
    using System;

    public class ReportParamsParticipantList
    {
        public bool GroupByAgeGroup { get; set; }

        public bool SortByName { get; set; }
    }
}

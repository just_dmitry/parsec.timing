﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class ParticipantRanking
    {
        [Key]
        public long Id { get; set; }

        public Guid ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual Participant Participant { get; set; }

        [Required]
        public Guid TournamentCompetitionId { get; set; }

        [ForeignKey(nameof(TournamentCompetitionId))]
        public virtual TournamentCompetition TournamentCompetition { get; set; }

        public byte Age { get; set; }

        public Guid? StatusId { get; set; }

        [ForeignKey(nameof(StatusId))]
        public virtual ParticipantStatus Status { get; set; }

        public Guid? FinalSplitId { get; set; }

        [ForeignKey(nameof(FinalSplitId))]
        public virtual Split FinalSplit { get; set; }

        public int SortOrder { get; set; }

        [MaxLength(20)]
        public string ResultText { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthRank)]
        public string Rank { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthShortComment)]
        public string ResultComment { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantRanking>(entity =>
            {
                entity.HasAlternateKey(t => new { t.TournamentCompetitionId, t.ParticipantId });
                entity.HasOne(t => t.Participant).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(t => t.TournamentCompetition).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Status).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.FinalSplit).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

﻿namespace System.ComponentModel.DataAnnotations
{
    using System.Reflection;

    public class AllPropertiesAreNotNullAttribute : ValidationAttribute
    {
        private readonly string[] propertyNames;

        public AllPropertiesAreNotNullAttribute(params string[] propertyNames)
        {
            if (propertyNames == null || propertyNames.Length == 0)
            {
                throw new ArgumentNullException(nameof(propertyNames));
            }

            if (propertyNames.Length == 1)
            {
                throw new ArgumentOutOfRangeException(nameof(propertyNames), "Need at least two (2) properties.");
            }

            this.propertyNames = propertyNames;
        }

        public override bool RequiresValidationContext => true;

        public override string FormatErrorMessage(string name)
        {
            return "Property should not be null: " + name;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var first = true;
            var firstIsNull = false;

            foreach (var name in propertyNames)
            {
                var prop = validationContext.ObjectType.GetRuntimeProperty(name);
                if (prop == null)
                {
                    return new ValidationResult("Property not found: " + name);
                }

                var val = prop.GetValue(validationContext.ObjectInstance, null);
                var isNull = val == null;

                if (first)
                {
                    firstIsNull = isNull;
                    first = false;
                }
                else if (isNull != firstIsNull)
                {
                    return new ValidationResult(this.FormatErrorMessage(name));
                }
            }

            return null;
        }
    }
}

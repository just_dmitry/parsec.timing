﻿namespace System.ComponentModel.DataAnnotations
{
    using System.Reflection;

    public class ExactlyOnePropertyNotNullAttribute : ValidationAttribute
    {
        private readonly string[] propertyNames;

        public ExactlyOnePropertyNotNullAttribute(params string[] propertyNames)
        {
            if (propertyNames == null || propertyNames.Length == 0)
            {
                throw new ArgumentNullException(nameof(propertyNames));
            }

            if (propertyNames.Length == 1)
            {
                throw new ArgumentOutOfRangeException(nameof(propertyNames), "Need at least two (2) properties.");
            }

            this.propertyNames = propertyNames;
        }

        public override bool RequiresValidationContext => true;

        public override string FormatErrorMessage(string name)
        {
            return "Only one property shound not be null: " + string.Join(", ", propertyNames);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var notNullCount = 0;

            foreach (var name in propertyNames)
            {
                var prop = validationContext.ObjectType.GetRuntimeProperty(name);
                if (prop == null)
                {
                    return new ValidationResult("Property not found: " + name);
                }

                var val = prop.GetValue(validationContext.ObjectInstance, null);
                if (val != null)
                {
                    notNullCount++;
                }
            }

            if (notNullCount != 1)
            {
                return new ValidationResult(this.FormatErrorMessage(string.Empty));
            }

            return null;
        }
    }
}

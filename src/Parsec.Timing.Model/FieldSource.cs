﻿namespace Parsec.Timing.Model
{
    public enum FieldSource : byte
    {
        Person,
        Participant,
        ParticipantField,
        ParticipantRanking,
        ParticipantAgeRanking,
    }
}

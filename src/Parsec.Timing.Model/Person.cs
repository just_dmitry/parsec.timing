﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Security.Cryptography;
    using Microsoft.EntityFrameworkCore;

    public class Person
    {
        private static readonly HashAlgorithm HashProvider = MD5.Create();

        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string FirstName { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string MiddleName { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string LastNameInternational { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string FirstNameInternational { get; set; }

        public Gender Gender { get; set; }

        public byte? BirthDay { get; set; }

        public byte? BirthMonth { get; set; }

        public short? BirthYear { get; set; }

        public DateTimeOffset InsertTime { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [Required]
        [MaxLength(32)]
        public string HashValue { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasIndex(x => x.HashValue);
                entity.Property(x => x.InsertTime).HasDateTimeOffsetPrecision(0);
            });
        }

        public string GetBirthDateText()
        {
            return (BirthDay?.ToString() ?? "__")
                + "."
                + (BirthMonth?.ToString("00") ?? "__")
                + "."
                + (BirthYear?.ToString() ?? "____");
        }

        public string ComputeHash()
        {
            return GetHashString(GetAllFields());
        }

        public bool FieldsEqualTo(Person other)
        {
            if (string.CompareOrdinal(this.HashValue, other.HashValue) != 0)
            {
                return false;
            }

            if (string.CompareOrdinal(this.GetAllFields(), other.GetAllFields()) == 0)
            {
                return true;
            }

            return false;
        }

        private static string GetHashString(string value)
        {
            var hashBytes = HashProvider.ComputeHash(System.Text.Encoding.UTF8.GetBytes(value));
            return BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToUpperInvariant();
        }

        private string GetAllFields()
        {
            const string separator = ";";
            return string.Concat(new[]
            {
                LastName, separator,
                FirstName, separator,
                MiddleName, separator,
                LastNameInternational, separator,
                FirstNameInternational, separator,
                ((int)Gender).ToString(), separator,
                BirthDay?.ToString(), separator,
                BirthMonth?.ToString(), separator,
                BirthYear.ToString(), separator,
            });
        }
    }
}

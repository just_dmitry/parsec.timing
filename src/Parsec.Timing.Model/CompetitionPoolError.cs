﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class CompetitionPoolError
    {
        [Key]
        public int Id { get; set; }

        public Guid CompetitionPoolId { get; set; }

        [ForeignKey(nameof(CompetitionPoolId))]
        public virtual CompetitionPool CompetitionPool { get; set; }

        public ProcessingErrorType Type { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthBib)]
        public string Bib { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string Text { get; set; }

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Nothing
        }
    }
}

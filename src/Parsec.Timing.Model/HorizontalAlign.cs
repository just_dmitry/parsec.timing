﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel;

    public enum HorizontalAlign : byte
    {
        [Description("По левому краю")]
        Left = 0,

        [Description("По центру")]
        Center = 1,

        [Description("По правому краю")]
        Right = 2,
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Результат участника соревнований на промежуточной отметке
    /// </summary>
    public class ParticipantSplitRanking
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public Guid ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual Participant Participant { get; set; }

        [Required]
        public Guid TournamentCompetitionId { get; set; }

        [ForeignKey(nameof(TournamentCompetitionId))]
        public virtual TournamentCompetition TournamentCompetition { get; set; }

        [Required]
        public Guid SplitId { get; set; }

        [ForeignKey(nameof(SplitId))]
        public virtual Split Split { get; set; }

        public Guid? StatusId { get; set; }

        [ForeignKey(nameof(StatusId))]
        public virtual ParticipantStatus Status { get; set; }

        [MaxLength(20)]
        public string ResultText { get; set; }

        [MaxLength(20)]
        public string Rank { get; set; }

        public int SortOrder { get; set; }

        public ResultType DiffResultType { get; set; }

        public long DiffResultValue { get; set; }

        [NotMapped]
        public TimeOrLength DiffResult
        {
            get
            {
                return new TimeOrLength(DiffResultType, DiffResultValue);
            }

            set
            {
                DiffResultType = value.Type;
                DiffResultValue = value.EncodedValue;
            }
        }

        [MaxLength(20)]
        public string DiffResultText { get; set; }

        [MaxLength(20)]
        public string DiffRank { get; set; }

        public int DiffSortOrder { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantSplitRanking>(entity =>
            {
                entity.HasOne(t => t.Participant).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(t => t.TournamentCompetition).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Split).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Status).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

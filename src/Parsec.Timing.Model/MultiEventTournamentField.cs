﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class MultiEventTournamentField
    {
        [Key]
        public int Id { get; set; }

        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        public Guid FieldId { get; set; }

        [ForeignKey(nameof(FieldId))]
        public virtual Field Field { get; set; }

        public byte SortOrder { get; set; }

        public bool IsUsed { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MultiEventTournamentField>(entity =>
            {
                entity.HasIndex(x => new { x.MultiEventTournamentId, x.FieldId }).IsUnique();

                entity.HasOne(x => x.MultiEventTournament).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Field).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

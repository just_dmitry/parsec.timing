﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class ReportField
    {
        [Key]
        public int Id { get; set; }

        public Guid ReportId { get; set; }

        [ForeignKey(nameof(ReportId))]
        public virtual Report Report { get; set; }

        public Guid FieldId { get; set; }

        [ForeignKey(nameof(FieldId))]
        public virtual Field Field { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Title { get; set; }

        public int SortOrder { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReportField>(entity =>
            {
                entity.HasOne(t => t.Report).WithMany(t => t.Fields).OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(t => t.Field).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

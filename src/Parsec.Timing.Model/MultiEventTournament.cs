﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Глобальное "спортивное событие", эквивалент "соревнования в целом".
    /// </summary>
    public class MultiEventTournament
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string Location { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string Website { get; set; }

        public Guid ServicingTeamId { get; set; }

        [ForeignKey(nameof(ServicingTeamId))]
        public virtual ServicingTeam ServicingTeam { get; set; }

        public bool IsLocked { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string Comment { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MultiEventTournament>(entity =>
            {
                entity.HasIndex(x => x.StartDate);
                entity.HasOne(x => x.ServicingTeam).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

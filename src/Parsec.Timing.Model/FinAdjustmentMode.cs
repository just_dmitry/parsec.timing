﻿namespace Parsec.Timing.Model
{
    public enum FinAdjustmentMode : byte
    {
        Unspecified = 0,

        FLAG_SPECIAL = 0b0001_0000,

        Reject = FLAG_SPECIAL + 1,
        StartNextLap = FLAG_SPECIAL + 2,

        FLAG_MODIFY = 0b0010_0000,

        AssignValue = FLAG_MODIFY + 1,

        FLAG_APPLY = 0b0100_0000,

        ApplyAbsoluteSetBoth = FLAG_APPLY + 1,
        ApplyAbsoluteAddLengthSetTime = FLAG_APPLY + 2,
        ApplyAbsoluteAddTimeSetLength = FLAG_APPLY + 3,
        ApplyAbsoluteAddBoth = FLAG_APPLY + 4,
        ApplyLastLapAddLengthSetTime = FLAG_APPLY + 5,
        ApplyLastLapAddTimeSetLength = FLAG_APPLY + 6,
        ApplyLastLapAddBoth = FLAG_APPLY + 7,
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    public class FinAdjustment
    {
        [Key]
        public Guid Id { get; set; }

        public int Order { get; set; }

        public bool IsAutoGenerated { get; set; }

        [Required]
        public Guid CompetitionPoolId { get; set; }

        [ForeignKey(nameof(CompetitionPoolId))]
        public virtual CompetitionPool CompetitionPool { get; set; }

        public Guid? CompetitionId { get; set; }

        [ForeignKey(nameof(CompetitionId))]
        public virtual Competition Competition { get; set; }

        public Guid? CheckpointId { get; set; }

        [ForeignKey(nameof(CheckpointId))]
        public virtual Checkpoint Checkpoint { get; set; }

        public ResultType NotEarlierThanResultType { get; set; }

        public long NotEarlierThanResultValue { get; set; }

        [NotMapped]
        public TimeOrLength NotEarlierThanResult
        {
            get
            {
                return new TimeOrLength(NotEarlierThanResultType, NotEarlierThanResultValue);
            }

            set
            {
                NotEarlierThanResultType = value.Type;
                NotEarlierThanResultValue = value.EncodedValue;
            }
        }

        public ResultType NotLaterThanResultType { get; set; }

        public long NotLaterThanResultValue { get; set; }

        [NotMapped]
        public TimeOrLength NotLaterThanResult
        {
            get
            {
                return new TimeOrLength(NotLaterThanResultType, NotLaterThanResultValue);
            }

            set
            {
                NotLaterThanResultType = value.Type;
                NotLaterThanResultValue = value.EncodedValue;
            }
        }

        public int Category { get; set; }

        [Column("Bibs")]
        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string BibString
        {
            get
            {
                return BibArray == null
                    ? string.Empty
                    : string.Join(",", BibArray.Where(x => !string.IsNullOrEmpty(x)));
            }

            set
            {
                BibArray = string.IsNullOrEmpty(value)
                    ? new string[0]
                    : value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        [NotMapped]
        public string[] BibArray { get; set; }

        public FinAdjustmentMode Mode { get; set; }

        public ResultType ValueType { get; set; }

        public long ValueValue { get; set; }

        [NotMapped]
        public TimeOrLength Value
        {
            get
            {
                return new TimeOrLength(ValueType, ValueValue);
            }

            set
            {
                ValueType = value.Type;
                ValueValue = value.EncodedValue;
            }
        }

        public Guid? NextFinAdjustmentId { get; set; }

        [ForeignKey(nameof(NextFinAdjustmentId))]
        public virtual FinAdjustment NextFinAdjustment { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthShortComment)]
        public string Comment { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FinAdjustment>(entity =>
            {
                entity.HasOne(x => x.CompetitionPool).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Competition).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(x => x.Checkpoint).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(x => x.NextFinAdjustment).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}
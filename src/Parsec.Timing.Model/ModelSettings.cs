﻿namespace Parsec.Timing.Model
{
    public static class ModelSettings
    {
        public const int FieldMaxLengthName = 200;

        public const int FieldMaxLengthOther = 500;

        public const int FieldMaxLengthComment = 1000;

        public const int FieldMaxLengthBib = 10;

        public const int FieldMaxLengthRank = 20;

        public const int FieldMaxLengthShortComment = 50;

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            AgeGroup.OnModelCreating(modelBuilder);
            AgeGroupSet.OnModelCreating(modelBuilder);
            AgeGroupSetContent.OnModelCreating(modelBuilder);
            CategoryFlag.OnModelCreating(modelBuilder);
            Checkpoint.OnModelCreating(modelBuilder);
            Competition.OnModelCreating(modelBuilder);
            CompetitionPool.OnModelCreating(modelBuilder);
            CompetitionPoolError.OnModelCreating(modelBuilder);
            Field.OnModelCreating(modelBuilder);
            FinGroup.OnModelCreating(modelBuilder);
            FinMark.OnModelCreating(modelBuilder);
            FinAdjustment.OnModelCreating(modelBuilder);
            MultiEventTournament.OnModelCreating(modelBuilder);
            MultiEventTournamentField.OnModelCreating(modelBuilder);
            Participant.OnModelCreating(modelBuilder);
            ParticipantAgeRanking.OnModelCreating(modelBuilder);
            ParticipantField.OnModelCreating(modelBuilder);
            ParticipantOverride.OnModelCreating(modelBuilder);
            ParticipantRanking.OnModelCreating(modelBuilder);
            ParticipantSplitAgeRanking.OnModelCreating(modelBuilder);
            ParticipantSplitRanking.OnModelCreating(modelBuilder);
            ParticipantStatus.OnModelCreating(modelBuilder);
            Person.OnModelCreating(modelBuilder);
            Report.OnModelCreating(modelBuilder);
            ReportField.OnModelCreating(modelBuilder);
            Result.OnModelCreating(modelBuilder);
            ResultOverride.OnModelCreating(modelBuilder);
            ServicingTeam.OnModelCreating(modelBuilder);
            Split.OnModelCreating(modelBuilder);
            Tournament.OnModelCreating(modelBuilder);
            TournamentCompetition.OnModelCreating(modelBuilder);
            TournamentCompetitionSplit.OnModelCreating(modelBuilder);
            TournamentOfficial.OnModelCreating(modelBuilder);
            TournamentSeries.OnModelCreating(modelBuilder);
            //////User.OnModelCreating(modelBuilder);
        }
    }
}

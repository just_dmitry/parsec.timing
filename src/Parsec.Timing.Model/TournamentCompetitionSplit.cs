﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class TournamentCompetitionSplit
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid TournamentCompetitionId { get; set; }

        [ForeignKey(nameof(TournamentCompetitionId))]
        public virtual TournamentCompetition TournamentCompetition { get; set; }

        [Required]
        public Guid SplitId { get; set; }

        [ForeignKey(nameof(SplitId))]
        public virtual Split Split { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsFinishable { get; set; }

        public bool IsHidden { get; set; }

        public bool AllocatePlaces { get; set; }

        public byte PlacesGroupIndex { get; set; }

        public bool AllocateSplitPlacesByResult { get; set; }

        public bool AllocateSplitPlacesByLapTime { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TournamentCompetitionSplit>(entity =>
            {
                entity.HasAlternateKey(x => new { x.TournamentCompetitionId, x.SplitId });
                entity.HasOne(t => t.TournamentCompetition).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(t => t.Split).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

﻿namespace Parsec.Timing.Model
{
    public enum ResultType : byte
    {
        Unknown,

        /// <summary>
        /// Time, in 1/1000 of second
        /// </summary>
        Time1000,

        /// <summary>
        /// Length, in 1/1000 of meter
        /// </summary>
        Length1000,
    }
}

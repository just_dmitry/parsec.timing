﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Report
    {
        [Key]
        public Guid Id { get; set; }

        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Тип")]
        public ReportType Type { get; set; }

        [MaxLength(1024)]
        public string Params { get; set; }

        [Display(Name = "Только для этих соревнований")]
        public Guid? TournamentId { get; set; }

        [ForeignKey(nameof(TournamentId))]
        public virtual Tournament Tournament { get; set; }

        [InverseProperty("Report")]
        public virtual ICollection<ReportField> Fields { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Report>(entity =>
            {
                entity.HasAlternateKey(x => new { x.MultiEventTournamentId, x.Name });
            });
        }
    }
}

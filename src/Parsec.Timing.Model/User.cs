﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public static readonly Guid BackgroundTaskUserId = Guid.Parse("a0641b1b-7ea4-462d-bd05-7573ea523ec7");

        [Key]
        public Guid UserId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string NormalizedName { get; set; }

        [Required]
        [MaxLength(100)]
        public string EMail { get; set; }

        [Required]
        [MaxLength(40)]
        public string EMailMD5 { get; set; }

        [Required]
        [MaxLength(100)]
        public string NormalizedEmail { get; set; }

        [Required]
        public bool IsAdmin { get; set; }

        ////[Required]
        ////public bool IsManager { get; set; }

        [Required]
        public bool IsArchived { get; set; }

        [MaxLength(1000)]
        public string ProfileUrl { get; set; }

        [MaxLength(1000)]
        public string PictureUrl { get; set; }

        [MaxLength(100)]
        public string TimeZoneId { get; set; }

        [Required]
        public bool AutomaticUpdatesAllowed { get; set; }

        [Required]
        public DateTimeOffset MemberSince { get; set; }

        public DateTimeOffset? LastLogon { get; set; }

        public DateTimeOffset? LastVisit { get; set; }

        #region Identity: External login info

        [MaxLength(200)]
        public string LoginProvider { get; set; }

        [MaxLength(200)]
        public string ProviderDisplayName { get; set; }

        [MaxLength(200)]
        public string ProviderKey { get; set; }

        /// <summary>
        /// User security stamp value.
        /// See <see cref="http://stackoverflow.com/questions/37162791/"/> and <see cref="http://stackoverflow.com/questions/19487322/"/> for details.
        /// </summary>
        [MaxLength(100)]
        public string SecurityStamp { get; set; }

        #endregion

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            // Nothing
        }
    }
}

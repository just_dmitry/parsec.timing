﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Возможный статус участника (и правила работы с ним)
    /// </summary>
    public class ParticipantStatus
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string PublicName { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string PublicNameInternational { get; set; }

        [Required]
        public byte SortOrder { get; set; }

        public bool KeepResult { get; set; }

        public bool AssignPlace { get; set; }

        public bool IsGlobal { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantStatus>(entity =>
            {
                entity.HasIndex(x => x.Name).IsUnique();
            });
        }
    }
}

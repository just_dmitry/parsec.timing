﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Место (точка) фиксации времени
    /// </summary>
    /// <remarks>
    /// Previously: <b>FIN_GROUP</b>
    /// </remarks>
    public class Checkpoint
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        public byte SortOrder { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Checkpoint>(entity =>
            {
                entity.HasAlternateKey(x => new { x.MultiEventTournamentId, x.Name });
            });
        }
    }
}

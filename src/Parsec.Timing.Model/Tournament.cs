﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// "Официальное" ("юридическое") соревнование - то, на что есть отдельное (подписанное) положение.
    ///     Например отдельными объектами являются Чемпионат Москвы по суточному бегу,
    ///     Чемпионат России по суточному бегу, Чемпионат ЮЗАО по суточному бегу
    ///     внутри единого спортивного события (<see cref="Model.MultiEventTournament"/>) "Сутки Бегом".
    /// </summary>
    public class Tournament
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [Required]
        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        public Guid? TournamentSeriesId { get; set; }

        [ForeignKey(nameof(TournamentSeriesId))]
        public virtual TournamentSeries TournamentSeries { get; set; }

        /// <summary>
        /// Основное (главное, самое обширное) соревнование среди остальных
        /// </summary>
        public bool IsMain { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string TitleLocal { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string TitleInternational { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tournament>(entity =>
            {
                entity.HasAlternateKey(x => new { x.MultiEventTournamentId, x.Name });
                entity.HasOne(t => t.TournamentSeries).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

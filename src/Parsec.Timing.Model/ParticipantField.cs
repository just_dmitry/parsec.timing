﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Дополнительные данные участника соревнований
    /// </summary>
    public class ParticipantField
    {
        [Key]
        public long Id { get; set; }

        public int CompetitionKey { get; set; }

        public Guid ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual Participant Participant { get; set; }

        public int MultiEventTournamentFieldId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentFieldId))]
        public virtual MultiEventTournamentField MultiEventTournamentField { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string Value { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantField>(entity =>
            {
                entity.HasIndex(x => x.CompetitionKey);

                entity.HasOne(x => x.Participant).WithMany().IsRequired().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.MultiEventTournamentField).WithMany().IsRequired().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

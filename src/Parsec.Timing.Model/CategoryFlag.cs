﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class CategoryFlag
    {
        public int Id { get; set; }

        [Required]
        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string Comment { get; set; }

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryFlag>(entity =>
            {
                entity.HasKey(x => new { x.MultiEventTournamentId, x.Id });
            });
        }
    }
}

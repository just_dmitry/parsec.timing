﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Вид программы ("дистанция") в рамках <see cref="MultiEventTournament">спортивного события</see>
    /// </summary>
    public class Competition
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public int Key { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string NameInternational { get; set; }

        [Required]
        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        [Required]
        public byte SortOrder { get; set; }

        public Guid CompetitionPoolId { get; set; }

        [ForeignKey(nameof(CompetitionPoolId))]
        public virtual CompetitionPool CompetitionPool { get; set; }

        public bool AllowMen { get; set; }

        public bool AllowWomen { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Competition>(entity =>
            {
                entity.HasAlternateKey(x => new { x.MultiEventTournamentId, x.Name });
                entity.HasIndex(x => x.Key).IsUnique();
                ////entity.HasOne(t => t.StartGun).WithMany().OnDelete(DeleteBehavior.Restrict);
                ////entity.HasOne(t => t.BeyoundMaxResultStatus).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(x => x.CompetitionPool).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

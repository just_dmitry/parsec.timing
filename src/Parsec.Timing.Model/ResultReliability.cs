﻿namespace Parsec.Timing.Model
{
    public enum ResultReliability : byte
    {
        Unknown = 0,
        NonApplicable = 1,

        VeryFast = 50,
        Faster = 80,
        Ok = 100,
        Slower = 120,
        VerySlow = 150,
    }
}

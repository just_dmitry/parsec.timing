﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class FinMark
    {
        [Key]
        public long Id { get; set; }

        public int FinGroupId { get; set; }

        [ForeignKey(nameof(FinGroupId))]
        public virtual FinGroup FinGroup { get; set; }

        [MaxLength(50)]
        public string Bib { get; set; }

        public ResultType ResultType { get; set; }

        public long OriginalResultValue { get; set; }

        public long RevisedResultValue { get; set; }

        /// <summary>
        /// "Исходный" результат (как получен от источника)
        /// </summary>
        [NotMapped]
        public TimeOrLength OriginalResult
        {
            get
            {
                return new TimeOrLength(ResultType, OriginalResultValue);
            }

            set
            {
                ResultType = value.Type;
                OriginalResultValue = value.EncodedValue;
            }
        }

        /// <summary>
        /// "Скорректированный" результат (если источник не откалиброван)
        /// </summary>
        [NotMapped]
        public TimeOrLength RevisedResult
        {
            get
            {
                return new TimeOrLength(ResultType, RevisedResultValue);
            }

            set
            {
                if (value.Type != ResultType)
                {
                    throw new ArgumentOutOfRangeException("Wrong Type");
                }

                RevisedResultValue = value.EncodedValue;
            }
        }

        /// <summary>
        /// "Итоговый" (эффективный) результат: "скорректированный" если он задан, иначе "исходный".
        /// </summary>
        [NotMapped]
        public TimeOrLength EffectiveResult
        {
            get
            {
                return new TimeOrLength(ResultType, RevisedResultValue == 0 ? OriginalResultValue : RevisedResultValue);
            }
        }

        public FinMarkStatus Status { get; set; }

        public DateTimeOffset Inserted { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FinMark>(entity =>
            {
                entity.Property(x => x.Inserted).HasDateTimeOffsetPrecision(2);
            });
        }
    }
}

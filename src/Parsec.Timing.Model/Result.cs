﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class Result
    {
        public Guid ParticipantId { get; set; }

        public int Index { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual Participant Participant { get; set; }

        public long? FinMarkId { get; set; }

        [ForeignKey(nameof(FinMarkId))]
        public virtual FinMark FinMark { get; set; }

        public TimeSpan TotalTime { get; set; }

        public decimal TotalLength { get; set; }

        public int FullLapsCount { get; set; }

        public TimeSpan FullLapsTime { get; set; }

        public decimal FullLapsLength { get; set; }

        public ResultReliability Reliability { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public long TotalTimeTicks
        {
            get { return TotalTime.Ticks; }
            set { TotalTime = TimeSpan.FromTicks(value); }
        }

        public long FullLapsTimeTicks
        {
            get { return FullLapsTime.Ticks; }
            set { FullLapsTime = TimeSpan.FromTicks(value); }
        }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Result>(entity =>
            {
                entity.HasKey(x => new { x.ParticipantId, x.Index });
                entity.HasOne(x => x.FinMark).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.Ignore(x => x.TotalTime);
                entity.Property(x => x.TotalTimeTicks);
                entity.Ignore(x => x.FullLapsTime);
                entity.Property(x => x.FullLapsTimeTicks);
            });
        }

        public Result CreateNextGround()
        {
            return new Result
            {
                ParticipantId = this.ParticipantId,
                Index = this.Index + 1,
                FinMarkId = this.FinMarkId,
                TotalTime = this.TotalTime,
                TotalLength = this.TotalLength,
                FullLapsCount = this.FullLapsCount,
                FullLapsTime = this.FullLapsTime,
                FullLapsLength = this.FullLapsLength,
                Reliability = ResultReliability.Unknown,
            };
        }
    }
}
﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    public class ParticipantOverride
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid CompetitionPoolId { get; set; }

        [ForeignKey(nameof(CompetitionPoolId))]
        public virtual CompetitionPool CompetitionPool { get; set; }

        public Guid? TournamentId { get; set; }

        [ForeignKey(nameof(TournamentId))]
        public virtual Tournament Tournament { get; set; }

        public Guid? CompetitionId { get; set; }

        [ForeignKey(nameof(CompetitionId))]
        public virtual Competition Competition { get; set; }

        public Gender? Gender { get; set; }

        [Column("Bibs")]
        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string BibString
        {
            get
            {
                return BibArray == null
                    ? string.Empty
                    : string.Join(",", BibArray.Where(x => !string.IsNullOrEmpty(x)));
            }

            set
            {
                BibArray = string.IsNullOrEmpty(value)
                    ? new string[0]
                    : value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public int Category { get; set; }

        [NotMapped]
        public string[] BibArray { get; set; }

        public byte? AgeOverride { get; set; }

        public bool? ParticipationOverride { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthShortComment)]
        public string Comment { get; set; }

        public DateTimeOffset InsertTime { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantOverride>(entity =>
            {
                entity.HasOne(x => x.CompetitionPool).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Tournament).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(x => x.Competition).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.Property(t => t.InsertTime).HasDateTimeOffsetPrecision(2);
            });
        }
    }
}

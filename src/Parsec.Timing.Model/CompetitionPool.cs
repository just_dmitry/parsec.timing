﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Группа соревнований, проходящих одновременно (общий старт)
    /// </summary>
    public class CompetitionPool
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [Required]
        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        [Required]
        public byte SortOrder { get; set; }

        public bool IsLocked { get; set; }

        public ProcessingStatus ParticipationProcessingStatus { get; set; }

        public DateTimeOffset ParticipationProcessingTime { get; set; }

        public ProcessingStatus ResultProcessingStatus { get; set; }

        public DateTimeOffset ResultProcessingTime { get; set; }

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompetitionPool>(entity =>
            {
                entity.HasAlternateKey(x => new { x.MultiEventTournamentId, x.Name });
                entity.Property(x => x.ParticipationProcessingTime).HasDateTimeOffsetPrecision(2);
                entity.Property(x => x.ResultProcessingTime).HasDateTimeOffsetPrecision(2);
            });
        }
    }
}

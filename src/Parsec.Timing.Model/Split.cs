﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// "Промежуточная отметка" на дистанции
    /// </summary>
    public class Split
    {
        [Key]
        public Guid Id { get; set; }

        public Guid CompetitionId { get; set; }

        [ForeignKey(nameof(CompetitionId))]
        public virtual Competition Competition { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string NameInternational { get; set; }

        [Required]
        public short SortOrder { get; set; }

        public Guid? InstantArrivalCheckpointId { get; set; }

        [ForeignKey(nameof(InstantArrivalCheckpointId))]
        public virtual Checkpoint InstantArrivalCheckpoint { get; set; }

        public ResultType ResultType { get; set; }

        public long ResultValue { get; set; }

        /// <summary>
        /// "Исходный" результат (как получен от источника)
        /// </summary>
        [NotMapped]
        public TimeOrLength Result
        {
            get
            {
                return new TimeOrLength(ResultType, ResultValue);
            }

            set
            {
                ResultType = value.Type;
                ResultValue = value.EncodedValue;
            }
        }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Split>(entity =>
            {
                entity.HasIndex(x => new { x.CompetitionId, x.Name }).IsUnique();
            });
        }
    }
}

﻿namespace Parsec.Timing.Model
{
    public enum ProcessingStatus : byte
    {
        None = 0,
        WithWarnings = 1,
        Success = 2,
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Участник соревнований ("заявка")
    /// </summary>
    public class Participant
    {
        [Key]
        public Guid Id { get; set; }

        public Guid PersonId { get; set; }

        [ForeignKey(nameof(PersonId))]
        public virtual Person Person { get; set; }

        public Guid CompetitionId { get; set; }

        [ForeignKey(nameof(CompetitionId))]
        public virtual Competition Competition { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthBib)]
        public string Bib { get; set; }

        /// <summary>
        /// Битовая маска флагов-категорий
        /// </summary>
        public int Categories { get; set; }

        public int SortOrder { get; set; }

        public DateTimeOffset InsertTime { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Participant>(entity =>
            {
                entity.HasIndex(t => new { t.CompetitionId, t.Bib });
                entity.HasIndex(t => new { t.CompetitionId, t.PersonId });
                entity.Property(t => t.InsertTime).HasDateTimeOffsetPrecision(2);
            });
        }
    }
}

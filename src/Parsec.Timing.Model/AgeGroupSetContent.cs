﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class AgeGroupSetContent
    {
        [Required]
        public Guid AgeGroupSetId { get; set; }

        [ForeignKey(nameof(AgeGroupSetId))]
        public virtual AgeGroupSet AgeGroupSet { get; set; }

        [Required]
        public Guid AgeGroupId { get; set; }

        [ForeignKey(nameof(AgeGroupId))]
        public virtual AgeGroup AgeGroup { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgeGroupSetContent>(entity =>
            {
                entity.HasKey(t => new { t.AgeGroupSetId, t.AgeGroupId });
                entity.HasOne(t => t.AgeGroup).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Официальное самостоятельное "состязание" - конкретное соревнование по конкретному положению
    /// </summary>
    public class TournamentCompetition
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid TournamentId { get; set; }

        [ForeignKey(nameof(TournamentId))]
        public virtual Tournament Tournament { get; set; }

        [Required]
        public Guid CompetitionId { get; set; }

        [ForeignKey(nameof(CompetitionId))]
        public virtual Competition Competition { get; set; }

        public bool AutoParticipation { get; set; }

        public bool AbsolutePositions { get; set; }

        public Guid? AgeGroupSetId { get; set; }

        [ForeignKey(nameof(AgeGroupSetId))]
        public virtual AgeGroupSet AgeGroupSet { get; set; }

        public bool AgeGroupByFullBirthDate { get; set; }

        /// <summary>
        /// Дата для вычисления возраста
        /// </summary>
        public DateTime AgeGroupReferenceDate { get; set; }

        public Guid DefaultParticipantStatusId { get; set; }

        [ForeignKey(nameof(DefaultParticipantStatusId))]
        public virtual ParticipantStatus DefaultParticipantStatus { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TournamentCompetition>(entity =>
            {
                entity.HasAlternateKey(x => new { x.CompetitionId, x.TournamentId });
                entity.HasOne(t => t.AgeGroupSet).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Competition).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Tournament).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(t => t.DefaultParticipantStatus).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

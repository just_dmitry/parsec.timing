﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public class ParticipantAgeRanking
    {
        [Key]
        public long Id { get; set; }

        public Guid ParticipantId { get; set; }

        [ForeignKey(nameof(ParticipantId))]
        public virtual Participant Participant { get; set; }

        [Required]
        public Guid TournamentCompetitionId { get; set; }

        [ForeignKey(nameof(TournamentCompetitionId))]
        public virtual TournamentCompetition TournamentCompetition { get; set; }

        public Guid AgeGroupId { get; set; }

        [ForeignKey(nameof(AgeGroupId))]
        public virtual AgeGroup AgeGroup { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthRank)]
        public string Rank { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantAgeRanking>(entity =>
            {
                entity.HasOne(t => t.Participant).WithMany().OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(t => t.TournamentCompetition).WithMany().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.AgeGroup).WithMany().OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

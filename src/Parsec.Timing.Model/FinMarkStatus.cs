﻿namespace Parsec.Timing.Model
{
    public enum FinMarkStatus : byte
    {
        Default = 0,

        ErrorUnknownBib = 1,

        ErrorWrongBib = 2,

        Duplicate = 3,
    }
}

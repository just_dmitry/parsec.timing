﻿namespace Parsec.Timing.Model
{
    public enum ProcessingErrorType : byte
    {
        None = 0,

        ParticipantOverrideBibNotFound = 1,

        ResultError = 128,
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;

    public struct TimeOrLength : IComparable, IComparable<TimeOrLength>, IEquatable<TimeOrLength>
    {
        public static readonly TimeOrLength Zero = new TimeOrLength(ResultType.Unknown, 0);

        public TimeOrLength(TimeSpan time)
        {
            Type = ResultType.Time1000;
            EncodedValue = (long)time.TotalMilliseconds;
        }

        public TimeOrLength(decimal length)
        {
            Type = ResultType.Length1000;
            EncodedValue = (long)(length * 1000);
        }

        public TimeOrLength(ResultType type, long encodedValue)
        {
            if (type == ResultType.Unknown && encodedValue != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(encodedValue), "Must be Zero");
            }

            Type = type;
            EncodedValue = encodedValue;
        }

        public ResultType Type { get; }

        public long EncodedValue { get; }

        public bool IsTime
        {
            get { return Type == ResultType.Time1000; }
        }

        public bool IsLength
        {
            get { return Type == ResultType.Length1000; }
        }

        public TimeSpan Time
        {
            get
            {
                if (!IsTime)
                {
                    throw new InvalidOperationException("Not a Time");
                }

                return TimeSpan.FromMilliseconds(EncodedValue);
            }
        }

        public decimal Length
        {
            get
            {
                if (!IsLength)
                {
                    throw new InvalidOperationException("Not a Length");
                }

                return new decimal(EncodedValue) / 1000;
            }
        }

        public static bool operator ==(TimeOrLength value1, TimeOrLength value2)
        {
            return value1.CompareTo(value2) == 0;
        }

        public static bool operator !=(TimeOrLength value1, TimeOrLength value2)
        {
            return value1.CompareTo(value2) != 0;
        }

        public static bool operator >(TimeOrLength value1, TimeOrLength value2)
        {
            return value1.CompareTo(value2) > 0;
        }

        public static bool operator <(TimeOrLength value1, TimeOrLength value2)
        {
            return value1.CompareTo(value2) < 0;
        }

        public static bool operator >=(TimeOrLength value1, TimeOrLength value2)
        {
            return value1.CompareTo(value2) >= 0;
        }

        public static bool operator <=(TimeOrLength value1, TimeOrLength value2)
        {
            return value1.CompareTo(value2) <= 0;
        }

        public int CompareTo(object obj)
        {
            var other = (TimeOrLength)obj;
            return CompareTo(other);
        }

        public int CompareTo(TimeOrLength other)
        {
            var res = Type.CompareTo(other.Type);
            if (res == 0)
            {
                res = EncodedValue.CompareTo(other.EncodedValue);
            }

            return res;
        }

        public bool Equals(TimeOrLength other)
        {
            return this.Type == other.Type && this.EncodedValue == other.EncodedValue;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is TimeOrLength tol))
            {
                return false;
            }

            return this.Type == tol.Type && this.EncodedValue == tol.EncodedValue;
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode() ^ EncodedValue.GetHashCode();
        }

        public override string ToString()
        {
            if (IsTime)
            {
                var val = Time.ToString();
                if (Time.Milliseconds > 0)
                {
                    val = val.TrimEnd('0');
                }

                return val;
            }

            if (IsLength)
            {
                return Length.ToString();
            }

            return "-?-?-";
        }
    }
}

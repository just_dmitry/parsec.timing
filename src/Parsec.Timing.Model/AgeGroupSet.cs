﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// "Линейка" возрастных групп, относящаяся к соревнованию
    /// </summary>
    public class AgeGroupSet
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        public Guid MultiEventTournamentId { get; set; }

        [ForeignKey(nameof(MultiEventTournamentId))]
        public virtual MultiEventTournament MultiEventTournament { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgeGroupSet>(entity =>
            {
                entity.HasAlternateKey(x => new { x.MultiEventTournamentId, x.Name });
            });
        }
    }
}

﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    [AllPropertiesAreNotNull(nameof(CompetitionPoolId), nameof(CheckpointId))]
    public class FinGroup
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        public bool IsDisabled { get; set; }

        public DateTimeOffset Created { get; set; }

        public DateTimeOffset LastSeen { get; set; }

        public int MarksCount { get; set; }

        public Guid? CompetitionPoolId { get; set; }

        [ForeignKey(nameof(CompetitionPoolId))]
        public virtual CompetitionPool CompetitionPool { get; set; }

        public Guid? CheckpointId { get; set; }

        [ForeignKey(nameof(CheckpointId))]
        public virtual Checkpoint Checkpoint { get; set; }

        [MaxLength(100)]
        public string IpAddress { get; set; }

        [MaxLength(100)]
        public string ClientNameVersion { get; set; }

        public ResultType ResultShiftType { get; set; }

        public long ResultShiftValue { get; set; }

        [NotMapped]
        public TimeOrLength ResultShift
        {
            get
            {
                return new TimeOrLength(ResultShiftType, ResultShiftValue);
            }

            set
            {
                ResultShiftType = value.Type;
                ResultShiftValue = value.EncodedValue;
            }
        }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthComment)]
        public string Comment { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FinGroup>(entity =>
            {
                entity.HasOne(x => x.CompetitionPool).WithMany().IsRequired(false).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(x => x.Checkpoint).WithMany().IsRequired(false).OnDelete(DeleteBehavior.Restrict);
                entity.Property(x => x.Created).HasDateTimeOffsetPrecision(2);
            });
        }
    }
}

namespace Microsoft.EntityFrameworkCore
{
    using System;
    using EntityFrameworkCore.Metadata.Builders;

    public static class EntityFrameworkExtensions
    {
        // http://stackoverflow.com/questions/30388695/entity-framework-7-set-decimal-precision-for-model-builder
        public static PropertyBuilder<decimal> HasDecimalPrecision(this PropertyBuilder<decimal> propertyBuilder, int precision, int scale)
        {
            return propertyBuilder.HasColumnType($"decimal({precision},{scale})");
        }

        public static PropertyBuilder<DateTimeOffset> HasDateTimeOffsetPrecision(this PropertyBuilder<DateTimeOffset> propertyBuilder, int precision)
        {
            return propertyBuilder.HasColumnType($"datetimeoffset({precision})");
        }

        public static PropertyBuilder<DateTimeOffset?> HasDateTimeOffsetPrecision(this PropertyBuilder<DateTimeOffset?> propertyBuilder, int precision)
        {
            return propertyBuilder.HasColumnType($"datetimeoffset({precision})");
        }

        public static PropertyBuilder<DateTime> HasDateTimePrecision(this PropertyBuilder<DateTime> propertyBuilder, int precision)
        {
            return propertyBuilder.HasColumnType($"datetime2({precision})");
        }
    }
}
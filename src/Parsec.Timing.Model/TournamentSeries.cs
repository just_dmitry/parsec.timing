﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Серия соревнований, проводящихся ежегодно (=Бренд)
    /// </summary>
    public class TournamentSeries
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TournamentSeries>(entity =>
            {
                entity.HasAlternateKey(x => x.Name);
            });
        }
    }
}

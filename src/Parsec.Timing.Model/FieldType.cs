﻿namespace Parsec.Timing.Model
{
    public enum FieldType : byte
    {
        String = 0,
        YesNo = 1,
        Byte = 2,
        Int32 = 3,

        Phone = 10,
        Email = 11,

        City = 20,
        Region = 21,
        Country = 22,
        MoscowDistrict = 23,
        Team = 24,
        Coaches = 25,
        SportsCategory = 26,
    }
}

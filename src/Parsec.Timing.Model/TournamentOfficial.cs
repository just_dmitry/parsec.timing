﻿namespace Parsec.Timing.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Официальные лица соревнования (гл.судья, рефери, тех.делегат и т.п.)
    /// </summary>
    public class TournamentOfficial
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid TournamentId { get; set; }

        [ForeignKey(nameof(TournamentId))]
        public virtual Tournament Tournament { get; set; }

        public short SortOrder { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string JobTitle { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string Name { get; set; }

        [MaxLength(ModelSettings.FieldMaxLengthOther)]
        public string Grade { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TournamentOfficial>(entity =>
            {
                entity.HasIndex(x => new { x.TournamentId, x.SortOrder });
            });
        }
    }
}

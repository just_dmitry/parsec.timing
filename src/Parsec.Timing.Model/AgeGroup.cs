﻿namespace Parsec.Timing.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class AgeGroup
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string Name { get; set; }

        [Required]
        [MaxLength(ModelSettings.FieldMaxLengthName)]
        public string ShortName { get; set; }

        public byte MinAge { get; set; }

        public byte MaxAge { get; set; }

        public static void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            // Nothing
        }
    }
}

﻿namespace Parsec.Timing.Readers.MyLaps.V21
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;

    public class MylapsMessageHandler
    {
        public const char MessageDelimiter = '$';

        public const char MessagePartDelimiter = '@';

        private readonly ILogger logger;

        public MylapsMessageHandler(ILogger<MylapsMessageHandler> logger)
        {
            this.logger = logger;
        }

        public delegate Task NewTagVisitsHandler(MylapsMessageHandler source, NewTagVisitsEventArgs args);

        public event NewTagVisitsHandler NewTagVisits;

        public async Task HandleMessageAsync(ReaderListener readerListener, NewMessageEventArgs args)
        {
            var parts = args.Message.Split(new[] { MessagePartDelimiter }, StringSplitOptions.RemoveEmptyEntries);

            var name = parts[0];
            var type = parts[1];

            if (type == "Pong")
            {
                var responseText = string.Concat(
                    "Parsec",
                    MessagePartDelimiter,
                    "AckPong",
                    MessagePartDelimiter,
                    "Version2.1",
                    MessagePartDelimiter,
                    "c|n|t|d|dv|dm|prof|l",
                    MessagePartDelimiter);
                await args.SendResponseAsync(responseText);
                return;
            }

            if (type == "Passing")
            {
                // Sample: Passing@c=KF06288|n=KF06288|t=19:00:15.518|d=180202|dv=1|dm=003056A8F84A
                var reads = parts
                    .Take(parts.Length - 1) // do not take last one
                    .Skip(2) // skip name and type
                    .Select(x => x.Split('|').ToDictionary(y => y.Split('=')[0], y => y.Split('=')[1]))
                    .Select(x => new MylapsTagData
                    {
                        Tag = x["c"],
                        Time = DateTimeOffset.ParseExact(x["d"] + " " + x["t"] + "Z", "yyMMdd HH%:mm%:ss%.fffZ", CultureInfo.InvariantCulture),
                    })
                    .ToArray();

                var msgNumber = long.Parse(parts.Last());

                await OnNewTagVisitsAsync(name, msgNumber, reads);

                var responseText = string.Concat("Parsec", MessagePartDelimiter, "AckPassing", MessagePartDelimiter, msgNumber, MessagePartDelimiter);
                await args.SendResponseAsync(responseText);
                return;
            }

            logger.LogError($"Unknown message type '{type}' (ignored)");
        }

        protected virtual async Task OnNewTagVisitsAsync(string name, long sequence, MylapsTagData[] data)
        {
            var args = new NewTagVisitsEventArgs
            {
                Name = name,
                Sequence = sequence,
                TagVisits = data,
            };

            await NewTagVisits?.Invoke(this, args);
        }
    }
}

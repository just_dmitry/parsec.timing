﻿namespace Parsec.Timing.Readers.MyLaps.V21
{
    using System;

    public class MylapsTagData
    {
        public string Tag { get; set; }

        public DateTimeOffset Time { get; set; }
    }
}

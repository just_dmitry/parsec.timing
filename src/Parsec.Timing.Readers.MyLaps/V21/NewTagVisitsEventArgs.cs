﻿namespace Parsec.Timing.Readers.MyLaps.V21
{
    using System;

    public class NewTagVisitsEventArgs : EventArgs
    {
        public string Name { get; set; }

        public long Sequence { get; set; }

        public MylapsTagData[] TagVisits { get; set; }
    }
}

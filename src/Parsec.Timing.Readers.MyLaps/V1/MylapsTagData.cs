﻿namespace Parsec.Timing.Readers.MyLaps.V1
{
    using System;

    public class MylapsTagData
    {
        public string Tag { get; set; }

        public TimeSpan Time { get; set; }

        public int Unknown1 { get; set; }

        public int Unknown2 { get; set; }

        public string Antenna { get; set; }
    }
}

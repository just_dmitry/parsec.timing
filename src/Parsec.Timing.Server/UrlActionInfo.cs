﻿namespace Parsec.Timing.Server
{
    using System;
    using System.Reflection;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.AspNetCore.Routing;

    public class UrlActionInfo : UrlActionContext
    {
        public UrlActionInfo(Type controller, string action)
        {
            Controller = controller.Name;
            Action = action;

            var area = AreaModelConvention.GetAreaName(controller);
            Add("area", area);
        }

        public UrlActionInfo Add(string name, object value)
        {
            if (value != null)
            {
                if (Values == null)
                {
                    Values = new RouteValueDictionary();
                }

                ((RouteValueDictionary)Values)[name] = value;
            }

            return this;
        }
    }
}

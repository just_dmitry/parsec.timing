﻿namespace Parsec.Timing.Server.ReportGenerators
{
    using System;
    using System.Collections.Generic;
    using Model;

    public class ParticipantListViewModel
    {
        public Report Report { get; set; }

        public Tournament Tournament { get; set; }

        public List<Competition> Competitions { get; set; }

        public List<TournamentCompetition> TournamentCompetitions { get; set; }

        public Dictionary<Guid, Participant> Participants { get; set; }

        public List<MultiEventTournamentField> MultiEventTournamentFields { get; set; }

        public Dictionary<Guid, ParticipantField[]> ParticipantFields { get; set; }

        public List<ParticipantRanking> ParticipantRankings { get; set; }

        public List<ParticipantAgeRanking> ParticipantAgeRankings { get; set; }

        public Dictionary<Guid, AgeGroup> AgeGroups { get; set; }

        public Dictionary<Guid, Field> Fields { get; set; }

        public Dictionary<Guid, ParticipantStatus> Statuses { get; set; }
    }
}

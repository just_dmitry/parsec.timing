﻿namespace Parsec.Timing.Server.ReportGenerators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    public class ParticipantList
    {
        private readonly TimingDb db;

        public ParticipantList(TimingDb db)
        {
            this.db = db;
        }

        public async Task<ParticipantListViewModel> GenerateAsync(Report report, ReportParamsParticipantList prms, Guid tournamentId)
        {
            var model = new ParticipantListViewModel();

            model.Report = report;

            model.Tournament = await db.Tournaments.AsNoTracking().SingleAsync(x => x.Id == tournamentId);

            model.TournamentCompetitions = await db.TournamentCompetitions.AsNoTracking()
                .Include(x => x.Competition)
                .Where(x => x.TournamentId == tournamentId)
                .OrderBy(x => x.Competition.SortOrder)
                .ToListAsync();

            var tournamentCompetitionsId = model.TournamentCompetitions.Select(x => x.Id).ToArray();

            model.Competitions = model.TournamentCompetitions.Select(x => x.Competition).ToList();

            var competitonIds = model.Competitions.Select(x => x.Id).ToArray();
            var competitonKeys = model.Competitions.Select(x => x.Key).ToArray();
            var fieldIds = report.Fields.Select(x => x.FieldId).ToArray();

            model.Fields = await db.Fields.AsNoTracking().Where(x => fieldIds.Contains(x.Id)).ToDictionaryAsync(x => x.Id);

            model.Participants = await db.Participants.AsNoTracking()
                .Include(x => x.Person)
                .Where(x => competitonIds.Contains(x.CompetitionId))
                .ToDictionaryAsync(x => x.Id);

            if (model.Fields.Values.Any(x => x.Source == FieldSource.ParticipantAgeRanking))
            {
                model.ParticipantAgeRankings = await db.ParticipantAgeRankings.AsNoTracking()
                    .Where(x => tournamentCompetitionsId.Contains(x.TournamentCompetitionId))
                    .ToListAsync();
                model.AgeGroups = await db.AgeGroups.AsNoTracking().ToDictionaryAsync(x => x.Id);
            }
            else
            {
                model.ParticipantAgeRankings = new List<ParticipantAgeRanking>();
                model.AgeGroups = new Dictionary<Guid, AgeGroup>();
            }

            model.ParticipantRankings = await db.ParticipantRankings.AsNoTracking()
                .Where(x => tournamentCompetitionsId.Contains(x.TournamentCompetitionId))
                .OrderBy(x => x.SortOrder)
                .ThenBy(x => x.ParticipantId) // for stable sorting
                .ToListAsync();

            model.ParticipantFields = (await db.ParticipantFields.AsNoTracking()
                .Where(x => competitonKeys.Contains(x.CompetitionKey))
                .ToListAsync())
                .GroupBy(x => x.ParticipantId)
                .ToDictionary(x => x.Key, x => x.ToArray());

            model.MultiEventTournamentFields = await db.MultiEventTournamentFields.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == model.Tournament.MultiEventTournamentId)
                .ToListAsync();

            model.Statuses = await db.ParticipantStatuses.AsNoTracking().ToDictionaryAsync(x => x.Id);

            return model;
        }
    }
}

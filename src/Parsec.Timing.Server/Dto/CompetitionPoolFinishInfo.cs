﻿namespace Parsec.Timing.Server.Dto
{
    using System;
    using System.Collections.Generic;
    using Parsec.Timing.Model;

    public class CompetitionPoolFinishInfo
    {
        public CompetitionPool CompetitionPool { get; set; }

        public List<Checkpoint> Checkpoints { get; set; }

        public List<FinGroup> FinGroups { get; set; }
    }
}

﻿namespace Parsec.Timing.Server.Dto
{
    using System;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public class ServerResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public string[] Errors { get; set; }

        public string NewUri { get; set; }

        public static ServerResponse Complete(string message)
        {
            return new ServerResponse
            {
                Success = true,
                Message = message,
            };
        }

        public static ServerResponse Fail(ModelStateDictionary modelState)
        {
            if (modelState.IsValid)
            {
                throw new InvalidOperationException("Can't fail with Model.IsValid == true");
            }

            return new ServerResponse
            {
                Success = false,
                Errors = modelState.SelectMany(x => x.Value.Errors.Select(y => y.ErrorMessage)).ToArray(),
            };
        }

        public static ServerResponse Fail(params string[] errors)
        {
            if (errors == null || errors.Length == 0)
            {
                throw new ArgumentNullException(nameof(errors));
            }

            return new ServerResponse
            {
                Success = false,
                Errors = errors,
            };
        }
    }
}

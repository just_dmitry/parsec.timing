declare module server {
	interface ServerResponse {
		success: boolean;
		message: string;
		errors: string[];
		newUri: string;
	}
}

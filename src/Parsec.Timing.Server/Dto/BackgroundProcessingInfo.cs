﻿namespace Parsec.Timing.Server.Dto
{
    using System;
    using System.Collections.Generic;

    public class BackgroundProcessingInfo
    {
        public bool IsRunning { get; set; }

        public List<ActiveJobInfo> ActiveJobs { get; set; }

        public List<CompletedJobInfo> CompletedJobs { get; set; }

        public class ActiveJobInfo
        {
            public ActiveJobInfo(string name, byte progress)
            {
                this.Name = name;
                this.Progress = progress;
            }

            public string Name { get; set; }

            public byte Progress { get; set; }
        }

        public class CompletedJobInfo
        {
            public CompletedJobInfo(string name, string completedAt, bool success)
            {
                Name = name;
                CompletedAt = completedAt;
                Success = success;
            }

            public string Name { get; set; }

            public string CompletedAt { get; set; }

            public bool Success { get; set; }
        }
    }
}

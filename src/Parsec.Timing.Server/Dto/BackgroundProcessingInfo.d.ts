declare module server {
	interface ActiveJobInfo {
		name: string;
		progress: any;
	}
	interface CompletedJobInfo {
		name: string;
		completedAt: string;
		success: boolean;
	}
	interface BackgroundProcessingInfo {
		isRunning: boolean;
        activeJobs: ActiveJobInfo[];
        completedJobs: CompletedJobInfo[];
	}
}

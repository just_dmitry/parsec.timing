﻿namespace Parsec.Timing.Server.Dto
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;

    public class FinData
    {
        public const string CommentDelimiter = "//";

        private static readonly Regex MuraveyFormat = new Regex(@"^ \s* (\d+)? \s ([\s\d]\d) \s ([\s\d]\d) \s ([\s\d]\d) \s* $", RegexOptions.IgnorePatternWhitespace);

        private static readonly Regex FinCatchFormat = new Regex(@"^ (\d{1,2}) : (\d{1,2}) : (\d{1,2}) \t (\d+)? $", RegexOptions.IgnorePatternWhitespace);

        private static readonly Regex AnyWhitespace = new Regex(@"\s+");

        public FinData()
        {
            // Nothing
        }

        public FinData(string bib, TimeSpan result)
        {
            this.Bib = bib;
            this.Result = result;
        }

        public string Bib { get; set; }

        public TimeSpan Result { get; set; }

        public string Comment { get; set; }

        public static FinData TryParse(string input)
        {
            if (TryParse(input, out FinData retval))
            {
                return retval;
            }

            return null;
        }

        public static bool TryParse(string input, out FinData data)
        {
            data = null;

            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            var m = FinCatchFormat.Match(input);
            if (m.Success)
            {
                var hr = int.Parse(m.Groups[1].Value.Trim());
                var min = int.Parse(m.Groups[2].Value.Trim());
                var sec = int.Parse(m.Groups[3].Value.Trim());
                var bib = m.Groups[4].Value.Trim();
                data = new FinData
                {
                    Bib = string.IsNullOrEmpty(bib) ? null : bib,
                    Result = new TimeSpan(hr, min, sec),
                };
                return true;
            }

            m = MuraveyFormat.Match(input);
            if (m.Success)
            {
                var bib = m.Groups[1].Value.Trim();
                var hr = int.Parse(m.Groups[2].Value.Trim());
                var min = int.Parse(m.Groups[3].Value.Trim());
                var sec = int.Parse(m.Groups[4].Value.Trim());
                data = new FinData
                {
                    Bib = string.IsNullOrEmpty(bib) ? null : bib,
                    Result = new TimeSpan(hr, min, sec),
                };
                return true;
            }

            input = AnyWhitespace.Replace(input, " ");
            var parsed = new FinData();

            var commentStart = input.IndexOf(CommentDelimiter);
            if (commentStart != -1)
            {
                var text = input.Substring(commentStart + 2).Trim();
                if (!string.IsNullOrEmpty(text))
                {
                    parsed.Comment = text;
                }

                input = input.Substring(0, commentStart).Trim();
            }

            input = input
                .Replace(';', ':').Replace('Ж', ':').Replace('ж', ':')
                .Replace(',', '.').Replace('Ю', '.').Replace('ю', '.')
                .Trim();

            var parts = input.Split(' ');
            if (parts.Length > 2)
            {
                return false;
            }

            if (parts[0].IndexOf(':') != -1
                && TimeSpan.TryParse(parts[0], CultureInfo.InvariantCulture, out TimeSpan result1))
            {
                parsed.Bib = parts.Length == 1 ? null : parts[1];
                parsed.Result = result1;
                data = parsed;
                return true;
            }

            if (parts.Length == 1)
            {
                return false;
            }

            if (parts[1].IndexOf(':') != -1
                && TimeSpan.TryParse(parts[1], CultureInfo.InvariantCulture, out TimeSpan result2))
            {
                parsed.Bib = parts[0];
                parsed.Result = result2;
                data = parsed;
                return true;
            }

            return false;
        }
    }
}

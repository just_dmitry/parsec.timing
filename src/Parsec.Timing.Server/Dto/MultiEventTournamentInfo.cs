﻿namespace Parsec.Timing.Server.Dto
{
    using System;
    using System.Collections.Generic;
    using Model;

    public class MultiEventTournamentInfo
    {
        public MultiEventTournament MultiEventTournament { get; set; }

        public List<CompetitionPool> CompetitionPools { get; set; }

        public List<Competition> Competitions { get; set; }

        public List<TournamentCompetition> TournamentCompetitions { get; set; }

        public string[] FieldTitles { get; set; }

        public List<Report> Reports { get; set; }

        public string[] AgeGroupSetNames { get; set; }

        public List<CategoryFlag> Categories { get; set; }

        public List<Checkpoint> Checkpoints { get; set; }

        public List<FinGroup> UnusedFinGroups { get; set; }
    }
}

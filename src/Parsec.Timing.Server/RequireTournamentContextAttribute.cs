﻿namespace Parsec.Timing.Server
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Services;

    public class RequireTournamentContextAttribute : Attribute, IFilterFactory
    {
        public RequireTournamentContextAttribute(bool requireUnlocked = true)
        {
            RequireUnlocked = requireUnlocked;
        }

        public bool IsReusable { get; } = true;

        public bool RequireUnlocked { get; set; }

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var currentTournamentService = serviceProvider.GetRequiredService<ICurrentTournamentService>();
            var urlHelperFactory = serviceProvider.GetRequiredService<IUrlHelperFactory>();
            var logger = serviceProvider.GetRequiredService<ILogger<RequireTournamentContextFilter>>();

            return new RequireTournamentContextFilter(currentTournamentService, urlHelperFactory, RequireUnlocked, logger);
        }

        private class RequireTournamentContextFilter : IActionFilter
        {
            private readonly ICurrentTournamentService currentTournamentService;

            private readonly IUrlHelperFactory urlHelperFactory;

            private readonly ILogger logger;

            private readonly bool requireUnlocked;

            public RequireTournamentContextFilter(
                ICurrentTournamentService currentTournamentService,
                IUrlHelperFactory urlHelperFactory,
                bool requireUnlocked,
                ILogger logger)
            {
                this.currentTournamentService = currentTournamentService;
                this.urlHelperFactory = urlHelperFactory;
                this.requireUnlocked = requireUnlocked;
                this.logger = logger;
            }

            public void OnActionExecuting(ActionExecutingContext context)
            {
                if (!currentTournamentService.IsSet)
                {
                    var urlHelper = urlHelperFactory.GetUrlHelper(context);
                    var url = urlHelper.Action(Areas.Default.Home.SetActiveRoute());
                    logger.LogWarning($"There is no current MultiEventTournament, redirecting to {url}");
                    context.Result = new RedirectResult(url);
                }

                if (requireUnlocked && currentTournamentService.IsLocked)
                {
                    var urlHelper = urlHelperFactory.GetUrlHelper(context);
                    var url = urlHelper.Action(Areas.Default.Home.LockedUnavailableRoute());
                    logger.LogWarning($"Current MultiEventTournament is Locked, redirecting to {url}");
                    context.Result = new RedirectResult(url);
                }
            }

            public void OnActionExecuted(ActionExecutedContext context)
            {
                // Nothing
            }
        }
    }
}

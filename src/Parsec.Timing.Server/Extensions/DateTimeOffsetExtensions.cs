namespace System
{
    using System;

    public static class DateTimeOffsetExtensions
    {
        private static readonly TimeZoneInfo MoscowZoneInfo;

        static DateTimeOffsetExtensions()
        {
            try
            {
                MoscowZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
            }
            catch (Exception)
            {
                MoscowZoneInfo = TimeZoneInfo.Local;
            }
        }

        /// <summary>
        /// Переводит время в часовой пояс Москвы (абсолютная точка на оси времени остается)
        /// </summary>
        public static DateTimeOffset ToMoscowTime(this DateTimeOffset value)
        {
            return TimeZoneInfo.ConvertTime(value, MoscowZoneInfo);
        }

        /// <summary>
        /// Переписывает время в часовой пояс Москвы (локальное время остается, абсолютная точка сдвигается)
        /// </summary>
        public static DateTimeOffset AsMoscowTime(this DateTimeOffset value)
        {
            return new DateTimeOffset(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second, MoscowZoneInfo.BaseUtcOffset);
        }

        public static string ToStringWithZone(this DateTimeOffset value)
        {
            return string.Format("{0:G} {0:zzz}", value);
        }

        /// <summary>
        /// Converts to string with pattern 'dd MMM HH:mm:ss'
        /// </summary>
        public static string ToFriendString(this DateTimeOffset value)
        {
            if ((value.Hour == 0 && value.Minute == 0)
                || value.TimeOfDay == value.Offset)
            {
                return value.ToString("dd MMM");
            }

            return value.ToString("dd MMM HH:mm");
        }

        /// <summary>
        /// Converts to string with time only (pattern 'H:mm:ss') if value is less than 3 hour before now,
        ///   and to string with pattern 'dd MMM HH:mm' otherwise
        /// </summary>
        public static string ToFastFriendString(this DateTimeOffset value)
        {
            var elapsed = DateTimeOffset.Now.Subtract(value);
            if (elapsed.Duration().TotalHours < 3)
            {
                return value.ToString("H:mm:ss");
            }

            return value.ToFriendString();
        }

        public static string ToIsoString(this DateTimeOffset value)
        {
            return value.ToString("o");
        }

        public static string ToFriendPeriod(this DateTimeOffset value1, DateTimeOffset value2)
        {
            return value1.Date.ToFriendPeriod(value2.Date);
        }
    }
}

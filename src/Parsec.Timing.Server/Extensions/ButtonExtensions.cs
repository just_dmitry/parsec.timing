﻿namespace BootstrapMvc
{
    using BootstrapMvc.Core;

    public static class ButtonExtensions
    {
        public static IItemWriter<T> GoBack<T>(this IItemWriter<T> target)
            where T : Buttons.Button
        {
            return target.Attribute("onclick", "window.history.back();");
        }

        public static IItemWriter<T, TContent> GoBack<T, TContent>(this IItemWriter<T, TContent> target)
            where T : ContentElement<TContent>, ILink
            where TContent : DisposableContent
        {
            return target.Attribute("onclick", "window.history.back();");
        }
    }
}

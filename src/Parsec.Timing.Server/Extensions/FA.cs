﻿namespace Microsoft.AspNetCore.Mvc
{
    using System;
    using Microsoft.AspNetCore.Html;
    using Microsoft.AspNetCore.Mvc.Rendering;

    public static class FA
    {
        public static IHtmlContent Check(string tooltip = null)
        {
            return Get("check", tooltip);
        }

        public static IHtmlContent CheckSuccessFixed(string tooltip = null)
        {
            return Get("check fa-fw text-success", tooltip);
        }

        public static IHtmlContent CheckSuccess(string tooltip = null)
        {
            return Get("check text-success", tooltip);
        }

        public static IHtmlContent ExclamationTriangle(string tooltip = null)
        {
            return Get("exclamation-triangle", tooltip);
        }

        public static IHtmlContent Home(string tooltip = null)
        {
            return Get("home", tooltip);
        }

        public static IHtmlContent HourglassStart(string tooltip = null)
        {
            return Get("hourglass-start", tooltip);
        }

        public static IHtmlContent Refresh(string tooltip = null)
        {
            return Get("refresh", tooltip);
        }

        public static IHtmlContent RefreshAnimated(string tooltip = null)
        {
            var refresh = (TagBuilder)Get("refresh", tooltip);
            refresh.AddCssClass("fa-spin");
            return refresh;
        }

        public static IHtmlContent Times(string tooltip = null)
        {
            return Get("times", tooltip);
        }

        public static IHtmlContent TimesDanger(string tooltip = null)
        {
            return Get("times text-danger", tooltip);
        }

        public static IHtmlContent TimesDangerFixed(string tooltip = null)
        {
            return Get("times fa-fw text-danger", tooltip);
        }

        public static IHtmlContent Plus(string tooltip = null)
        {
            return Get("plus", tooltip);
        }

        public static IHtmlContent Pencil(string tooltip = null)
        {
            return Get("pencil", tooltip);
        }

        public static IHtmlContent Trash(string tooltip = null)
        {
            return Get("trash-o", tooltip);
        }

        public static IHtmlContent Lock(string tooltip = null)
        {
            return Get("lock", tooltip);
        }

        public static IHtmlContent LockDanger(string tooltip = null)
        {
            return Get("lock text-danger", tooltip);
        }

        public static IHtmlContent LockWarning(string tooltip = null)
        {
            return Get("lock text-warning", tooltip);
        }

        public static IHtmlContent Unlock(string tooltip = null)
        {
            return Get("unlock-alt", tooltip);
        }

        public static IHtmlContent Question(string tooltip = null)
        {
            return Get("question", tooltip);
        }

        public static IHtmlContent QuestionDanger(string tooltip = null)
        {
            return Get("question text-danger", tooltip);
        }

        public static IHtmlContent Info(string tooltip = null)
        {
            return Get("info", tooltip);
        }

        public static IHtmlContent InfoMuted(string tooltip = null)
        {
            return Get("info text-muted", tooltip);
        }

        public static IHtmlContent InfoCircle(string tooltip = null)
        {
            return Get("info-circle", tooltip);
        }

        public static IHtmlContent InfoCircleMuted(string tooltip = null)
        {
            return Get("info-circle text-muted", tooltip);
        }

        public static IHtmlContent Ban(string tooltip = null)
        {
            return Get("ban", tooltip);
        }

        public static IHtmlContent BanDanger(string tooltip = null)
        {
            return Get("ban text-danger", tooltip);
        }

        private static IHtmlContent Get(string className, string tooltip = null)
        {
            if (string.IsNullOrEmpty(className))
            {
                throw new ArgumentNullException(nameof(className));
            }

            var tb = new TagBuilder("i");
            tb.AddCssClass("fa");
            tb.AddCssClass("fa-" + className);
            if (!string.IsNullOrEmpty(tooltip))
            {
                tb.Attributes.Add("title", tooltip);
            }

            return tb;
        }
    }
}

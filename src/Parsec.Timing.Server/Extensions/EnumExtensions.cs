namespace System
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;

    public static class EnumExtensions
    {
        private static Dictionary<Type, Dictionary<object, string>> typeDescriptions = new Dictionary<Type, Dictionary<object, string>>();

        public static IDictionary<object, string> GetDescriptions(this Type enumType)
        {
            var enumTypeInfo = enumType.GetTypeInfo();

            if (!enumTypeInfo.IsEnum)
            {
                throw new ArgumentException("Is not a Enum type", nameof(enumTypeInfo));
            }

            if (typeDescriptions.ContainsKey(enumType))
            {
                return typeDescriptions[enumType];
            }

            var dic = new Dictionary<object, string>();

            var enumValues = enumTypeInfo.GetEnumValues();
            var enumFields = enumTypeInfo.GetFields();

            foreach (var val in enumValues)
            {
                var valName = Enum.GetName(enumType, val);
                var field = enumFields.First(x => x.Name == valName);
                var attr = field.GetCustomAttribute<DescriptionAttribute>();

                dic.Add(val, attr == null ? valName : attr.Description);
            }

            typeDescriptions.Add(enumType, dic);

            return dic;
        }

        public static string GetDescription(this Enum value)
        {
            var enumType = value.GetType();
            var dic = GetDescriptions(enumType);
            string val;
            if (dic.TryGetValue(value, out val))
            {
                return val;
            }

            return value.ToString();
        }
    }
}
namespace System
{
    using System;

    public static class DateTimeExtensions
    {
        public static string ToFriendPeriod(this DateTime value1, DateTime value2)
        {
            var cultureInfo = Globalization.CultureInfo.CurrentUICulture;
            var months = cultureInfo.DateTimeFormat.AbbreviatedMonthGenitiveNames;

            if (value1.Year != value2.Year)
            {
                return value1.ToString("d") + "-" + value2.ToString("d");
            }

            if (value1.Month != value2.Month)
            {
                return value1.Day + " " + months[value1.Month - 1] + " - " + value2.Day + months[value2.Month - 1] + " " + value2.Year;
            }

            if (value1.Day != value2.Day)
            {
                return value1.Day + "-" + value2.Day + months[value2.Month - 1] + " " + value2.Year;
            }

            return value2.Day + " " + months[value2.Month - 1] + " " + value2.Year;
        }
    }
}

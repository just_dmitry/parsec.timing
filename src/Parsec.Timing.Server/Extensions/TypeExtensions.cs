﻿namespace System
{
    using System.Reflection;

    public static class TypeExtensions
    {
        public static string GetAssemblyNameVersion()
        {
            var assembly = Assembly.GetEntryAssembly();
            var ver = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            var title = assembly.GetCustomAttribute<AssemblyTitleAttribute>().Title;

            return title + " v" + ver;
        }
    }
}

﻿namespace System.Threading.Tasks.Dataflow
{
    public static class DataflowExtensions
    {
        /// <summary>
        /// Inverse of <see cref="TransformBlock<TInput, TOutput>.LinkTo"/>
        /// </summary>
        public static TOutputBlock AttachAfter<TInput, TOutput, TOutputBlock>(
            this TOutputBlock target,
            TransformBlock<TInput, TOutput> after,
            DataflowLinkOptions linkOptions)
            where TOutputBlock : ITargetBlock<TOutput>
        {
            after.LinkTo(target, linkOptions);
            return target;
        }

        /// <summary>
        /// Inverse of <see cref="TransformManyBlock<TInput, TOutput>.LinkTo"/>
        /// </summary>
        public static TOutputBlock AttachAfter<TInput, TOutput, TOutputBlock>(
            this TOutputBlock target,
            TransformManyBlock<TInput, TOutput> after,
            DataflowLinkOptions linkOptions)
            where TOutputBlock : ITargetBlock<TOutput>
        {
            after.LinkTo(target, linkOptions);
            return target;
        }
    }
}

﻿namespace System
{
    public static class TimeSpanExtensions
    {
        /// <summary>
        /// Truncates TimeSpan to integral seconds (fraction = 1) or to specified second fractions
        /// </summary>
        /// <param name="value">Original TimeSpan value</param>
        /// <param name="fractions">Amount of sub-seconds (e.g. 10 for 0.1 accuracy, 100 for 0.01 etc)</param>
        /// <returns>New (truncated) TimeSpan value</returns>
        public static TimeSpan Truncate(this TimeSpan value, int fractions = 1)
        {
            if (fractions < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(fractions));
            }

            var precision = TimeSpan.TicksPerSecond / fractions;

            return new TimeSpan((value.Ticks / precision) * precision);
        }
    }
}

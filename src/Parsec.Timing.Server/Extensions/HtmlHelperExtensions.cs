namespace System
{
    using Microsoft.AspNetCore.Html;
    using Microsoft.AspNetCore.Mvc.Rendering;

    public static class HtmlHelperExtensions
    {
        public static IHtmlContent WriteMultiline(this IHtmlHelper helper, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var encoded = helper.Encode(value);
            encoded = encoded.Replace(Environment.NewLine, "<br/>").Replace("&#xD;&#xA;", "<br/>");
            return new HtmlContentBuilder().AppendHtml(encoded);
        }
    }
}
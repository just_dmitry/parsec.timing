﻿namespace System.Reflection
{
    public static class AssemblyExtensions
    {
        public static string GetNameVersion(this Assembly assembly)
        {
            var name = assembly.GetName();
            return name.Name + " v" + name.Version;
        }
    }
}

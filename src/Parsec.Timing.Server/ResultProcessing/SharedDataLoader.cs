﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Server.Services;

    public static class SharedDataLoader
    {
        public static async Task<SharedData> LoadAsync(Guid competitionPoolId, TimingDb db)
        {
            var sharedData = new SharedData { CompetitionPoolId = competitionPoolId };

            sharedData.FinAdjustments = await db.FinAdjustments.AsNoTracking()
                .Where(x => x.CompetitionPoolId == competitionPoolId)
                .OrderBy(x => x.Order)
                .ToListAsync();

            sharedData.InstantArrivalSplits = await db.Splits.AsNoTracking()
                .Where(x => x.Competition.CompetitionPoolId == competitionPoolId)
                .Where(x => x.InstantArrivalCheckpointId != null)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            return sharedData;
        }
    }
}

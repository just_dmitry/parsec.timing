﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Parsec.Timing.Model;
    using Parsec.Timing.Server.Services;

    public static class FinMarksProcessor
    {
        public static async Task Process(ProcessingItem item, ParticipantData data, List<FinMark> finMarks, SharedData sharedData, TimingDb db)
        {
            var adjustments = GetAdjustmentsForParticipant(sharedData.FinAdjustments, data).ToList();

            var splitsToVisit = sharedData.InstantArrivalSplits
                .Where(x => x.CompetitionId == data.CompetitionId)
                .ToList();

            var lastResult =
                data.LastResults.FirstOrDefault()
                ?? new Result { ParticipantId = item.ParticipantId };

            var results = GenerateResults(finMarks, lastResult, splitsToVisit, adjustments);

            data.LastResults.AddRange(results);

            db.Results.AddRange(results);
            await db.SaveChangesAsync();

            // оставляем только несоколько верхних (последних по хронологии)
            if (data.LastResults.Count > ParticipantData.LastResultMaxCount)
            {
                data.LastResults = data.LastResults.GetRange(0, ParticipantData.LastResultMaxCount);
            }
        }

        public static IEnumerable<FinAdjustment> GetAdjustmentsForParticipant(
            IEnumerable<FinAdjustment> source,
            ParticipantData participant)
        {
            return source
                .Where(x => x.BibArray == null || x.BibArray.Length == 0 || x.BibArray.Contains(participant.Bib))
                .Where(x => x.CompetitionId == null || x.CompetitionId == participant.CompetitionId)
                .Where(x => x.Category == 0 || (x.Category & participant.Categories) == x.Category);
        }

        public static bool IsAdjustmentMatch(FinAdjustment adjustment, TimeAndLength currentValue, Guid? checkpointId)
        {
            if (adjustment.CheckpointId.HasValue && adjustment.CheckpointId != checkpointId)
            {
                return false;
            }

            if (adjustment.NotEarlierThanResultValue != 0)
            {
                if (adjustment.NotEarlierThanResultType == ResultType.Time1000
                    && adjustment.NotEarlierThanResult.Time > currentValue.Time)
                {
                    return false;
                }

                if (adjustment.NotEarlierThanResultType == ResultType.Length1000
                    && adjustment.NotEarlierThanResult.Length > currentValue.Length)
                {
                    return false;
                }
            }

            if (adjustment.NotLaterThanResultValue != 0)
            {
                if (adjustment.NotLaterThanResultType == ResultType.Time1000
                    && adjustment.NotLaterThanResult.Time < currentValue.Time)
                {
                    return false;
                }

                if (adjustment.NotLaterThanResultType == ResultType.Length1000
                    && adjustment.NotLaterThanResult.Length < currentValue.Length)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsDuplicate(FinMark mark, Result lastResult)
        {
            if (lastResult == null)
            {
                return false;
            }

            var timeOrLength = mark.EffectiveResult;

            if (timeOrLength.IsLength && timeOrLength.EncodedValue != 0
                && Math.Abs(lastResult.TotalLength - timeOrLength.Length) < 5)
            {
                return true;
            }

            if (timeOrLength.IsTime && timeOrLength.EncodedValue != 0
                && lastResult.TotalTime.Subtract(timeOrLength.Time).Duration() < TimeSpan.FromSeconds(10))
            {
                return true;
            }

            return false;
        }

        public static FinMark FindNextMarkSkipDuplicates(IEnumerator<FinMark> marks, Result lastResult)
        {
            while (marks.MoveNext())
            {
                var mark = marks.Current;
                if (IsDuplicate(mark, lastResult))
                {
                    mark.Status = FinMarkStatus.Duplicate;
                }
                else
                {
                    return mark;
                }
            }

            return null;
        }

        public static (Result predictedResult, List<Split> updatedSplits) PredictResult(
            FinMark mark,
            Result lastResult,
            List<Split> splits,
            List<FinAdjustment> adjustments)
        {
            if (mark == null)
            {
                return (null, splits);
            }

            var predictOk = false;
            var predictedResult = lastResult.CreateNextGround();

            var markValue = new TimeAndLength(mark.EffectiveResult);

            if (ApplyNearestSplit(markValue, mark.FinGroup.CheckpointId, ref splits))
            {
                predictedResult.TotalTime = markValue.Time.Value;
                predictedResult.TotalLength = markValue.Length.Value;
                predictOk = true;
            }

            foreach (var adj in adjustments)
            {
                if (IsAdjustmentMatch(adj, markValue, mark.FinGroup.CheckpointId))
                {
                    if (adj.Mode == FinAdjustmentMode.Reject)
                    {
                        return (null, null);
                    }

                    ApplyAdjustment(adj, markValue, predictedResult);
                    predictOk = true;
                }
            }

            return predictOk ? (predictedResult, splits) : (null, null);
        }

        public static List<Result> GenerateResults(List<FinMark> finMarks, Result lastResult, List<Split> splits, List<FinAdjustment> adjustments)
        {
            var results = new List<Result>();

            var lengthMarks = finMarks.Where(x => x.EffectiveResult.IsLength).GetEnumerator();
            var timeMarks = finMarks.Where(x => x.EffectiveResult.IsTime).GetEnumerator();

            FinMark lengthMark = null, timeMark = null;

            while (true)
            {
                if (lengthMark == null)
                {
                    lengthMark = FindNextMarkSkipDuplicates(lengthMarks, lastResult);
                }

                if (timeMark == null)
                {
                    timeMark = FindNextMarkSkipDuplicates(timeMarks, lastResult);
                }

                if (lengthMark == null && timeMark == null)
                {
                    break;
                }

                var (lengthResult, lengthSplits) = PredictResult(lengthMark, lastResult, splits, adjustments);
                var (timeResult, timeSplits) = PredictResult(timeMark, lastResult, splits, adjustments);

                if (lengthResult == null && timeResult == null)
                {
                    lengthMark = null;
                    timeMark = null;
                    continue;
                }

                if (lengthResult == null || lengthResult.TotalTime > timeResult?.TotalTime)
                {
                    results.Add(timeResult);
                    timeMark.Status = FinMarkStatus.Default;
                    timeMark = null;
                    lastResult = timeResult;
                    splits = timeSplits;
                    continue;
                }

                if (timeResult == null || lengthResult?.TotalTime < timeResult.TotalTime)
                {
                    results.Add(lengthResult);
                    lengthMark.Status = FinMarkStatus.Default;
                    lengthMark = null;
                    lastResult = lengthResult;
                    splits = lengthSplits;
                    continue;
                }
            }

            return results;
        }

        public static bool ApplyNearestSplit(TimeAndLength value, Guid? checkpointId, ref List<Split> splitsToVisit)
        {
            if (!checkpointId.HasValue)
            {
                return false;
            }

            var nearestSplit = splitsToVisit.Find(x => x.InstantArrivalCheckpointId == checkpointId);
            if (nearestSplit == null)
            {
                return false;
            }

            var splitTimeOrLength = nearestSplit.Result;

            if (splitTimeOrLength.IsLength)
            {
                value.Length = splitTimeOrLength.Length;
            }

            if (splitTimeOrLength.IsTime)
            {
                value.Time = splitTimeOrLength.Time;
            }

            splitsToVisit = splitsToVisit.SkipWhile(x => x != nearestSplit).Skip(1).ToList();

            return true;
        }

        public static void ApplyAdjustment(FinAdjustment adjustment, TimeAndLength currentValue, Result result)
        {
            switch (adjustment.Mode)
            {
                case FinAdjustmentMode.Unspecified:
                case FinAdjustmentMode.FLAG_SPECIAL:
                case FinAdjustmentMode.FLAG_MODIFY:
                case FinAdjustmentMode.FLAG_APPLY:
                case FinAdjustmentMode.Reject:
                    throw new InvalidOperationException($"Can't apply '{adjustment.Mode}' adjustment");

                /* SPECIAL */

                case FinAdjustmentMode.StartNextLap:
                    result.FullLapsCount++;
                    result.FullLapsLength = result.TotalLength;
                    result.FullLapsTime = result.TotalTime;
                    break;

                /* MODIFY */

                case FinAdjustmentMode.AssignValue:
                    if (adjustment.Value.IsLength)
                    {
                        currentValue.Length = adjustment.Value.Length;
                    }

                    if (adjustment.Value.IsTime)
                    {
                        currentValue.Time = adjustment.Value.Time;
                    }

                    break;

                /* APPLY */

                case FinAdjustmentMode.ApplyAbsoluteSetBoth:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength = currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime = currentValue.Time.Value;
                    }

                    break;

                case FinAdjustmentMode.ApplyAbsoluteAddLengthSetTime:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength += currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime = currentValue.Time.Value;
                    }

                    break;

                case FinAdjustmentMode.ApplyAbsoluteAddTimeSetLength:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength = currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime += currentValue.Time.Value;
                    }

                    break;

                case FinAdjustmentMode.ApplyAbsoluteAddBoth:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength += currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime += currentValue.Time.Value;
                    }

                    break;

                case FinAdjustmentMode.ApplyLastLapAddLengthSetTime:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength = result.FullLapsLength + currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime = currentValue.Time.Value;
                    }

                    break;

                case FinAdjustmentMode.ApplyLastLapAddTimeSetLength:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength = currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime = result.FullLapsTime + currentValue.Time.Value;
                    }

                    break;

                case FinAdjustmentMode.ApplyLastLapAddBoth:
                    if (currentValue.Length.HasValue)
                    {
                        result.TotalLength = result.FullLapsLength + currentValue.Length.Value;
                    }

                    if (currentValue.Time.HasValue)
                    {
                        result.TotalTime = result.FullLapsTime + currentValue.Time.Value;
                    }

                    break;
            }
        }
    }
}

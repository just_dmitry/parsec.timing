﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;

    public class TimeAndLength
    {
        public TimeAndLength()
        {
            // Nothing
        }

        public TimeAndLength(Model.TimeOrLength baseValue)
        {
            if (baseValue.IsLength)
            {
                Length = baseValue.Length;
            }

            if (baseValue.IsTime)
            {
                Time = baseValue.Time;
            }
        }

        public TimeSpan? Time { get; set; }

        public decimal? Length { get; set; }
    }
}

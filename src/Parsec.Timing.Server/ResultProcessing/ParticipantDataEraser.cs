﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Server.Services;

    public static class ParticipantDataEraser
    {
        public static async Task Erase(ProcessingItem item, TimingDb db)
        {
            var boundary = item.Boundary.Ticks;

            while (true)
            {
                var oldResults = await db.Results
                    .Where(x => x.ParticipantId == item.ParticipantId)
                    .Where(x => x.TotalTimeTicks >= boundary)
                    .Take(500)
                    .ToListAsync();

                if (oldResults.Count == 0)
                {
                    break;
                }

                db.Results.RemoveRange(oldResults);
                await db.SaveChangesAsync();
            }
        }
    }
}

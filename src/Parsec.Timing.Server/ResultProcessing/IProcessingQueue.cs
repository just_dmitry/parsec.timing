﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Threading;

    public interface IProcessingQueue
    {
        int Count { get; }

        ManualResetEventSlim QueueIsEmpty { get; }

        ManualResetEventSlim QueueContainsData { get; }

        void Add(ProcessingItem item);

        ProcessingItem GetAndLock();

        void Unlock(ProcessingItem item);
    }
}

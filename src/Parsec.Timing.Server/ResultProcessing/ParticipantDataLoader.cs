﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Server.Services;

    public static class ParticipantDataLoader
    {
        public static async Task<ParticipantData> Load(Guid participantId, TimingDb db)
        {
            var retVal = new ParticipantData();

            var p = await db.Participants.AsNoTracking().SingleAsync(x => x.Id == participantId);

            retVal.CompetitionId = p.CompetitionId;
            retVal.Bib = p.Bib;
            retVal.Categories = p.Categories;

            retVal.LastResults = await db.Results.AsNoTracking()
                .Where(x => x.ParticipantId == participantId)
                .OrderByDescending(x => x.Index)
                .Take(ParticipantData.LastResultMaxCount)
                .ToListAsync();

            return retVal;
        }
    }
}

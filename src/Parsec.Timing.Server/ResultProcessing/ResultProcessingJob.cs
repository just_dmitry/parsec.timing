﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Model;
    using Parsec.Timing.Server.Services;

    public class ResultProcessingJob : IBackgroundProcessingJob
    {
        private readonly ILogger logger;

        private ResultProcessingJob(Guid id, string name, ILogger<ResultProcessingJob> logger)
        {
            Name = $"Обработка финиша «{name}»";
            Progress = 0;
            CompetitionPoolId = id;
            this.logger = logger;
        }

        public static int BatchSize { get; set; } = 500;

        public Guid CompetitionPoolId { get; }

        public string Name { get; }

        public byte Progress { get; private set; }

        public static ResultProcessingJob Create(CompetitionPool pool, ILogger<ResultProcessingJob> logger)
        {
            return new ResultProcessingJob(pool.Id, pool.Name, logger);
        }

        public static ResultProcessingJob Create(Guid id, string name, ILogger<ResultProcessingJob> logger)
        {
            return new ResultProcessingJob(id, name, logger);
        }

        public async Task RunAsync(IServiceProvider serviceProvider)
        {
            logger.LogDebug($"Processing CompetitionPool #{CompetitionPoolId.ToString()}");

            var db = serviceProvider.GetRequiredService<TimingDb>();

            var participants = await db.Participants
                .Where(x => x.Competition.CompetitionPoolId == CompetitionPoolId)
                .Select(x => new ProcessingItem
                {
                    ParticipantId = x.Id,
                    CompetitionPoolId = CompetitionPoolId,
                    Boundary = TimeSpan.Zero,
                    IsLocked = false,
                })
                .ToListAsync();

            var queue = serviceProvider.GetRequiredService<IProcessingQueue>();

            foreach (var item in participants)
            {
                queue.Add(item);
            }

            queue.QueueIsEmpty.Wait();

            logger.LogInformation($"Processed Ok: CompetitionPool #{CompetitionPoolId.ToString()}.");
        }
    }
}

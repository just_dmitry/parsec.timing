﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Timing.Model;
    using Parsec.Timing.Server.Services;

    public static class FinMarksLoader
    {
        public static Task<List<FinMark>> Load(ProcessingItem item, ParticipantData data, TimingDb db)
        {
            long ticks = new TimeOrLength(item.Boundary).EncodedValue;

            return db.FinMarks
                .Include(x => x.FinGroup)
                .Where(x => x.FinGroup.CompetitionPoolId == item.CompetitionPoolId)
                .Where(x => !x.FinGroup.IsDisabled)
                .Where(x => x.Bib == data.Bib)
                .Where(x => x.RevisedResultValue > ticks)
                .OrderBy(x => x.RevisedResultValue)
                .ToListAsync();
        }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.Services;
    using RecurrentTasks;

    public class ResultProcessingTask : IRunnable
    {
        private readonly IServiceProvider serviceProvider;

        private readonly ILogger logger;

        public ResultProcessingTask(IServiceProvider serviceProvider, ILogger<ResultProcessingTask> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public void Run(ITask currentTask, CancellationToken cancellationToken)
        {
            RunAsync(currentTask, cancellationToken).Wait();
        }

        public async Task RunAsync(ITask currentTask, CancellationToken cancellationToken)
        {
            var queue = serviceProvider.GetRequiredService<IProcessingQueue>();
            var db = serviceProvider.GetRequiredService<TimingDb>();

            var waitArray = new WaitHandle[] { cancellationToken.WaitHandle, queue.QueueContainsData.WaitHandle };

            var allSharedData = new Dictionary<Guid, SharedData>(4);
            var allParticipantData = new Dictionary<Guid, ParticipantData>(256);

            while (true)
            {
                var waitResult = WaitHandle.WaitAny(waitArray, TimeSpan.FromMinutes(5));

                if (waitResult == 0)
                {
                    logger.LogDebug("CancellationToken signaled. Quitting...");
                    break;
                }

                if (waitResult == WaitHandle.WaitTimeout)
                {
                    logger.LogDebug("Wait timeout. Nothing hapened. Re-waiting...");
                    continue;
                }

                var data = queue.GetAndLock();
                if (data == null)
                {
                    logger.LogWarning("Ooops, no data in queue. OK, re-waiting...");
                    continue;
                }

                if (!allSharedData.TryGetValue(data.CompetitionPoolId, out var sharedData))
                {
                    sharedData = await SharedDataLoader.LoadAsync(data.CompetitionPoolId, db);
                    allSharedData[sharedData.CompetitionPoolId] = sharedData;
                }

                await ParticipantDataEraser.Erase(data, db);

                if (!allParticipantData.TryGetValue(data.ParticipantId, out var participantData))
                {
                    participantData = await ParticipantDataLoader.Load(data.ParticipantId, db);
                    allParticipantData[data.ParticipantId] = participantData;
                }

                var marks = await FinMarksLoader.Load(data, participantData, db);

                await FinMarksProcessor.Process(data, participantData, marks, sharedData, db);

                logger.LogDebug($"Done: {data.CompetitionPoolId}/{data.ParticipantId} ({participantData.Bib})");

                queue.Unlock(data);
            }

            logger.LogInformation("Finished");
        }
    }
}

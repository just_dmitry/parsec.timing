﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    public class ProcessingQueue : IProcessingQueue
    {
        private readonly object syncRoot = new object();

        private readonly Dictionary<Guid, ProcessingItem> data = new Dictionary<Guid, ProcessingItem>();

        public int Count => data.Count;

        public ManualResetEventSlim QueueIsEmpty { get; } = new ManualResetEventSlim(true);

        public ManualResetEventSlim QueueContainsData { get; } = new ManualResetEventSlim(false);

        public void Add(ProcessingItem item)
        {
            lock (syncRoot)
            {
                if (data.TryGetValue(item.ParticipantId, out ProcessingItem val))
                {
                    val.Boundary = item.Boundary < val.Boundary ? item.Boundary : val.Boundary;
                }
                else
                {
                    data.Add(item.ParticipantId, item);
                    QueueIsEmpty.Reset();
                    QueueContainsData.Set();
                }
            }
        }

        public ProcessingItem GetAndLock()
        {
            lock (syncRoot)
            {
                foreach (var kv in data)
                {
                    if (!kv.Value.IsLocked)
                    {
                        kv.Value.IsLocked = true;

                        var retVal = new ProcessingItem
                        {
                            ParticipantId = kv.Value.ParticipantId,
                            CompetitionPoolId = kv.Value.CompetitionPoolId,
                            Boundary = kv.Value.Boundary,
                        };

                        kv.Value.Boundary = TimeSpan.MaxValue;
                        return retVal;
                    }
                }
            }

            QueueContainsData.Reset();

            return null;
        }

        public void Unlock(ProcessingItem item)
        {
            lock (syncRoot)
            {
                if (data.TryGetValue(item.ParticipantId, out ProcessingItem val))
                {
                    if (val.Boundary == TimeSpan.MaxValue)
                    {
                        data.Remove(item.ParticipantId);
                    }
                    else
                    {
                        val.IsLocked = false;
                    }
                }
            }

            if (data.Count == 0)
            {
                QueueIsEmpty.Set();
                QueueContainsData.Reset();
            }
        }
    }
}

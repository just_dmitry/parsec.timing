﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;

    public class ProcessingItem
    {
        public Guid ParticipantId { get; set; }

        public bool IsLocked { get; set; }

        public Guid CompetitionPoolId { get; set; }

        public TimeSpan Boundary { get; set; }
    }
}

﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using Parsec.Timing.Model;

    public class ParticipantData
    {
        public const int LastResultMaxCount = 3;

        public Guid CompetitionId { get; set; }

        public string Bib { get; set; }

        public int Categories { get; set; }

        public List<Result> LastResults { get; set; }
    }
}

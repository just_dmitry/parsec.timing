﻿namespace Parsec.Timing.Server.ResultProcessing
{
    using System;
    using System.Collections.Generic;
    using Parsec.Timing.Model;

    public class SharedData
    {
        public Guid CompetitionPoolId { get; set; }

        public List<Split> InstantArrivalSplits { get; set; }

        public List<FinAdjustment> FinAdjustments { get; set; }
    }
}

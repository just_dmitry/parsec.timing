﻿namespace Parsec.Timing.Server
{
    using System;
    using System.Globalization;
    using System.Text.Unicode;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Razor;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.ResultProcessing;
    using RecurrentTasks;
    using Services;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Microsoft.Extensions.WebEncoders.WebEncoderOptions>(
               o =>
               {
                   o.TextEncoderSettings = new System.Text.Encodings.Web.TextEncoderSettings();
                   o.TextEncoderSettings.AllowRanges(new[] { UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic });
               });

            services.AddSingleton(Configuration);

            var connString = Configuration["ConnectionString"];
            services.AddDbContextPool<TimingDb>(o => o
                .UseSqlServer(connString)
                .ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning)));

            // services.Configure<IdentityOptions>(options => options.User.AllowedUserNameCharacters = null);

            services.AddMvc(options =>
            {
                options.Conventions.Add(new AreaModelConvention());
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.AreaViewLocationFormats.Clear();
                options.AreaViewLocationFormats.Add("/Areas/{2}/{1}/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Areas/{2}/{1}_{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Areas/{2}/{1}.cshtml");
                options.AreaViewLocationFormats.Add("/Areas/{2}/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Areas/Shared/{0}.cshtml");
            });

            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddTransient(typeof(BootstrapMvc.Mvc6.BootstrapHelper<>));

            services.AddSingleton<ICurrentTournamentService, CurrentTournamentService>();
            services.AddSingleton<ICacheService, CacheService>();

            services.AddScoped<SeedService>();
            services.AddScoped<IMultiEventTournamentFieldService, MultiEventTournamentFieldService>();
            services.AddScoped<IFinGroupService, FinGroupService>();
            services.AddScoped<CommonOperationsService>();

            services.AddSingleton<ResultProcessingTask>();
            services.AddSingleton<IProcessingQueue, ProcessingQueue>();

            services.AddSingleton<ReadersService>();

            services.AddSingleton<BackgroundProcessingService>();
            services.AddTask<BackgroundProcessingService>();
            services.AddTask<ResultProcessingTask>();

            services.AddSignalR();

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddMemory(Configuration.GetSection("Logging"));

            app.UseDeveloperExceptionPage();

            var staticFileOptions = new StaticFileOptions
            {
                OnPrepareResponse = (context) =>
                {
                    context.Context.Response.Headers.Add("Cache-Control", "public, max-age=15552000"); // 180 days
                },
            };
            app.UseStaticFiles(staticFileOptions);

            ////app.UseIdentity();
            ////app.UseGoogleAuthentication(new GoogleOptions
            ////{
            ////    ClientId = Configuration["OAuth:Google:ClientId"],
            ////    ClientSecret = Configuration["OAuth:Google:Secret"]
            ////});

            var ruCulture = new CultureInfo("ru-RU");
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture(ruCulture),
                SupportedCultures = new[] { ruCulture },
                SupportedUICultures = new[] { ruCulture },
            });

            app.UseSignalR(routes => routes.MapHub<Areas.Manage.ReadersHub>("manage/readers/hub"));

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "one",
                    template: "{action=Index}/{id?}",
                    defaults: new { area = "Default", controller = "Home" },
                    constraints: new { area = "Default", controller = "Home" });
                routes.MapRoute(
                    name: "two",
                    template: "{controller}/{action=Index}/{id?}",
                    defaults: new { area = "Default" },
                    constraints: new { area = "Default" });
                routes.MapRoute(
                    name: "three",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
            });

            app.StartTask<BackgroundProcessingService>(TimeSpan.FromMinutes(5));
        }
    }
}

﻿namespace Parsec.Timing.Server.Migrations
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Parsec.Timing.Server.Services;

    [DbContext(typeof(TimingDb))]
    public partial class TimingDbModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Parsec.Timing.Model.AgeGroup", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte>("MaxAge");

                    b.Property<byte>("MinAge");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("ShortName")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.ToTable("AgeGroups");
                });

            modelBuilder.Entity("Parsec.Timing.Model.AgeGroupSet", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.HasAlternateKey("MultiEventTournamentId", "Name");

                    b.ToTable("AgeGroupSets");
                });

            modelBuilder.Entity("Parsec.Timing.Model.AgeGroupSetContent", b =>
                {
                    b.Property<Guid>("AgeGroupSetId");

                    b.Property<Guid>("AgeGroupId");

                    b.HasKey("AgeGroupSetId", "AgeGroupId");

                    b.HasIndex("AgeGroupId");

                    b.ToTable("AgeGroupSetContents");
                });

            modelBuilder.Entity("Parsec.Timing.Model.CategoryFlag", b =>
                {
                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<int>("Id");

                    b.Property<string>("Comment")
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .HasMaxLength(200);

                    b.HasKey("MultiEventTournamentId", "Id");

                    b.ToTable("CategoryFlags");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Checkpoint", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<byte>("SortOrder");

                    b.HasKey("Id");

                    b.HasAlternateKey("MultiEventTournamentId", "Name");

                    b.ToTable("Checkpoints");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Competition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AllowMen");

                    b.Property<bool>("AllowWomen");

                    b.Property<Guid>("CompetitionPoolId");

                    b.Property<int>("Key");

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("NameInternational")
                        .HasMaxLength(200);

                    b.Property<byte>("SortOrder");

                    b.HasKey("Id");

                    b.HasAlternateKey("MultiEventTournamentId", "Name");

                    b.HasIndex("CompetitionPoolId");

                    b.HasIndex("Key")
                        .IsUnique();

                    b.ToTable("Competitions");
                });

            modelBuilder.Entity("Parsec.Timing.Model.CompetitionPool", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsLocked");

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<byte>("ParticipationProcessingStatus");

                    b.Property<DateTimeOffset>("ParticipationProcessingTime")
                        .HasColumnType("datetimeoffset(2)");

                    b.Property<byte>("ResultProcessingStatus");

                    b.Property<DateTimeOffset>("ResultProcessingTime")
                        .HasColumnType("datetimeoffset(2)");

                    b.Property<byte>("SortOrder");

                    b.HasKey("Id");

                    b.HasAlternateKey("MultiEventTournamentId", "Name");

                    b.ToTable("CompetitionPools");
                });

            modelBuilder.Entity("Parsec.Timing.Model.CompetitionPoolError", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Bib")
                        .HasMaxLength(10);

                    b.Property<Guid>("CompetitionPoolId");

                    b.Property<string>("Text")
                        .HasMaxLength(1000);

                    b.Property<byte>("Type");

                    b.HasKey("Id");

                    b.HasIndex("CompetitionPoolId");

                    b.ToTable("CompetitionPoolErrors");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Field", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte>("HorizontalAlign");

                    b.Property<bool>("IsComputed");

                    b.Property<string>("Key")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<byte>("SortOrder");

                    b.Property<byte>("Source");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<byte>("Type");

                    b.HasKey("Id");

                    b.ToTable("Fields");
                });

            modelBuilder.Entity("Parsec.Timing.Model.FinAdjustment", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BibString")
                        .HasColumnName("Bibs")
                        .HasMaxLength(1000);

                    b.Property<int>("Category");

                    b.Property<Guid?>("CheckpointId");

                    b.Property<string>("Comment")
                        .HasMaxLength(50);

                    b.Property<Guid?>("CompetitionId");

                    b.Property<Guid>("CompetitionPoolId");

                    b.Property<bool>("IsAutoGenerated");

                    b.Property<byte>("Mode");

                    b.Property<Guid?>("NextFinAdjustmentId");

                    b.Property<byte>("NotEarlierThanResultType");

                    b.Property<long>("NotEarlierThanResultValue");

                    b.Property<byte>("NotLaterThanResultType");

                    b.Property<long>("NotLaterThanResultValue");

                    b.Property<int>("Order");

                    b.Property<byte>("ValueType");

                    b.Property<long>("ValueValue");

                    b.HasKey("Id");

                    b.HasIndex("CheckpointId");

                    b.HasIndex("CompetitionId");

                    b.HasIndex("CompetitionPoolId");

                    b.HasIndex("NextFinAdjustmentId");

                    b.ToTable("FinAdjustments");
                });

            modelBuilder.Entity("Parsec.Timing.Model.FinGroup", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CheckpointId");

                    b.Property<string>("ClientNameVersion")
                        .HasMaxLength(100);

                    b.Property<string>("Comment")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("CompetitionPoolId");

                    b.Property<DateTimeOffset>("Created")
                        .HasColumnType("datetimeoffset(2)");

                    b.Property<string>("IpAddress")
                        .HasMaxLength(100);

                    b.Property<bool>("IsDisabled");

                    b.Property<DateTimeOffset>("LastSeen");

                    b.Property<int>("MarksCount");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<byte>("ResultShiftType");

                    b.Property<long>("ResultShiftValue");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.HasKey("Id");

                    b.HasIndex("CheckpointId");

                    b.HasIndex("CompetitionPoolId");

                    b.ToTable("FinGroups");
                });

            modelBuilder.Entity("Parsec.Timing.Model.FinMark", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Bib")
                        .HasMaxLength(50);

                    b.Property<int>("FinGroupId");

                    b.Property<DateTimeOffset>("Inserted")
                        .HasColumnType("datetimeoffset(2)");

                    b.Property<long>("OriginalResultValue");

                    b.Property<byte>("ResultType");

                    b.Property<long>("RevisedResultValue");

                    b.Property<byte>("Status");

                    b.HasKey("Id");

                    b.HasIndex("FinGroupId");

                    b.ToTable("FinMarks");
                });

            modelBuilder.Entity("Parsec.Timing.Model.MultiEventTournament", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("EndDate");

                    b.Property<bool>("IsLocked");

                    b.Property<string>("Location")
                        .HasMaxLength(500);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<Guid>("ServicingTeamId");

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("Website")
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.HasIndex("ServicingTeamId");

                    b.HasIndex("StartDate");

                    b.ToTable("MultiEventTournaments");
                });

            modelBuilder.Entity("Parsec.Timing.Model.MultiEventTournamentField", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("FieldId");

                    b.Property<bool>("IsUsed");

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<byte>("SortOrder");

                    b.HasKey("Id");

                    b.HasIndex("FieldId");

                    b.HasIndex("MultiEventTournamentId", "FieldId")
                        .IsUnique();

                    b.ToTable("MultiEventTournamentFields");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Participant", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Bib")
                        .HasMaxLength(10);

                    b.Property<int>("Categories");

                    b.Property<Guid>("CompetitionId");

                    b.Property<DateTimeOffset>("InsertTime")
                        .HasColumnType("datetimeoffset(2)");

                    b.Property<Guid>("PersonId");

                    b.Property<int>("SortOrder");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.HasKey("Id");

                    b.HasIndex("PersonId");

                    b.HasIndex("CompetitionId", "Bib");

                    b.HasIndex("CompetitionId", "PersonId");

                    b.ToTable("Participants");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantAgeRanking", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AgeGroupId");

                    b.Property<Guid>("ParticipantId");

                    b.Property<string>("Rank")
                        .HasMaxLength(20);

                    b.Property<Guid>("TournamentCompetitionId");

                    b.HasKey("Id");

                    b.HasIndex("AgeGroupId");

                    b.HasIndex("ParticipantId");

                    b.HasIndex("TournamentCompetitionId");

                    b.ToTable("ParticipantAgeRankings");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantField", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompetitionKey");

                    b.Property<int>("MultiEventTournamentFieldId");

                    b.Property<Guid>("ParticipantId");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.HasIndex("CompetitionKey");

                    b.HasIndex("MultiEventTournamentFieldId");

                    b.HasIndex("ParticipantId");

                    b.ToTable("ParticipantFields");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantOverride", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte?>("AgeOverride");

                    b.Property<string>("BibString")
                        .HasColumnName("Bibs")
                        .HasMaxLength(1000);

                    b.Property<int>("Category");

                    b.Property<string>("Comment")
                        .HasMaxLength(50);

                    b.Property<Guid?>("CompetitionId");

                    b.Property<Guid>("CompetitionPoolId");

                    b.Property<byte?>("Gender");

                    b.Property<DateTimeOffset>("InsertTime")
                        .HasColumnType("datetimeoffset(2)");

                    b.Property<bool?>("ParticipationOverride");

                    b.Property<Guid?>("TournamentId");

                    b.HasKey("Id");

                    b.HasIndex("CompetitionId");

                    b.HasIndex("CompetitionPoolId");

                    b.HasIndex("TournamentId");

                    b.ToTable("ParticipantOverrides");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantRanking", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte>("Age");

                    b.Property<Guid?>("FinalSplitId");

                    b.Property<Guid>("ParticipantId");

                    b.Property<string>("Rank")
                        .HasMaxLength(20);

                    b.Property<string>("ResultComment")
                        .HasMaxLength(50);

                    b.Property<string>("ResultText")
                        .HasMaxLength(20);

                    b.Property<int>("SortOrder");

                    b.Property<Guid?>("StatusId");

                    b.Property<Guid>("TournamentCompetitionId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TournamentCompetitionId", "ParticipantId");

                    b.HasIndex("FinalSplitId");

                    b.HasIndex("ParticipantId");

                    b.HasIndex("StatusId");

                    b.ToTable("ParticipantRankings");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantSplitAgeRanking", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AgeGroupId");

                    b.Property<string>("DiffRank")
                        .HasMaxLength(20);

                    b.Property<Guid>("ParticipantId");

                    b.Property<string>("Rank")
                        .HasMaxLength(20);

                    b.Property<Guid>("SplitId");

                    b.Property<Guid>("TournamentCompetitionId");

                    b.HasKey("Id");

                    b.HasIndex("AgeGroupId");

                    b.HasIndex("ParticipantId");

                    b.HasIndex("SplitId");

                    b.HasIndex("TournamentCompetitionId");

                    b.ToTable("ParticipantSplitAgeRankings");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantSplitRanking", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DiffRank")
                        .HasMaxLength(20);

                    b.Property<string>("DiffResultText")
                        .HasMaxLength(20);

                    b.Property<byte>("DiffResultType");

                    b.Property<long>("DiffResultValue");

                    b.Property<int>("DiffSortOrder");

                    b.Property<Guid>("ParticipantId");

                    b.Property<string>("Rank")
                        .HasMaxLength(20);

                    b.Property<string>("ResultText")
                        .HasMaxLength(20);

                    b.Property<int>("SortOrder");

                    b.Property<Guid>("SplitId");

                    b.Property<Guid?>("StatusId");

                    b.Property<Guid>("TournamentCompetitionId");

                    b.HasKey("Id");

                    b.HasIndex("ParticipantId");

                    b.HasIndex("SplitId");

                    b.HasIndex("StatusId");

                    b.HasIndex("TournamentCompetitionId");

                    b.ToTable("ParticipantSplitRankings");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantStatus", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AssignPlace");

                    b.Property<bool>("IsGlobal");

                    b.Property<bool>("KeepResult");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("PublicName")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("PublicNameInternational")
                        .HasMaxLength(200);

                    b.Property<byte>("SortOrder");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("ParticipantStatuses");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Person", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte?>("BirthDay");

                    b.Property<byte?>("BirthMonth");

                    b.Property<short?>("BirthYear");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("FirstNameInternational")
                        .HasMaxLength(200);

                    b.Property<byte>("Gender");

                    b.Property<string>("HashValue")
                        .IsRequired()
                        .HasMaxLength(32);

                    b.Property<DateTimeOffset>("InsertTime")
                        .HasColumnType("datetimeoffset(0)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("LastNameInternational")
                        .HasMaxLength(200);

                    b.Property<string>("MiddleName")
                        .HasMaxLength(200);

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.HasKey("Id");

                    b.HasIndex("HashValue");

                    b.ToTable("Persons");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Report", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Params")
                        .HasMaxLength(1024);

                    b.Property<Guid?>("TournamentId");

                    b.Property<byte>("Type");

                    b.HasKey("Id");

                    b.HasAlternateKey("MultiEventTournamentId", "Name");

                    b.HasIndex("TournamentId");

                    b.ToTable("Reports");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ReportField", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("FieldId");

                    b.Property<Guid>("ReportId");

                    b.Property<int>("SortOrder");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.HasIndex("FieldId");

                    b.HasIndex("ReportId");

                    b.ToTable("ReportFields");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Result", b =>
                {
                    b.Property<Guid>("ParticipantId");

                    b.Property<int>("Index");

                    b.Property<long?>("FinMarkId");

                    b.Property<int>("FullLapsCount");

                    b.Property<decimal>("FullLapsLength");

                    b.Property<long>("FullLapsTimeTicks");

                    b.Property<byte>("Reliability");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<decimal>("TotalLength");

                    b.Property<long>("TotalTimeTicks");

                    b.HasKey("ParticipantId", "Index");

                    b.HasIndex("FinMarkId");

                    b.ToTable("Results");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ResultOverride", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BibString")
                        .HasColumnName("Bibs")
                        .HasMaxLength(1000);

                    b.Property<int>("Category");

                    b.Property<string>("Comment")
                        .HasMaxLength(50);

                    b.Property<Guid?>("CompetitionId");

                    b.Property<Guid>("CompetitionPoolId");

                    b.Property<byte?>("Gender");

                    b.Property<string>("Name")
                        .HasMaxLength(200);

                    b.Property<string>("ResultComment")
                        .HasMaxLength(50);

                    b.Property<Guid?>("SplitId");

                    b.Property<Guid?>("StatusIdOverride");

                    b.Property<Guid?>("TournamentId");

                    b.HasKey("Id");

                    b.HasIndex("CompetitionId");

                    b.HasIndex("CompetitionPoolId");

                    b.HasIndex("SplitId");

                    b.HasIndex("StatusIdOverride");

                    b.HasIndex("TournamentId");

                    b.ToTable("ResultOverrides");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ServicingTeam", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("MarketingText")
                        .IsRequired()
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.ToTable("ServicingTeams");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Split", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CompetitionId");

                    b.Property<Guid?>("InstantArrivalCheckpointId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("NameInternational")
                        .HasMaxLength(200);

                    b.Property<byte>("ResultType");

                    b.Property<long>("ResultValue");

                    b.Property<short>("SortOrder");

                    b.HasKey("Id");

                    b.HasIndex("InstantArrivalCheckpointId");

                    b.HasIndex("CompetitionId", "Name")
                        .IsUnique();

                    b.ToTable("Splits");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Tournament", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsMain");

                    b.Property<Guid>("MultiEventTournamentId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("TitleInternational")
                        .HasMaxLength(500);

                    b.Property<string>("TitleLocal")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.Property<Guid?>("TournamentSeriesId");

                    b.HasKey("Id");

                    b.HasAlternateKey("MultiEventTournamentId", "Name");

                    b.HasIndex("TournamentSeriesId");

                    b.ToTable("Tournaments");
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentCompetition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AbsolutePositions");

                    b.Property<bool>("AgeGroupByFullBirthDate");

                    b.Property<DateTime>("AgeGroupReferenceDate");

                    b.Property<Guid?>("AgeGroupSetId");

                    b.Property<bool>("AutoParticipation");

                    b.Property<Guid>("CompetitionId");

                    b.Property<Guid>("DefaultParticipantStatusId");

                    b.Property<Guid>("TournamentId");

                    b.HasKey("Id");

                    b.HasAlternateKey("CompetitionId", "TournamentId");

                    b.HasIndex("AgeGroupSetId");

                    b.HasIndex("DefaultParticipantStatusId");

                    b.HasIndex("TournamentId");

                    b.ToTable("TournamentCompetitions");
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentCompetitionSplit", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AllocatePlaces");

                    b.Property<bool>("AllocateSplitPlacesByLapTime");

                    b.Property<bool>("AllocateSplitPlacesByResult");

                    b.Property<bool>("IsEnabled");

                    b.Property<bool>("IsFinishable");

                    b.Property<bool>("IsHidden");

                    b.Property<byte>("PlacesGroupIndex");

                    b.Property<Guid>("SplitId");

                    b.Property<Guid>("TournamentCompetitionId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TournamentCompetitionId", "SplitId");

                    b.HasIndex("SplitId");

                    b.ToTable("TournamentCompetitionSplits");
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentOfficial", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Grade")
                        .HasMaxLength(500);

                    b.Property<string>("JobTitle")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.Property<short>("SortOrder");

                    b.Property<Guid>("TournamentId");

                    b.HasKey("Id");

                    b.HasIndex("TournamentId", "SortOrder");

                    b.ToTable("TournamentOfficials");
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentSeries", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.ToTable("TournamentSeries");
                });

            modelBuilder.Entity("Parsec.Timing.Model.User", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AutomaticUpdatesAllowed");

                    b.Property<string>("EMail")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("EMailMD5")
                        .IsRequired()
                        .HasMaxLength(40);

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsArchived");

                    b.Property<DateTimeOffset?>("LastLogon");

                    b.Property<DateTimeOffset?>("LastVisit");

                    b.Property<string>("LoginProvider")
                        .HasMaxLength(200);

                    b.Property<DateTimeOffset>("MemberSince");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(100);

                    b.Property<string>("PictureUrl")
                        .HasMaxLength(1000);

                    b.Property<string>("ProfileUrl")
                        .HasMaxLength(1000);

                    b.Property<string>("ProviderDisplayName")
                        .HasMaxLength(200);

                    b.Property<string>("ProviderKey")
                        .HasMaxLength(200);

                    b.Property<string>("SecurityStamp")
                        .HasMaxLength(100);

                    b.Property<string>("TimeZoneId")
                        .HasMaxLength(100);

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Parsec.Timing.Model.AgeGroupSet", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.AgeGroupSetContent", b =>
                {
                    b.HasOne("Parsec.Timing.Model.AgeGroup", "AgeGroup")
                        .WithMany()
                        .HasForeignKey("AgeGroupId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.AgeGroupSet", "AgeGroupSet")
                        .WithMany()
                        .HasForeignKey("AgeGroupSetId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.CategoryFlag", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.Checkpoint", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.Competition", b =>
                {
                    b.HasOne("Parsec.Timing.Model.CompetitionPool", "CompetitionPool")
                        .WithMany()
                        .HasForeignKey("CompetitionPoolId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.CompetitionPool", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.CompetitionPoolError", b =>
                {
                    b.HasOne("Parsec.Timing.Model.CompetitionPool", "CompetitionPool")
                        .WithMany()
                        .HasForeignKey("CompetitionPoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.FinAdjustment", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Checkpoint", "Checkpoint")
                        .WithMany()
                        .HasForeignKey("CheckpointId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Competition", "Competition")
                        .WithMany()
                        .HasForeignKey("CompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.CompetitionPool", "CompetitionPool")
                        .WithMany()
                        .HasForeignKey("CompetitionPoolId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.FinAdjustment", "NextFinAdjustment")
                        .WithMany()
                        .HasForeignKey("NextFinAdjustmentId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.FinGroup", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Checkpoint", "Checkpoint")
                        .WithMany()
                        .HasForeignKey("CheckpointId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.CompetitionPool", "CompetitionPool")
                        .WithMany()
                        .HasForeignKey("CompetitionPoolId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.FinMark", b =>
                {
                    b.HasOne("Parsec.Timing.Model.FinGroup", "FinGroup")
                        .WithMany()
                        .HasForeignKey("FinGroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.MultiEventTournament", b =>
                {
                    b.HasOne("Parsec.Timing.Model.ServicingTeam", "ServicingTeam")
                        .WithMany()
                        .HasForeignKey("ServicingTeamId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.MultiEventTournamentField", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Field", "Field")
                        .WithMany()
                        .HasForeignKey("FieldId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.Participant", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Competition", "Competition")
                        .WithMany()
                        .HasForeignKey("CompetitionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Person", "Person")
                        .WithMany()
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantAgeRanking", b =>
                {
                    b.HasOne("Parsec.Timing.Model.AgeGroup", "AgeGroup")
                        .WithMany()
                        .HasForeignKey("AgeGroupId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Participant", "Participant")
                        .WithMany()
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.TournamentCompetition", "TournamentCompetition")
                        .WithMany()
                        .HasForeignKey("TournamentCompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantField", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournamentField", "MultiEventTournamentField")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentFieldId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Participant", "Participant")
                        .WithMany()
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantOverride", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Competition", "Competition")
                        .WithMany()
                        .HasForeignKey("CompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.CompetitionPool", "CompetitionPool")
                        .WithMany()
                        .HasForeignKey("CompetitionPoolId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Tournament", "Tournament")
                        .WithMany()
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantRanking", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Split", "FinalSplit")
                        .WithMany()
                        .HasForeignKey("FinalSplitId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Participant", "Participant")
                        .WithMany()
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.ParticipantStatus", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.TournamentCompetition", "TournamentCompetition")
                        .WithMany()
                        .HasForeignKey("TournamentCompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantSplitAgeRanking", b =>
                {
                    b.HasOne("Parsec.Timing.Model.AgeGroup", "AgeGroup")
                        .WithMany()
                        .HasForeignKey("AgeGroupId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Participant", "Participant")
                        .WithMany()
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Split", "Split")
                        .WithMany()
                        .HasForeignKey("SplitId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.TournamentCompetition", "TournamentCompetition")
                        .WithMany()
                        .HasForeignKey("TournamentCompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ParticipantSplitRanking", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Participant", "Participant")
                        .WithMany()
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Split", "Split")
                        .WithMany()
                        .HasForeignKey("SplitId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.ParticipantStatus", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.TournamentCompetition", "TournamentCompetition")
                        .WithMany()
                        .HasForeignKey("TournamentCompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.Report", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Tournament", "Tournament")
                        .WithMany()
                        .HasForeignKey("TournamentId");
                });

            modelBuilder.Entity("Parsec.Timing.Model.ReportField", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Field", "Field")
                        .WithMany()
                        .HasForeignKey("FieldId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Report", "Report")
                        .WithMany("Fields")
                        .HasForeignKey("ReportId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.Result", b =>
                {
                    b.HasOne("Parsec.Timing.Model.FinMark", "FinMark")
                        .WithMany()
                        .HasForeignKey("FinMarkId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Participant", "Participant")
                        .WithMany()
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.ResultOverride", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Competition", "Competition")
                        .WithMany()
                        .HasForeignKey("CompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.CompetitionPool", "CompetitionPool")
                        .WithMany()
                        .HasForeignKey("CompetitionPoolId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Split", "Split")
                        .WithMany()
                        .HasForeignKey("SplitId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.ParticipantStatus", "StatusOverride")
                        .WithMany()
                        .HasForeignKey("StatusIdOverride")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Tournament", "Tournament")
                        .WithMany()
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.Split", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Competition", "Competition")
                        .WithMany()
                        .HasForeignKey("CompetitionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.Checkpoint", "InstantArrivalCheckpoint")
                        .WithMany()
                        .HasForeignKey("InstantArrivalCheckpointId");
                });

            modelBuilder.Entity("Parsec.Timing.Model.Tournament", b =>
                {
                    b.HasOne("Parsec.Timing.Model.MultiEventTournament", "MultiEventTournament")
                        .WithMany()
                        .HasForeignKey("MultiEventTournamentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Timing.Model.TournamentSeries", "TournamentSeries")
                        .WithMany()
                        .HasForeignKey("TournamentSeriesId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentCompetition", b =>
                {
                    b.HasOne("Parsec.Timing.Model.AgeGroupSet", "AgeGroupSet")
                        .WithMany()
                        .HasForeignKey("AgeGroupSetId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Competition", "Competition")
                        .WithMany()
                        .HasForeignKey("CompetitionId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.ParticipantStatus", "DefaultParticipantStatus")
                        .WithMany()
                        .HasForeignKey("DefaultParticipantStatusId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.Tournament", "Tournament")
                        .WithMany()
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentCompetitionSplit", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Split", "Split")
                        .WithMany()
                        .HasForeignKey("SplitId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Parsec.Timing.Model.TournamentCompetition", "TournamentCompetition")
                        .WithMany()
                        .HasForeignKey("TournamentCompetitionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Timing.Model.TournamentOfficial", b =>
                {
                    b.HasOne("Parsec.Timing.Model.Tournament", "Tournament")
                        .WithMany()
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}

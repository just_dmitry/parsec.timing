$(function () {
    initBackgroundJobsInfo();
});
var verificationTokenField = "__RequestVerificationToken";
var verificationToken;
function initAjaxForm(form, successCallback) {
    var cb = (function (success) { return function (evt) { return doAjaxFormSubmit(evt, success); }; })(successCallback);
    form.submit(cb);
    $.get("/token", function (data) { verificationToken = data; });
}
function doAjaxFormSubmit(eventObject, successCallback) {
    eventObject.preventDefault();
    var form = $(eventObject.target);
    var cb = (function (success, form) { return function (data) { return onAjaxFormSuccess(form, data, success); }; })(successCallback, form);
    var cb2 = (function (form) { return function () { return onAjaxFormFail(form); }; })(form);
    form.find('button').addClass('disabled').prop('disabled', true).addClass('ajax-submit-disabled');
    $.ajax(form.attr("action"), {
        method: "POST",
        data: form.serializeArray().concat([{ name: verificationTokenField, value: verificationToken }]),
        success: cb,
        error: cb2
    });
}
function onAjaxFormFail(form) {
    form.find('button.ajax-submit-disabled').removeClass('disabled').removeProp('disabled').removeAttr('disabled').removeClass('ajax-submit-disabled');
    alert("Непредвиденная ошибка на сервере при обработке запроса. Попробуйте ещё раз, если ошибка сохранится - обратитесь к программистам.");
}
function onAjaxFormSuccess(form, data, successCallback) {
    form.find('button.ajax-submit-disabled').removeClass('disabled').removeProp('disabled').removeAttr('disabled').removeClass('ajax-submit-disabled');
    if (!data.success) {
        var msg = (data.message ? data.message + '\r\n\r\n' : '') + data.errors.concat('\r\n');
        alert(msg);
        return;
    }
    if (data.message) {
        alert(data.message);
    }
    if (successCallback) {
        successCallback(data);
    }
    if (data.newUri) {
        window.location.href = data.newUri;
    }
}
var backgroundJobsInfo = null;
var backgroundJobsInfoInitialized = false;
var backgroundJobsInfoOpened = false;
var backgroundJobsInfoCounter = 0;
function initBackgroundJobsInfo() {
    backgroundJobsInfo = $("#background-jobs");
    backgroundJobsInfo
        .click(function () { showBackgroundJobsInfo(); return false; })
        .on('show.bs.popover', function () { backgroundJobsInfoOpened = true; })
        .on('hide.bs.popover', function () { backgroundJobsInfoOpened = false; });
}
function showBackgroundJobsInfo() {
    if (!backgroundJobsInfoInitialized) {
        backgroundJobsInfo.popover({
            animation: false,
            container: 'body',
            trigger: 'focus',
            title: 'Фоновые задачи',
            html: true,
            content: 'Нет фоновых задач',
            placement: 'bottom'
        });
        backgroundJobsInfoInitialized = true;
    }
    if (!backgroundJobsInfoOpened) {
        backgroundJobsInfoCounter = 0;
        backgroundJobsInfo.data('bs.popover').config.content = "<span class='text-info'>Данные обновляются...</a>";
        backgroundJobsInfo.popover('show');
    }
    setTimeout(refreshBackgroundJobsInfo, 750);
}
function refreshBackgroundJobsInfo() {
    $.getJSON("/backgroundjobs", onNewBackgroundJobsInfo);
}
function onNewBackgroundJobsInfo(data) {
    if (!backgroundJobsInfoOpened)
        return;
    var text = '';
    var activeJobsCount = data.activeJobs.length;
    if (data.isRunning || (activeJobsCount && activeJobsCount > 0)) {
        for (var i = --activeJobsCount; i >= 0; i--) {
            text = text + (i == 0 ? "<i class='fa fa-refresh fa-spin'></i> " : "<i class='fa fa-hrouglass'></i>");
            text = text + htmlEncode(data.activeJobs[i].name) + ': ' + data.activeJobs[i].progress + '%' + '<br/>';
        }
    }
    else {
        text = "Все задачи выполнены" + '<br/>';
    }
    if (data.completedJobs.length > 0) {
        text = text + '<br/>';
        for (var i = 0; i < data.completedJobs.length; i++) {
            text = text + (data.completedJobs[i].success ? "<span class='text-muted'><i class='fa fa-check'></i> " : "<span class='text-danger'><i class='fa fa-exclamation-triangle'></i> ") + data.completedJobs[i].completedAt + ": " + htmlEncode(data.completedJobs[i].name) + '</span><br/>';
        }
    }
    backgroundJobsInfo.data('bs.popover').config.content = text;
    backgroundJobsInfo.popover('show');
    if (data.isRunning) {
        setTimeout(refreshBackgroundJobsInfo, 750);
        backgroundJobsInfoCounter = 0;
    }
    else if (backgroundJobsInfoCounter < 3) {
        setTimeout(refreshBackgroundJobsInfo, 750);
        backgroundJobsInfoCounter++;
    }
    else {
        setTimeout(function () { backgroundJobsInfo.popover('hide'); }, 3000);
    }
}
function htmlEncode(value) {
    return $('<div/>').text(value).html();
}
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

﻿namespace Parsec.Timing.Server
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;

    public class AreaModelConvention : IControllerModelConvention
    {
        public static string GetAreaName(Type controllerType)
        {
            string[] tokens = controllerType.FullName.Split('.');

            return tokens
              .SkipWhile(t => !t.Equals("Areas", StringComparison.OrdinalIgnoreCase))
              .Skip(1)
              .Take(1)
              .FirstOrDefault();
        }

        public void Apply(ControllerModel controller)
        {
            controller.RouteValues["area"] = GetAreaName(controller.ControllerType.AsType());
        }
    }
}

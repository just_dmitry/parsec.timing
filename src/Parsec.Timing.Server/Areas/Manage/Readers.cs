﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.Services;

    public class Readers : Controller
    {
        private const string Name = nameof(Readers);

        private readonly ReadersService readersService;
        private readonly ILogger logger;

        public Readers(ReadersService readersService, ILogger<Readers> logger)
        {
            this.readersService = readersService;
            this.logger = logger;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(Readers), nameof(Index));
        }

        public static UrlActionContext StartLineRoute()
        {
            return new UrlActionInfo(typeof(Readers), nameof(StartLine));
        }

        [HttpGet]
        public IActionResult StartLine()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(readersService);
        }

        [HttpPost]
        [ActionName("Index")]
        public IActionResult IndexSubmit(string command)
        {
            switch (command)
            {
                case "start":
                    readersService.MylapsListener.Start();
                    break;
                case "stop":
                    readersService.MylapsListener.Stop();
                    break;
            }

            return Redirect(Url.Action(IndexRoute()));
        }
    }
}

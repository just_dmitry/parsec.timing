﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.SignalR;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.Services;

    public class ReadersHub : Hub
    {
        private readonly ILogger logger;

        private readonly ReadersService readersService;

        public ReadersHub(ILogger<ReadersHub> logger, ReadersService readersService)
        {
            this.logger = logger;
            this.readersService = readersService;
        }

        public override Task OnConnectedAsync()
        {
            return Clients.Client(Context.ConnectionId).InvokeAsync("Registered", readersService.TagMappings);
        }
    }
}

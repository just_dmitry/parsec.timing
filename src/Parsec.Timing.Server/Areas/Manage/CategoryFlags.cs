﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    [RequireTournamentContext]
    public class CategoryFlags : Controller
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentTournamentService;

        private readonly ICacheService cacheService;

        public CategoryFlags(TimingDb db, ICurrentTournamentService currentTournamentService, ICacheService cacheService)
        {
            this.db = db;
            this.currentTournamentService = currentTournamentService;
            this.cacheService = cacheService;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(CategoryFlags), nameof(Index));
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionInfo(typeof(CategoryFlags), nameof(Create));
        }

        public static UrlActionContext EditRoute(int id)
        {
            return new UrlActionInfo(typeof(CategoryFlags), nameof(Edit)).Add("id", id);
        }

        public static UrlActionContext DeleteRoute(int id)
        {
            return new UrlActionInfo(typeof(CategoryFlags), nameof(Delete)).Add("id", id);
        }

        public async Task<IActionResult> Index()
        {
            var model = await db.CategoryFlags.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournamentService.Id)
                .OrderBy(x => x.Id)
                .ToListAsync();

            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            var ids = await db.CategoryFlags.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournamentService.Id)
                .Select(x => x.Id)
                .ToArrayAsync();

            var newId = 1;
            while (ids.Contains(newId))
            {
                newId *= 2;
            }

            var model = new CategoryFlag
            {
                Id = newId,
                MultiEventTournamentId = currentTournamentService.Id
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CategoryFlag model)
        {
            if (model.MultiEventTournamentId != currentTournamentService.Id)
            {
                ModelState.AddModelError(string.Empty, $"Несовпадение {nameof(model.MultiEventTournamentId)}.");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.CategoryFlags.Add(model);
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(currentTournamentService.Id);

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await db.CategoryFlags.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournamentService.Id)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, CategoryFlag model)
        {
            if (id != model.Id)
            {
                ModelState.AddModelError(nameof(model.Id), "Несовпадение кода. Код менять запрещено.");
            }

            if (model.MultiEventTournamentId != currentTournamentService.Id)
            {
                ModelState.AddModelError(string.Empty, $"Несовпадение {nameof(model.MultiEventTournamentId)}.");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.CategoryFlags.Update(model);
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(currentTournamentService.Id);

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Delete(int id)
        {
            var model = await db.CategoryFlags.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournamentService.Id)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var model = await db.CategoryFlags.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournamentService.Id)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            db.CategoryFlags.Remove(model);
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(currentTournamentService.Id);

            return Redirect(Url.Action(IndexRoute()));
        }
    }
}

﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    public class AgeGroupSets : Controller
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentTournmentService;

        public AgeGroupSets(TimingDb db, ICurrentTournamentService currentTournmentService)
        {
            this.db = db;
            this.currentTournmentService = currentTournmentService;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(AgeGroupSets), nameof(Index));
        }

        [RequireTournamentContext]
        public async Task<IActionResult> Index()
        {
            var ags = await db.AgeGroupSets.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournmentService.Id)
                .OrderBy(x => x.Name)
                .ToListAsync();

            var ids = ags.Select(x => x.Id).ToArray();

            var agsc = await db.AgeGroupSetContents.AsNoTracking()
                .Include(x => x.AgeGroup)
                .Where(x => ids.Contains(x.AgeGroupSetId))
                .ToListAsync();

            var data = ags
                .Select(x => Tuple.Create(
                                x,
                                agsc.Where(y => y.AgeGroupSetId == x.Id)
                                    .Select(y => y.AgeGroup)
                                    .OrderBy(y => y.MinAge)
                                    .ThenBy(y => y.MaxAge).ToList()))
                .ToList();

            return View(data);
        }
    }
}

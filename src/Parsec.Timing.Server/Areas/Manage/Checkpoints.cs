﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    public class Checkpoints : Controller
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentTournamentService;

        public Checkpoints(TimingDb db, ICurrentTournamentService currentTournamentService)
        {
            this.db = db;
            this.currentTournamentService = currentTournamentService;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(Checkpoints), nameof(Index));
        }

        [RequireTournamentContext]
        public async Task<IActionResult> Index()
        {
            var list = await db.Checkpoints
                .AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentTournamentService.Id)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            return View(list);
        }
    }
}

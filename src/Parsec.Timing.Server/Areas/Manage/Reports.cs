﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;
    using Services;

    [RequireTournamentContext]
    public class Reports : Controller
    {
        private readonly TimingDb db;
        private readonly ILogger logger;
        private readonly ICurrentTournamentService currentMetService;
        private readonly ICacheService cacheService;

        public Reports(ILogger<Reports> logger, TimingDb db, ICurrentTournamentService currentMetService, ICacheService cacheService)
        {
            this.logger = logger;
            this.db = db;
            this.currentMetService = currentMetService;
            this.cacheService = cacheService;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(Reports), nameof(Index));
        }

        public static UrlActionContext CreateRoute(ReportType type)
        {
            return new UrlActionInfo(typeof(Reports), nameof(Create)).Add("type", type);
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Reports), nameof(Edit)).Add("id", id);
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Reports), nameof(Delete)).Add("id", id);
        }

        public static UrlActionContext FieldsRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Reports), nameof(Fields)).Add("id", id);
        }

        public async Task<IActionResult> Index()
        {
            var data = await db.Reports.AsNoTracking()
                .Include(x => x.Fields)
                .Include(x => x.Tournament)
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .OrderBy(x => x.Name)
                .ToListAsync();

            return View(data);
        }

        public async Task<IActionResult> Create(ReportType type)
        {
            var model = new Report
            {
                Id = SequentialGuid.NewSequentialGuid(),
                Type = type,
            };

            ViewBag.Tournaments = await db.Tournaments.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .OrderBy(x => x.Name)
                .ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Report model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Tournaments = await db.Tournaments.AsNoTracking()
                   .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                   .OrderBy(x => x.Name)
                   .ToListAsync();

                return View(model);
            }

            model.MultiEventTournamentId = currentMetService.Id;

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(currentMetService.Id);

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await db.Reports.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            ViewBag.Tournaments = await db.Tournaments.AsNoTracking()
               .Where(x => x.MultiEventTournamentId == currentMetService.Id)
               .OrderBy(x => x.Name)
               .ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, Report model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Tournaments = await db.Tournaments.AsNoTracking()
                   .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                   .OrderBy(x => x.Name)
                   .ToListAsync();
                return View(model);
            }

            model.MultiEventTournamentId = currentMetService.Id;

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(currentMetService.Id);

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var model = await db.Reports.AsNoTracking()
                .Include(x => x.Tournament)
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Reports
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(currentMetService.Id);

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Fields(Guid id)
        {
            var report = await db.Reports.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (report == null)
            {
                return NotFound();
            }

            ViewBag.Fields = await db.Fields.AsNoTracking().OrderBy(x => x.SortOrder).ToListAsync();
            ViewBag.AdditionalFields = await db.MultiEventTournamentFields
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .Where(x => x.IsUsed)
                .OrderBy(x => x.SortOrder)
                .Select(x => x.FieldId)
                .ToArrayAsync();

            var list = await db.ReportFields.AsNoTracking()
                .Where(x => x.ReportId == id)
                .OrderBy(x => x.SortOrder)
                .ToArrayAsync();
            var model = new ReportFieldsViewModel
            {
                Ids = list.Select(x => x.FieldId).ToArray(),
                Titles = list.Select(x => x.Title).ToArray(),
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Fields(Guid id, ReportFieldsViewModel model)
        {
            if (model.Ids?.Length == 0)
            {
                ModelState.AddModelError(string.Empty, "Необходимо выбрать хотя бы одно поле");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Fields = await db.Fields.AsNoTracking().OrderBy(x => x.SortOrder).ToListAsync();
                ViewBag.AdditionalFields = await db.MultiEventTournamentFields
                    .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                    .Where(x => x.IsUsed)
                    .OrderBy(x => x.SortOrder)
                    .Select(x => x.FieldId)
                    .ToArrayAsync();
                return View(model);
            }

            var report = await db.Reports.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (report == null)
            {
                return NotFound();
            }

            var oldData = await db.ReportFields.Where(x => x.ReportId == id).ToListAsync();

            db.ReportFields.RemoveRange(oldData);

            for (var i = 0; i < model.Ids.Length; i++)
            {
                db.ReportFields.Add(new ReportField
                {
                    ReportId = id,
                    FieldId = model.Ids[i],
                    SortOrder = i,
                    Title = model.Titles[i]
                });
            }

            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }
    }
}

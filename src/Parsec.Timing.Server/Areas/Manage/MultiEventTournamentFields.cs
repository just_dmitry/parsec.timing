﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    [RequireTournamentContext]
    public class MultiEventTournamentFields : Controller
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentMetService;

        private readonly IMultiEventTournamentFieldService metService;

        private readonly ICacheService cacheService;

        public MultiEventTournamentFields(
            TimingDb db,
            ICurrentTournamentService currentMetService,
            IMultiEventTournamentFieldService metService,
            ICacheService cacheService)
        {
            this.db = db;
            this.currentMetService = currentMetService;
            this.metService = metService;
            this.cacheService = cacheService;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(MultiEventTournamentFields), nameof(Index));
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Fields = await db.Fields.AsNoTracking()
                .Where(x => x.Source == FieldSource.ParticipantField)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            var array = await db.MultiEventTournamentFields
                .Where(x => x.MultiEventTournamentId == currentMetService.Id && x.IsUsed)
                .OrderBy(x => x.SortOrder)
                .Select(x => x.FieldId)
                .ToArrayAsync();

            return View(array);
        }

        [HttpPost]
        public async Task<IActionResult> Index([ModelBinder(Name = "id")] Guid[] model)
        {
            if (model.Length == 0)
            {
                ModelState.AddModelError(string.Empty, "Необходимо выбрать хотя бы одно поле");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Fields = await db.Fields.AsNoTracking()
                    .Where(x => x.Source == FieldSource.ParticipantField)
                    .OrderBy(x => x.SortOrder)
                    .ToListAsync();
                return View(model);
            }

            await metService.SetAsync(currentMetService.Id, model);

            cacheService.RemoveMultiEventTournamentInfo(currentMetService.Id);

            return Redirect(Url.Action(Default.Home.IndexRoute()));
        }
    }
}

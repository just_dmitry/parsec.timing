﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    public class CompetitionPools : Controller
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentTournamentService;

        public CompetitionPools(TimingDb db, ICurrentTournamentService currentTournamentService)
        {
            this.db = db;
            this.currentTournamentService = currentTournamentService;
        }

        public static UrlActionContext LockRoute(Guid id)
        {
            return new UrlActionInfo(typeof(CompetitionPools), nameof(Lock)).Add("id", id);
        }

        public static UrlActionContext UnlockRoute(Guid id)
        {
            return new UrlActionInfo(typeof(CompetitionPools), nameof(Unlock)).Add("id", id);
        }

        [RequireTournamentContext]
        public async Task<IActionResult> Lock(Guid id)
        {
            var obj = await db.CompetitionPools
                .AsNoTracking()
                .Where(x => x.Id == id && x.MultiEventTournamentId == currentTournamentService.Id)
                .SingleOrDefaultAsync();

            if (obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        [HttpPost]
        [RequireTournamentContext]
        [ActionName("Lock")]
        public async Task<IActionResult> LockSubmit(Guid id, [FromServices] ICacheService cacheService)
        {
            var obj = await db.CompetitionPools
                .Where(x => x.Id == id && x.MultiEventTournamentId == currentTournamentService.Id)
                .SingleOrDefaultAsync();

            if (obj == null)
            {
                return NotFound();
            }

            obj.IsLocked = true;
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(obj.MultiEventTournamentId);

            return Redirect(Url.Action(Default.Home.IndexRoute()));
        }

        [RequireTournamentContext]
        public async Task<IActionResult> Unlock(Guid id)
        {
            var obj = await db.CompetitionPools
                .AsNoTracking()
                .Where(x => x.Id == id && x.MultiEventTournamentId == currentTournamentService.Id)
                .SingleOrDefaultAsync();

            if (obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        [HttpPost]
        [RequireTournamentContext]
        [ActionName("Unlock")]
        public async Task<IActionResult> UnlockSubmit(Guid id, [FromServices] ICacheService cacheService)
        {
            var obj = await db.CompetitionPools
                .Where(x => x.Id == id && x.MultiEventTournamentId == currentTournamentService.Id)
                .SingleOrDefaultAsync();

            if (obj == null)
            {
                return NotFound();
            }

            obj.IsLocked = false;
            await db.SaveChangesAsync();

            cacheService.RemoveMultiEventTournamentInfo(obj.MultiEventTournamentId);

            return Redirect(Url.Action(Default.Home.IndexRoute()));
        }
    }
}

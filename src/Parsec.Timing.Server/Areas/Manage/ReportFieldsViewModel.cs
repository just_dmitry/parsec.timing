﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using Microsoft.AspNetCore.Mvc;

    public class ReportFieldsViewModel
    {
        [ModelBinder(Name = "fieldId")]
        public Guid[] Ids { get; set; }

        [ModelBinder(Name = "title")]
        public string[] Titles { get; set; }
    }
}

﻿namespace Parsec.Timing.Server.Areas.Manage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Logging;
    using Services;

    public class MultiEventTournaments : Controller
    {
        private const string Name = nameof(MultiEventTournaments);

        private readonly TimingDb db;
        private readonly ILogger logger;

        public MultiEventTournaments(ILogger<MultiEventTournaments> logger, TimingDb db)
        {
            this.logger = logger;
            this.db = db;
        }

        public static UrlActionContext StructureRoute()
        {
            return new UrlActionInfo(typeof(MultiEventTournaments), nameof(Structure));
        }

        [RequireTournamentContext(false)]
        public IActionResult Structure()
        {
            // Not very good, but will read all data directly from DbContext
            return View();
        }
    }
}

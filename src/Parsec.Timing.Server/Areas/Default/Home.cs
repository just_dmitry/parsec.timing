﻿namespace Parsec.Timing.Server.Areas.Default
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.ResultProcessing;
    using RecurrentTasks;
    using Services;

    public class Home : Controller
    {
        private readonly ILogger logger;

        public Home(ILogger<Home> logger)
        {
            this.logger = logger;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(Index));
        }

        public static UrlActionContext SetActiveRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(SetActive));
        }

        public static UrlActionContext SetActiveConfirmRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Home), nameof(SetActiveConfirm)).Add("id", id);
        }

        public static UrlActionContext ResetActiveRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(ResetActive));
        }

        public static UrlActionContext LockedUnavailableRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(LockedUnavailable));
        }

        public static UrlActionContext BackgroundJobsRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(BackgroundJobs));
        }

        public static UrlActionContext AboutRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(About));
        }

        public static UrlActionContext LogsRoute()
        {
            return new UrlActionInfo(typeof(Home), nameof(Logs));
        }

        [RequireTournamentContext(false)]
        public async Task<IActionResult> Index(
            [FromServices] ICacheService cacheService,
            [FromServices] ICurrentTournamentService currentMetService)
        {
            var item = await cacheService.GetMultiEventTournamentInfoAsync(currentMetService.Id);

            return View(item);
        }

        public async Task<IActionResult> SetActive([FromServices] TimingDb db)
        {
            var boundary = DateTime.Now.AddMonths(-12);

            var list = await db.MultiEventTournaments
                .AsNoTracking()
                .Where(x => x.EndDate >= boundary)
                .OrderByDescending(x => x.StartDate)
                .ToListAsync();
            return View(list);
        }

        public async Task<IActionResult> SetActiveConfirm(
            Guid id,
            [FromServices] TimingDb db,
            [FromServices] ITask<ResultProcessingTask> resultProcessingTask,
            [FromServices] ICurrentTournamentService currentMetService)
        {
            if (!await currentMetService.SetAsync(id, db))
            {
                return NotFound();
            }

            logger.LogInformation($"Current MultiEventTiournament is set: {currentMetService.Title} (#{currentMetService.Id})");

            resultProcessingTask.Start(TimeSpan.Zero);

            return Redirect(Url.Action(IndexRoute()));
        }

        public IActionResult ResetActive(
            [FromServices] ICurrentTournamentService currentMetService,
            [FromServices] ITask<ResultProcessingTask> resultProcessingTask,
            [FromServices] ICacheService cacheService)
        {
            if (currentMetService.IsSet)
            {
                cacheService.RemoveMultiEventTournamentInfo(currentMetService.Id);
            }

            if (resultProcessingTask.IsStarted)
            {
                resultProcessingTask.Stop();
            }

            currentMetService.Reset();

            logger.LogInformation($"Current MultiEventTiournament is cleared.");

            return Redirect(Url.Action(SetActiveRoute()));
        }

        public IActionResult LockedUnavailable()
        {
            return View();
        }

        [Route("/token")]
        public IActionResult Token([FromServices] Microsoft.AspNetCore.Antiforgery.IAntiforgery antiforgery)
        {
            return Content(antiforgery.GetTokens(HttpContext).RequestToken, "text/plain");
        }

        public IActionResult BackgroundJobs([FromServices] BackgroundProcessingService backgroundProcessingService)
        {
            var data = new Dto.BackgroundProcessingInfo
            {
                IsRunning = backgroundProcessingService.Items?.Count != 0,
                ActiveJobs = backgroundProcessingService.Items
                    .Select(x => new Dto.BackgroundProcessingInfo.ActiveJobInfo(x.Name, x.Progress)).ToList(),
                CompletedJobs = backgroundProcessingService.ProcessedItems
                    .Select(x => new Dto.BackgroundProcessingInfo.CompletedJobInfo(x.Item2, x.Item1.ToMoscowTime().ToString("t"), x.Item3)).ToList(),
            };

            return Json(data);
        }

        [AllowAnonymous]
        [Route("/about")]
        public IActionResult About()
        {
            return View();
        }

        [Route("/logs")]
        public IActionResult Logs()
        {
            return View(iflight.Logging.MemoryLogger.LogList);
        }
    }
}

﻿namespace Parsec.Timing.Server.Areas.Start
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Services;

    public class Home : Controller
    {
        private readonly ILogger logger;

        public Home(ILogger<Home> logger)
        {
            this.logger = logger;
        }

        public static UrlActionContext ReprocessStartRoute(Guid competitionPoolId)
        {
            return new UrlActionInfo(typeof(Home), nameof(ReprocessStart)).Add("id", competitionPoolId);
        }

        [RequireTournamentContext]
        public async Task<IActionResult> ReprocessStart(
            Guid id,
            [FromServices] TimingDb db,
            [FromServices] ICurrentTournamentService currentTournamentService,
            [FromServices] BackgroundProcessingService backgroundService)
        {
            var pool = await db.CompetitionPools.AsNoTracking().SingleAsync(x => x.Id == id);

            if (pool.MultiEventTournamentId != currentTournamentService.Id)
            {
                return NotFound();
            }

            var job = ParticipationProcessingJob.Create(pool);

            backgroundService.Add(job);

            return Json(new Dto.ServerResponse { Success = true });
        }
    }
}

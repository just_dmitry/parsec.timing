﻿namespace Parsec.Timing.Server.Areas.Start
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Services;

    [RequireTournamentContext]
    public class Participants : Controller
    {
        private const string Name = nameof(Participants);

        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentMetService;

        public Participants(TimingDb db, ICurrentTournamentService currentMetService)
        {
            this.db = db;
            this.currentMetService = currentMetService;
        }

        public static UrlActionContext ListRoute(Guid competitionPoolId)
        {
            return new UrlActionInfo(typeof(Participants), nameof(List)).Add("id", competitionPoolId);
        }

        public static UrlActionContext AddRoute(Guid competitionId)
        {
            return new UrlActionInfo(typeof(Participants), nameof(Add)).Add("id", competitionId);
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Participants), nameof(Edit)).Add("id", id);
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Participants), nameof(Delete)).Add("id", id);
        }

        [RequireTournamentContext(false)]
        public async Task<IActionResult> List(Guid id)
        {
            var competitions = await db.Competitions.AsNoTracking()
                .Include(x => x.CompetitionPool)
                .Where(x => x.MultiEventTournamentId == currentMetService.Id)
                .Where(x => x.CompetitionPoolId == id)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            var ids = competitions.Select(x => x.Id).ToArray();

            var data = await db.Participants
                .Include(x => x.Person)
                .Where(x => ids.Contains(x.CompetitionId))
                .OrderBy(x => x.SortOrder)
                .ThenBy(x => x.Person.LastName)
                .ToListAsync();

            ViewBag.Competitions = competitions;

            return View(data);
        }

        public async Task<IActionResult> Add(Guid id)
        {
            var competition = await db.Competitions.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (competition == null)
            {
                return NotFound();
            }

            var obj = new Model.Participant()
            {
                CompetitionId = id,
            };

            ViewBag.Competitions = await db.Competitions.AsNoTracking()
                .Where(x => x.CompetitionPoolId == competition.CompetitionPoolId)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            return View(obj);
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var obj = await db.Participants
                .AsNoTracking()
                .Include(x => x.Person)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var obj = await db.Participants
                .AsNoTracking()
                .Include(x => x.Person)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }
    }
}

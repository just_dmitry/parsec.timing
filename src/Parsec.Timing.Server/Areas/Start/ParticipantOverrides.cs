﻿namespace Parsec.Timing.Server.Areas.Start
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.Services;

    public class ParticipantOverrides : Controller
    {
        private readonly TimingDb db;
        private readonly ILogger logger;

        public ParticipantOverrides(ILogger<ParticipantOverrides> logger, TimingDb db)
        {
            this.logger = logger;
            this.db = db;
        }

        public static UrlActionContext IndexRoute(Guid competitionPoolId)
        {
            return new UrlActionInfo(typeof(ParticipantOverrides), nameof(Index)).Add("id", competitionPoolId);
        }

        [RequireTournamentContext(false)]
        public async Task<IActionResult> Index(
            Guid id,
            [FromServices] ICurrentTournamentService currentTournamentService)
        {
            var list = await db.ParticipantOverrides
                .Include(x => x.Competition)
                .Include(x => x.Tournament)
                .AsNoTracking()
                .Where(x => x.CompetitionPoolId == id)
                .Where(x => x.CompetitionPool.MultiEventTournamentId == currentTournamentService.Id)
                .OrderBy(x => x.InsertTime)
                .ToListAsync();
            return View(list);
        }
    }
}

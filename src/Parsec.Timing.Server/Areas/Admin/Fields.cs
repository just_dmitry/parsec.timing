﻿namespace Parsec.Timing.Server.Areas.Admin
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    public class Fields : Controller
    {
        private TimingDb db;

        public Fields(TimingDb db)
        {
            this.db = db;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(Fields), nameof(Index));
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionInfo(typeof(Fields), nameof(Create));
        }

        public static UrlActionContext RestoreDefaultRoute()
        {
            return new UrlActionInfo(typeof(Fields), nameof(RestoreDefault));
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Fields), nameof(Edit)).Add("id", id);
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionInfo(typeof(Fields), nameof(Delete)).Add("id", id);
        }

        public async Task<IActionResult> Index()
        {
            var data = await db.Fields.OrderBy(x => x.SortOrder).ThenBy(x => x.Name).ToListAsync();

            return View(data);
        }

        public IActionResult Create()
        {
            var model = new Field { Id = SequentialGuid.NewSequentialGuid() };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Field model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> RestoreDefault([FromServices] SeedService seedService)
        {
            await seedService.RestoreDefaultFields();

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await db.Fields.SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, Field model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var model = await db.Fields.SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Fields.SingleOrDefaultAsync(x => x.Id == id);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }
    }
}

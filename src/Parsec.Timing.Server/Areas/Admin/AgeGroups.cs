﻿namespace Parsec.Timing.Server.Areas.Admin
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Services;

    public class AgeGroups : Controller
    {
        private TimingDb db;

        public AgeGroups(TimingDb db)
        {
            this.db = db;
        }

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionInfo(typeof(AgeGroups), nameof(Index));
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionInfo(typeof(AgeGroups), nameof(Create));
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionInfo(typeof(AgeGroups), nameof(Edit)).Add("id", id);
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionInfo(typeof(AgeGroups), nameof(Delete)).Add("id", id);
        }

        public async Task<IActionResult> Index()
        {
            var data = await db.AgeGroups.OrderBy(x => x.MinAge).ThenBy(x => x.MaxAge).ToListAsync();

            return View(data);
        }

        public IActionResult Create()
        {
            var model = new AgeGroup { Id = SequentialGuid.NewSequentialGuid() };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(AgeGroup model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await db.AgeGroups.SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, AgeGroup model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var model = await db.AgeGroups.SingleOrDefaultAsync(x => x.Id == id);

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.AgeGroups.SingleOrDefaultAsync(x => x.Id == id);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute()));
        }
    }
}

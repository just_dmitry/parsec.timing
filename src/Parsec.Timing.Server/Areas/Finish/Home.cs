﻿namespace Parsec.Timing.Server.Areas.Finish
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.ResultProcessing;
    using Services;

    public class Home : Controller
    {
        private readonly ILogger logger;

        private readonly ICurrentTournamentService currentTournamentService;

        public Home(ICurrentTournamentService currentTournamentService, ILogger<Home> logger)
        {
            this.currentTournamentService = currentTournamentService;
            this.logger = logger;
        }

        public static UrlActionContext IndexRoute(Guid competitionPoolId)
        {
            return new UrlActionInfo(typeof(Home), nameof(Index)).Add("id", competitionPoolId);
        }

        public static UrlActionContext ProcessResultsRoute(Guid competitionPoolId)
        {
            return new UrlActionInfo(typeof(Home), nameof(ReprocessResults)).Add("id", competitionPoolId);
        }

        [RequireTournamentContext(false)]
        public async Task<IActionResult> Index(
            Guid id,
            [FromServices] ICacheService cacheService)
        {
            if (!currentTournamentService.CompetitionPools.Contains(id))
            {
                return NotFound();
            }

            return View(await cacheService.GetCompetitionPoolFinishInfoAsync(id));
        }

        [RequireTournamentContext]
        public async Task<IActionResult> ReprocessResults(
            Guid id,
            [FromServices] TimingDb db,
            [FromServices] BackgroundProcessingService backgroundService,
            [FromServices] ILogger<ResultProcessingJob> resultProcessingJobLogger)
        {
            if (!currentTournamentService.CompetitionPools.Contains(id))
            {
                return NotFound();
            }

            var pool = await db.CompetitionPools.AsNoTracking().SingleAsync(x => x.Id == id);

            var job = ResultProcessingJob.Create(pool, resultProcessingJobLogger);

            backgroundService.Add(job);

            return Json(new Dto.ServerResponse { Success = true });
        }
    }
}

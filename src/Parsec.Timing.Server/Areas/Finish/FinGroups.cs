﻿namespace Parsec.Timing.Server.Areas.Finish
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using EFCore.BulkExtensions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Parsec.Timing.Server.Dto;
    using Services;

    public class FinGroups : Controller
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentTournamentService;

        private readonly ICacheService cacheService;

        private readonly IFinGroupService finGroupsService;

        private readonly CommonOperationsService commonOperationsService;

        public FinGroups(
            TimingDb db,
            ICurrentTournamentService currentTournamentService,
            ICacheService cacheService,
            IFinGroupService finGroupsService,
            CommonOperationsService commonOperationsService)
        {
            this.db = db;
            this.currentTournamentService = currentTournamentService;
            this.cacheService = cacheService;
            this.finGroupsService = finGroupsService;
            this.commonOperationsService = commonOperationsService;
        }

        public static UrlActionContext AppendRoute()
        {
            return new UrlActionInfo(typeof(FinGroups), nameof(Append));
        }

        public static UrlActionContext DetailsRoute(int id)
        {
            return new UrlActionInfo(typeof(FinGroups), nameof(Details)).Add("id", id);
        }

        public static UrlActionContext DisableRoute(int id)
        {
            return new UrlActionInfo(typeof(FinGroups), nameof(Disable)).Add("id", id);
        }

        public static UrlActionContext EnableRoute(int id)
        {
            return new UrlActionInfo(typeof(FinGroups), nameof(Enable)).Add("id", id);
        }

        public static UrlActionContext DeleteRoute(int id)
        {
            return new UrlActionInfo(typeof(FinGroups), nameof(Delete)).Add("id", id);
        }

        public static UrlActionContext ShiftRoute(int id)
        {
            return new UrlActionInfo(typeof(FinGroups), nameof(Shift)).Add("id", id);
        }

        [HttpPost]
        [RequireTournamentContext]
        public async Task<IActionResult> Append(AddFinishGroupViewModel model)
        {
            var newFinGroup = new FinGroup
            {
                Name = model.FileName,
                IsDisabled = false,
                Created = DateTimeOffset.Now,
                LastSeen = DateTimeOffset.Now,
                CompetitionPoolId = model.CompetitionPoolId,
                CheckpointId = model.CheckpointId,
                IpAddress = Environment.MachineName,
                ClientNameVersion = System.TypeExtensions.GetAssemblyNameVersion(),
            };

            if (!finGroupsService.ValidateKeys(newFinGroup))
            {
                ModelState.AddModelError(string.Empty, "CompetitionPoolId or CheckpointId: Not found (or wrong MultiEventTournamentId)");
            }

            if (!ModelState.IsValid)
            {
                return Json(ServerResponse.Fail(ModelState));
            }

            var firstWrongLines = new List<string>();
            var wrongLinesCount = 0;

            var results = model.Data
                .Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                .Select((x, i) =>
                {
                    var parsed = Dto.FinData.TryParse(x);
                    if (parsed == null)
                    {
                        wrongLinesCount++;
                        if (wrongLinesCount < 3)
                        {
                            firstWrongLines.Add($"Нераспознанная строка #{i}: {x}");
                        }
                    }

                    return parsed;
                })
                .Where(x => x != null)
                .Select(x => new FinMark
                {
                    Bib = x.Bib,
                    Inserted = DateTimeOffset.Now,
                    Status = FinMarkStatus.Default,
                    OriginalResult = new TimeOrLength(x.Result),
                    RevisedResult = new TimeOrLength(x.Result),
                })
                .ToList();

            if (wrongLinesCount > 0)
            {
                firstWrongLines.Add($"Все нераспознанных строк: {wrongLinesCount}");
                var retVal = new ServerResponse
                {
                    Success = false,
                    Errors = firstWrongLines.ToArray(),
                };
                return Json(retVal);
            }

            newFinGroup.MarksCount = results.Count;

            db.FinGroups.Add(newFinGroup);
            await db.SaveChangesAsync();

            foreach (var fm in results)
            {
                fm.FinGroupId = newFinGroup.Id;
            }

            db.FinMarks.AddRange(results);
            await db.SaveChangesAsync();

            await commonOperationsService.ResetCompetitionPoolResultStatusAsync(newFinGroup.CompetitionPoolId.Value);

            cacheService.RemoveCompetitionPoolFinishInfo(newFinGroup.CompetitionPoolId.Value);

            return Json(ServerResponse.Complete("Данные успешно сохранены"));
        }

        [RequireTournamentContext]
        public async Task<IActionResult> Details(int id)
        {
            var group = await db.FinGroups.AsNoTracking()
                .Include(x => x.CompetitionPool)
                .Include(x => x.Checkpoint)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (group == null)
            {
                return NotFound();
            }

            if (!finGroupsService.ValidateKeys(group))
            {
                return NotFound();
            }

            ViewBag.FinGroup = group;
            ViewBag.CompetitionPool = group.CompetitionPool;
            ViewBag.Checkpoint = group.Checkpoint;

            var data = await db.FinMarks.AsNoTracking()
                .Where(x => x.FinGroupId == id)
                .OrderBy(x => x.OriginalResultValue)
                .ToListAsync();

            return View(data);
        }

        [HttpPost]
        [RequireTournamentContext]
        public async Task<IActionResult> Disable(int id)
        {
            var group = await db.FinGroups.FindAsync(id);
            if (group == null)
            {
                return NotFound();
            }

            if (!finGroupsService.ValidateKeys(group))
            {
                return NotFound();
            }

            group.IsDisabled = true;
            await db.SaveChangesAsync();

            await commonOperationsService.ResetCompetitionPoolResultStatusAsync(group.CompetitionPoolId.Value);
            cacheService.RemoveCompetitionPoolFinishInfo(group.CompetitionPoolId.Value);

            return Redirect(Url.Action(DetailsRoute(id)));
        }

        [HttpPost]
        [RequireTournamentContext]
        public async Task<IActionResult> Enable(int id)
        {
            var group = await db.FinGroups.FindAsync(id);
            if (group == null)
            {
                return NotFound();
            }

            if (!finGroupsService.ValidateKeys(group))
            {
                return NotFound();
            }

            group.IsDisabled = false;
            await db.SaveChangesAsync();

            await commonOperationsService.ResetCompetitionPoolResultStatusAsync(group.CompetitionPoolId.Value);
            cacheService.RemoveCompetitionPoolFinishInfo(group.CompetitionPoolId.Value);

            return Redirect(Url.Action(DetailsRoute(id)));
        }

        [HttpPost]
        [RequireTournamentContext]
        public async Task<IActionResult> Delete(int id)
        {
            var group = await db.FinGroups.FindAsync(id);
            if (group == null)
            {
                return NotFound();
            }

            if (!finGroupsService.ValidateKeys(group))
            {
                return NotFound();
            }

            db.FinGroups.Remove(group);
            await db.SaveChangesAsync();

            if (!group.IsDisabled)
            {
                await commonOperationsService.ResetCompetitionPoolResultStatusAsync(group.CompetitionPoolId.Value);
            }

            cacheService.RemoveCompetitionPoolFinishInfo(group.CompetitionPoolId.Value);

            return Redirect(Url.Action(Home.IndexRoute(group.CompetitionPoolId.Value)));
        }

        [HttpPost]
        [RequireTournamentContext]
        public async Task<IActionResult> Shift(int id, string shiftType, string direction, string value)
        {
            var group = await db.FinGroups.FindAsync(id);
            if (group == null)
            {
                return NotFound();
            }

            if (!finGroupsService.ValidateKeys(group))
            {
                return NotFound();
            }

            TimeOrLength shift;
            switch (shiftType)
            {
                case "T":
                    shift = new TimeOrLength(TimeSpan.Parse(value));
                    break;
                case "L":
                    shift = new TimeOrLength(decimal.Parse(value));
                    break;
                default:
                    var retVal = new ServerResponse
                    {
                        Success = false,
                    };
                    return Json(retVal);
            }

            if (direction == "S")
            {
                shift = new TimeOrLength(shift.Type, -shift.EncodedValue);
            }

            if (shift.EncodedValue == 0)
            {
                shift = TimeOrLength.Zero;
            }

            group.ResultShift = shift;
            await db.SaveChangesAsync();

            await finGroupsService.UpdateMarksRevisedResult(group, db);

            if (!group.IsDisabled)
            {
                await commonOperationsService.ResetCompetitionPoolResultStatusAsync(group.CompetitionPoolId.Value);
            }

            cacheService.RemoveCompetitionPoolFinishInfo(group.CompetitionPoolId.Value);

            return Redirect(Url.Action(DetailsRoute(id)));
        }
    }
}

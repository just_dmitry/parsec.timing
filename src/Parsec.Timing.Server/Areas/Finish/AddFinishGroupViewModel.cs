﻿namespace Parsec.Timing.Server.Areas.Finish
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AddFinishGroupViewModel
    {
        [Required]
        public Guid? CheckpointId { get; set; }

        [Required]
        public Guid? CompetitionPoolId { get; set; }

        [Required]
        [MinLength(3)]
        public string FileName { get; set; }

        [Required]
        [MinLength(10)]
        public string Data { get; set; }
    }
}

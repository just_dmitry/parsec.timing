﻿namespace Parsec.Timing.Server.Areas.Reports
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Model;
    using Newtonsoft.Json;
    using Services;

    [RequireTournamentContext]
    public class ReportGenerator : Controller
    {
        private readonly TimingDb db;

        public ReportGenerator(TimingDb db)
        {
            this.db = db;
        }

        public static UrlActionContext IndexRoute(Guid reportId, Guid tournamentId)
        {
            return new UrlActionInfo(typeof(ReportGenerator), nameof(Index))
                .Add("reportId", reportId)
                .Add("tournamentId", tournamentId);
        }

        public async Task<IActionResult> Index(Guid reportId, Guid tournamentId)
        {
            var report = await db.Reports.AsNoTracking().Include(x => x.Fields).SingleOrDefaultAsync(x => x.Id == reportId);
            if (report == null)
            {
                return NotFound();
            }

            switch (report.Type)
            {
                case ReportType.ParticipantList:
                    var generator = new ReportGenerators.ParticipantList(db);
                    var prms = JsonConvert.DeserializeObject<ReportParamsParticipantList>(report.Params ?? "{}");
                    var model = await generator.GenerateAsync(report, prms, tournamentId);

                    return View("ParticipantList", model);
                default:
                    throw new Exception("Unknown report type: " + report.Type);
            }
        }
    }
}

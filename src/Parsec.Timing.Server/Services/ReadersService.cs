﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.SignalR;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Readers;

    public class ReadersService
    {
        private readonly MylapsMessageHandler mylapsHandler;

        private readonly IHubContext<Areas.Manage.ReadersHub> readersHubContext;

        private readonly ILogger logger;

        public ReadersService(ILoggerFactory loggerFactory, IHubContext<Areas.Manage.ReadersHub> readersHubContext)
        {
            this.readersHubContext = readersHubContext;
            this.logger = loggerFactory.CreateLogger<ReadersService>();

            MylapsListener = new ReaderListener(loggerFactory.CreateLogger<ReaderListener>())
            {
                MessageDelimiter = MylapsMessageHandler.MessageDelimiter,
                TrimNewLines = true,
                Encoding = Encoding.GetEncoding(1251),
            };

            mylapsHandler = new MylapsMessageHandler(loggerFactory.CreateLogger<MylapsMessageHandler>());
            MylapsListener.NewMessage += mylapsHandler.HandleMessageAsync;

            mylapsHandler.NewTagVisits += MylapsHandler_NewTagVisits;

            TagMappings = tagMappingSource
                .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries))
                .ToDictionary(x => x[0], x => x[1]);
        }

        public ReaderListener MylapsListener { get; }

        public Dictionary<string, string> TagMappings { get; }

        private async Task MylapsHandler_NewTagVisits(MylapsMessageHandler source, NewTagVisitsEventArgs args)
        {
            logger.LogDebug($"New tags from {args.Name} (#{args.Sequence}): {args.TagVisits.Length} items");

            foreach (var visit in args.TagVisits)
            {
                TagMappings.TryGetValue(visit.Tag, out var bib);
                await readersHubContext.Clients.All.InvokeAsync("TagVisit", visit.Tag, bib, visit.Time);
            }
        }

        private readonly string tagMappingSource = $@"
KX51831 123
KS22040 11
";
    }
}

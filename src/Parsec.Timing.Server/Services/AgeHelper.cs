﻿namespace Parsec.Timing.Server.Services
{
    using System;

    public static class AgeHelper
    {
        public static readonly byte UnknownAge = 255;

        public static readonly byte OldAgeThreshold = 100;

        public static byte ComputeAge(
            DateTime ageReferenceDate,
            bool ageGroupByFullBirthDate,
            short? birthYear,
            byte? birthMonth,
            byte? birthDay)
        {
            if (!birthYear.HasValue)
            {
                return UnknownAge;
            }

            var diff = ageReferenceDate.Year - birthYear.Value;

            if (!ageGroupByFullBirthDate)
            {
                return FixAge(diff);
            }

            if (!birthDay.HasValue || !birthMonth.HasValue)
            {
                return UnknownAge;
            }

            if (birthMonth.Value > ageReferenceDate.Month)
            {
                diff -= 1;
            }

            if (birthMonth.Value == ageReferenceDate.Month)
            {
                if (birthDay.Value > ageReferenceDate.Day)
                {
                    diff -= 1;
                }
            }

            return FixAge(diff);
        }

        private static byte FixAge(int value)
        {
            return (byte)((value < 0 || value > OldAgeThreshold) ? UnknownAge : value);
        }
    }
}

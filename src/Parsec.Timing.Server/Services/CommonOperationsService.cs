﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class CommonOperationsService
    {
        private readonly TimingDb db;

        private readonly ICurrentTournamentService currentTournamentService;

        private readonly ICacheService cacheService;

        private readonly ILogger logger;

        public CommonOperationsService(
            TimingDb db,
            ICurrentTournamentService currentTournamentService,
            ICacheService cacheService,
            ILogger<CommonOperationsService> logger)
        {
            this.db = db;
            this.currentTournamentService = currentTournamentService;
            this.cacheService = cacheService;
            this.logger = logger;
        }

        public async Task ResetCompetitionPoolResultStatusAsync(Guid id)
        {
            if (!currentTournamentService.IsSet)
            {
                throw new InvalidOperationException("No Current MultiEventTournament");
            }

            var met = await cacheService.GetMultiEventTournamentInfoAsync(currentTournamentService.Id);

            var pools = met.CompetitionPools;

            var cp = pools.FirstOrDefault(x => x.Id == id);
            if (cp == null)
            {
                throw new ArgumentException(nameof(id), "CompetitionPool not found: " + id);
            }

            if (cp.ResultProcessingStatus == Model.ProcessingStatus.None)
            {
                logger.LogDebug($"CompetitionPool.ResultProcessingStatus у {id} уже сброшено, ничего делать не надо");
                return;
            }

            var cp2 = await db.CompetitionPools.SingleAsync(x => x.Id == id);
            cp2.ResultProcessingStatus = Model.ProcessingStatus.None;
            await db.SaveChangesAsync();

            logger.LogDebug($"CompetitionPool.ResultProcessingStatus обновлён у {id}.");

            // отцепляем новый объект от трекинга и кладем на место старого
            db.Entry(cp2).State = EntityState.Detached;
            var index = pools.IndexOf(cp);
            pools[index] = cp2;

            if (cacheService.TryGetCompetitionPoolFinishInfo(id, out Dto.CompetitionPoolFinishInfo info))
            {
                info.CompetitionPool = cp2;
                logger.LogDebug($"ICacheService.CompetitionPoolFinishInfo также обновлена для {id}.");
            }
        }
    }
}

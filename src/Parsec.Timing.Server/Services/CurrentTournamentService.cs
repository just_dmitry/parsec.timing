﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Хранит основные сведения о текущем (выбранном, рабочем) соревновании.
    /// </summary>
    public class CurrentTournamentService : ICurrentTournamentService
    {
        private readonly ILogger logger;

        public CurrentTournamentService(ILogger<CurrentTournamentService> logger)
        {
            this.logger = logger;
            Reset();
        }

        public bool IsSet { get; private set; }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public bool IsLocked { get; private set; }

        public IReadOnlyList<Guid> Tournaments { get; private set; }

        public IReadOnlyList<Guid> CompetitionPools { get; private set; }

        public IReadOnlyList<Guid> Competitions { get; private set; }

        public IReadOnlyList<Guid> Checkpoints { get; private set; }

        public void Reset()
        {
            IsSet = false;
            Id = Guid.Empty;
            Title = "- не выбрано-";
            IsLocked = true;

            Tournaments = new List<Guid>().AsReadOnly();
            CompetitionPools = new List<Guid>().AsReadOnly();
            Competitions = new List<Guid>().AsReadOnly();
            Checkpoints = new List<Guid>().AsReadOnly();

            logger.LogInformation("Reset() done.");
        }

        public async Task<bool> SetAsync(Guid id, TimingDb db)
        {
            var met = await db.MultiEventTournaments
                .Where(x => x.Id == id)
                .Select(x => new { x.Id, x.Name, x.IsLocked })
                .FirstOrDefaultAsync();

            if (met == null)
            {
                return false;
            }

            Id = met.Id;
            Title = met.Name;
            IsLocked = met.IsLocked;
            IsSet = true;

            Tournaments = (await db.Tournaments.Where(x => x.MultiEventTournamentId == id).Select(x => x.Id).ToListAsync()).AsReadOnly();

            CompetitionPools = (await db.CompetitionPools.Where(x => x.MultiEventTournamentId == id).Select(x => x.Id).ToListAsync()).AsReadOnly();

            Competitions = (await db.Competitions.Where(x => x.MultiEventTournamentId == id).Select(x => x.Id).ToListAsync()).AsReadOnly();

            Checkpoints = (await db.Checkpoints.Where(x => x.MultiEventTournamentId == id).Select(x => x.Id).ToListAsync()).AsReadOnly();

            logger.LogInformation($"Set() done for #{Id} '{Title}'");
            return true;
        }
    }
}

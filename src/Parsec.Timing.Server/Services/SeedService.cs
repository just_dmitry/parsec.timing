﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Model;

    public class SeedService
    {
        private TimingDb db;

        private ILogger logger;

        public SeedService(TimingDb db, ILogger<SeedService> logger)
        {
            this.db = db;
            this.logger = logger;
        }

        public async Task RestoreDefaultFields()
        {
            var fields = new[]
            {
                ForPerson("ecab1dd9-6527-4b84-bbc4-bb342b1717ae", 10, "LastName", "Фамилия", "Фамилия", FieldType.String),
                ForPerson("3e149827-c056-4d16-befa-d60b7015ed09", 11, "FirstName", "Имя", "Имя", FieldType.String),
                ForPerson("89851f15-d15c-457a-bd34-4e3d59173c48", 12, "MiddleName", "Отчество", "Отчество", FieldType.String),
                ForPerson("4a9af8cc-da8f-454f-bdcc-95e9ddb69982", 13, "LastNameInternational", "Фамилия (англ)", "Фамилия (англ)", FieldType.String),
                ForPerson("996fb90b-993d-4481-9c81-e41f0d7b9058", 14, "FirstNameInternational", "Имя (англ)", "Имя (англ)", FieldType.String),
                ForPerson("ccc71818-e31b-42f1-b05e-b898dec2433b", 15, "Gender", "Пол", "Пол", FieldType.String),
                ForPerson("99f3beb1-9209-4a9f-b6c1-ef6980b85b7a", 16, "BirthDay", "День рожд.", "День рожд.", FieldType.Byte),
                ForPerson("5fe0693e-0fb5-4fbc-8a6a-3ed903528ff9", 17, "BirthMonth", "Месяц рожд.", "Месяц рожд.", FieldType.Byte),
                ForPerson("4a8776cd-a92d-474e-8d61-fe371b8e779c", 18, "BirthYear", "Год рожд.", "Год рожд.", FieldType.Int32),
                ForPerson("3f471161-2331-4491-938b-b4f67b4af03b", 19, "BirthDate", "Дата рожд.", "Дата рожд.", FieldType.String, true),
                ForPerson("82576f80-a64f-4077-b30b-422cdca577bb", 20, "LastFirstName", "Фамилия Имя", "Фамилия Имя", FieldType.String, true),
                ForPerson("94a487f6-2b55-4315-922e-a78d6d74fc04", 21, "UpperLastFirstName", "ФАМИЛИЯ Имя", "Фамилия Имя", FieldType.String, true),
                ForPerson("a202ad48-82d4-46ac-9653-e89335a07019", 22, "FirstLastName", "Имя Фамилия", "Имя", FieldType.String, true),
                ForPerson("f969c41d-05b8-429e-87fa-39ed922ae9bf", 23, "FirstUpperLastName", "Имя ФАМИЛИЯ", "Имя", FieldType.String, true),

                ForParticipant("137f2d5d-6e32-4904-aaed-93cfd85778de", 25, "Bib", "Ст. №", "Ст. №", FieldType.String),

                ForParticipantField("45dd06a2-147c-46fc-bf3a-cc741c5e2b26", 30, "City", "Город", "Город", FieldType.City),
                ForParticipantField("c0c51d02-6c94-4fb7-87d3-b134484e9bc5", 31, "Region", "Территория", "Территория", FieldType.Region),
                ForParticipantField("cbdc2c81-4328-4829-b0ea-8bb1350dc16d", 32, "Region2", "Территория (параллельный зачёт)", "Территория (параллельный зачёт)", FieldType.Region),
                ForParticipantField("4e2c2a7f-ebdf-447e-bbb2-109164291187", 33, "Country", "Страна", "Страна", FieldType.Country),
                ForParticipantField("e7d236e2-b255-4926-b7e7-1957c6d28b8d", 34, "MoscowDistrict", "Округ Москвы", "Округ Москвы", FieldType.MoscowDistrict),
                ForParticipantField("b8c4a338-d43d-4ff5-b277-e680075ee124", 35, "Team", "Команда", "Команда", FieldType.Team),
                ForParticipantField("6e424020-664f-4240-b18f-420d24953513", 36, "SportsCategory", "Спорт. разряд", "Спорт. разряд", FieldType.SportsCategory),
                ForParticipantField("a656cce0-64e8-44ae-813d-6d660948c815", 37, "Coaches", "Тренер(ы)", "Тренер(ы)", FieldType.Coaches),
                ForParticipantField("22fcb885-9062-4fee-9c91-a616aba6f767", 38, "Phone", "Телефон", "Телефон", FieldType.Phone),
                ForParticipantField("8e37507f-5eaa-4f89-b7a7-292a7b928de6", 39, "Email", "Email", "Email", FieldType.Email),

                ForParticipantRanking("baa3ca4b-844b-4e9e-9461-55ac1f00f0dc", 70, "Age", "Возраст", "Возраст", FieldType.Byte),
                ForParticipantRanking("324dfc91-27d1-43f9-93d1-b360a77e8c91", 71, "ResultText", "Результат", "Результат", FieldType.String),
                ForParticipantRanking("7d32a8ab-63fc-4b98-a6fd-6970110c0720", 72, "Status", "Статус", "Статус", FieldType.String),
                ForParticipantRanking("ec5ca2fe-bc90-4a4c-9e17-099e74421ad7", 73, "Rank", "Место", "Место", FieldType.String),
                ForParticipantRanking("c3fb1444-2a4f-49c8-a9b8-e196257f7f8d", 74, "RankComment", "Место (прим)", "Место (прим)", FieldType.String),
                ForParticipantRanking("f3af0c51-6802-4ebc-9310-f075986c6f2b", 75, "RankOrStatus", "Место", "Место", FieldType.String, true),

                new Field { Id = new Guid("451f9557-5919-46db-95b4-65f8a7ccb6ce"), SortOrder = 90, Key = "Comment", Name = "Примечание", Title = "Примечание", Type = FieldType.String },
            };

            var existingFields = await db.Fields.Select(x => x.Id).ToArrayAsync();

            var added = fields
                .Where(x => !existingFields.Contains(x.Id))
                .Count(x =>
                {
                    db.Entry(x).State = EntityState.Added;
                    return true;
                });

            var updated = fields
                .Where(x => existingFields.Contains(x.Id))
                .Count(x =>
                {
                    db.Entry(x).State = EntityState.Modified;
                    return true;
                });

            await db.SaveChangesAsync();

            logger.LogInformation($"{nameof(RestoreDefaultFields)}: {added} added, {updated} overwritten.");
        }

        private static Field ForPerson(string id, byte sortOrder, string key, string name, string title, FieldType type, bool isComputed = false)
        {
            return new Field
            {
                Id = Guid.Parse(id),
                SortOrder = sortOrder,
                Key = key,
                Name = name,
                Title = title,
                Type = type,
                Source = FieldSource.Person,
                IsComputed = isComputed,
            };
        }

        private static Field ForParticipant(string id, byte sortOrder, string key, string name, string title, FieldType type, bool isComputed = false)
        {
            return new Field
            {
                Id = Guid.Parse(id),
                SortOrder = sortOrder,
                Key = key,
                Name = name,
                Title = title,
                Type = type,
                Source = FieldSource.Participant,
                IsComputed = isComputed,
            };
        }

        private static Field ForParticipantField(string id, byte sortOrder, string key, string name, string title, FieldType type, bool isComputed = false)
        {
            return new Field
            {
                Id = Guid.Parse(id),
                SortOrder = sortOrder,
                Key = key,
                Name = name,
                Title = title,
                Type = type,
                Source = FieldSource.ParticipantField,
                IsComputed = isComputed,
            };
        }

        private static Field ForParticipantRanking(string id, byte sortOrder, string key, string name, string title, FieldType type, bool isComputed = false)
        {
            return new Field
            {
                Id = Guid.Parse(id),
                SortOrder = sortOrder,
                Key = key,
                Name = name,
                Title = title,
                Type = type,
                Source = FieldSource.ParticipantRanking,
                IsComputed = isComputed,
            };
        }
    }
}

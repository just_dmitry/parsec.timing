﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Model;

    public partial class TimingDb : DbContext
    {
        public TimingDb(DbContextOptions<TimingDb> options)
            : base(options)
        {
            // Nothing
        }

        //// public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<AgeGroup> AgeGroups { get; set; }

        public DbSet<AgeGroupSet> AgeGroupSets { get; set; }

        public DbSet<AgeGroupSetContent> AgeGroupSetContents { get; set; }

        public DbSet<CategoryFlag> CategoryFlags { get; set; }

        public DbSet<Checkpoint> Checkpoints { get; set; }

        public DbSet<Competition> Competitions { get; set; }

        public DbSet<CompetitionPool> CompetitionPools { get; set; }

        public DbSet<CompetitionPoolError> CompetitionPoolErrors { get; set; }

        public DbSet<Field> Fields { get; set; }

        public DbSet<FinGroup> FinGroups { get; set; }

        public DbSet<FinMark> FinMarks { get; set; }

        public DbSet<FinAdjustment> FinAdjustments { get; set; }

        public DbSet<MultiEventTournament> MultiEventTournaments { get; set; }

        public DbSet<MultiEventTournamentField> MultiEventTournamentFields { get; set; }

        public DbSet<Participant> Participants { get; set; }

        public DbSet<ParticipantAgeRanking> ParticipantAgeRankings { get; set; }

        public DbSet<ParticipantField> ParticipantFields { get; set; }

        public DbSet<ParticipantOverride> ParticipantOverrides { get; set; }

        public DbSet<ParticipantRanking> ParticipantRankings { get; set; }

        public DbSet<ParticipantSplitAgeRanking> ParticipantSplitAgeRankings { get; set; }

        public DbSet<ParticipantSplitRanking> ParticipantSplitRankings { get; set; }

        public DbSet<ParticipantStatus> ParticipantStatuses { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Report> Reports { get; set; }

        public DbSet<ReportField> ReportFields { get; set; }

        public DbSet<Result> Results { get; set; }

        public DbSet<ResultOverride> ResultOverrides { get; set; }

        public DbSet<ServicingTeam> ServicingTeams { get; set; }

        public DbSet<Split> Splits { get; set; }

        public DbSet<Tournament> Tournaments { get; set; }

        public DbSet<TournamentCompetition> TournamentCompetitions { get; set; }

        public DbSet<TournamentCompetitionSplit> TournamentCompetitionSplits { get; set; }

        public DbSet<TournamentOfficial> TournamentOfficials { get; set; }

        public DbSet<TournamentSeries> TournamentSeries { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ModelSettings.OnModelCreating(modelBuilder);
        }
    }
}

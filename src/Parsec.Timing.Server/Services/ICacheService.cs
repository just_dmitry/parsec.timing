﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Threading.Tasks;
    using Parsec.Timing.Server.Dto;

    public interface ICacheService
    {
        Task<CompetitionPoolFinishInfo> GetCompetitionPoolFinishInfoAsync(Guid competitionPoolId);

        Task<MultiEventTournamentInfo> GetMultiEventTournamentInfoAsync(Guid multiEventTournamentId);

        void RemoveCompetitionPoolFinishInfo(Guid competitionPoolId);

        void RemoveMultiEventTournamentInfo(Guid multiEventTournamentId);

        bool TryGetCompetitionPoolFinishInfo(Guid competitionPoolId, out CompetitionPoolFinishInfo info);

        bool TryGetMultiEventTournamentInfo(Guid multiEventTournamentId, out MultiEventTournamentInfo info);
    }
}
﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICurrentTournamentService
    {
        Guid Id { get; }

        bool IsLocked { get; }

        bool IsSet { get; }

        string Title { get; }

        IReadOnlyList<Guid> Tournaments { get; }

        IReadOnlyList<Guid> CompetitionPools { get; }

        IReadOnlyList<Guid> Competitions { get; }

        IReadOnlyList<Guid> Checkpoints { get; }

        /// <summary>
        /// Reset "current" tournament
        /// </summary>
        void Reset();

        /// <summary>
        /// Set "current" tournament
        /// </summary>
        /// <param name="id">MultiEventTournament.Id</param>
        /// <param name="db">DbContext</param>
        /// <returns><b>true</b> if tournament found and set as "current", <b>false</b> if tournament does not exist.</returns>
        Task<bool> SetAsync(Guid id, TimingDb db);
    }
}
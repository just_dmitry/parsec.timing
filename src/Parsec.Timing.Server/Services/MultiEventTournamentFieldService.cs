﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Model;

    public class MultiEventTournamentFieldService : IMultiEventTournamentFieldService
    {
        private readonly TimingDb db;

        public MultiEventTournamentFieldService(TimingDb db)
        {
            this.db = db;
        }

        public async Task SetAsync(Guid multiEventTournamentId, Guid[] fields)
        {
            var existing = await db.MultiEventTournamentFields
                .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                .ToListAsync();

            // hide deleted
            foreach (var item in existing)
            {
                if (!fields.Contains(item.FieldId))
                {
                    item.IsUsed = false;
                }
            }

            var newList = new List<MultiEventTournamentField>(fields.Length);
            for (byte i = 0; i < fields.Length; i++)
            {
                var field = existing.FirstOrDefault(x => x.FieldId == fields[i]);
                if (field == null)
                {
                    field = new MultiEventTournamentField
                    {
                        MultiEventTournamentId = multiEventTournamentId,
                        FieldId = fields[i],
                    };
                    db.MultiEventTournamentFields.Add(field);
                }

                field.IsUsed = true;
                field.SortOrder = i;
                newList.Add(field);
            }

            await db.SaveChangesAsync();
        }
    }
}

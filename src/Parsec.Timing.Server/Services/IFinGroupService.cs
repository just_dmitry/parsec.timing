﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Threading.Tasks;
    using Parsec.Timing.Model;

    public interface IFinGroupService
    {
        /// <summary>
        /// Проверяет указанный <see cref="FinGroup"/> на правильность заполнения ссылок
        /// (<see cref="CompetitionPool"/> и <see cref="Checkpoint"/>, если заполнены, должны относится к текущему <see cref="MultiEventTournament"/>)
        /// </summary>
        /// <param name="finGroup">Объект для проверки</param>
        /// <returns><b>True</b> если проверка прошла успешно, <b>false</b> если неуспешно</returns>
        bool ValidateKeys(FinGroup finGroup);

        /// <summary>
        /// Обновляет <see cref="FinMark.RevisedResult"/> у всех отсечек указанной <see cref="FinGroup"/>
        /// </summary>
        /// <param name="group">Файл результатов</param>
        /// <param name="db">Экземпляр DbContext</param>
        /// <returns>Task</returns>
        Task<int> UpdateMarksRevisedResult(FinGroup group, TimingDb db);
    }
}

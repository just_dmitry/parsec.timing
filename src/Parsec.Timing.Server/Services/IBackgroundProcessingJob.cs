﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Threading.Tasks;

    public interface IBackgroundProcessingJob
    {
        /// <summary>
        /// Отображаемое название процесса
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Прогресс (в процентах, от 0 до 100)
        /// </summary>
        byte Progress { get; }

        Task RunAsync(IServiceProvider serviceProvider);
    }
}

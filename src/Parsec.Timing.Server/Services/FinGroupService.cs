﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Model;

    public class FinGroupService : IFinGroupService
    {
        private readonly ICurrentTournamentService currentTournamentService;

        private readonly ILogger logger;

        public FinGroupService(
            ICurrentTournamentService currentTournamentService,
            ILogger<FinGroupService> logger)
        {
            this.currentTournamentService = currentTournamentService;
            this.logger = logger;
        }

        public bool ValidateKeys(FinGroup finGroup)
        {
            if (finGroup == null)
            {
                return false;
            }

            if (!currentTournamentService.IsSet)
            {
                return false;
            }

            if (finGroup.CompetitionPoolId.HasValue
                && !currentTournamentService.CompetitionPools.Any(x => x == finGroup.CompetitionPoolId))
            {
                return false;
            }

            if (finGroup.CheckpointId.HasValue
                && !currentTournamentService.Checkpoints.Any(x => x == finGroup.CheckpointId))
            {
                return false;
            }

            return true;
        }

        public async Task<int> UpdateMarksRevisedResult(FinGroup group, TimingDb db)
        {
            logger.LogDebug($"UpdateMarksRevisedResult() for FinGroup {group.Id} started.");

            var lastId = long.MinValue;
            var count = 0;

            while (true)
            {
                var marks = await db.FinMarks
                    .Where(x => x.FinGroupId == group.Id)
                    .Where(x => x.ResultType == group.ResultShiftType)
                    .Where(x => x.Id > lastId)
                    .OrderBy(x => x.Id)
                    .Take(1000)
                    .ToListAsync();

                if (marks.Count == 0)
                {
                    break;
                }

                foreach (var m in marks)
                {
                    m.RevisedResultValue = m.OriginalResultValue + group.ResultShiftValue;
                    lastId = m.Id;
                }

                count += await db.SaveChangesAsync();
                logger.LogError($"  {count} processed");
            }

            logger.LogDebug($"UpdateMarksRevisedResult() for FinGroup {group.Id} completed.");
            return count;
        }
    }
}

﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Parsec.Timing.Server.Dto;

    public class CacheService : ICacheService
    {
        private const string MultiEventTournamentNameFormat = "CACHE:MultiEventTournamentInfo:{0}";

        private const string CompetitionPoolFinishNameFormat = "CACHE:CompetitionPoolFinishInfo:{0}";

        private readonly IMemoryCache memoryCache;

        private readonly IServiceProvider serviceProvider;

        private readonly ILogger logger;

        private readonly ISet<Guid> cachedCompetitionPools = new HashSet<Guid>();

        public CacheService(IMemoryCache memoryCache, IServiceProvider serviceProvider, ILogger<CacheService> logger)
        {
            this.memoryCache = memoryCache;
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public Task<MultiEventTournamentInfo> GetMultiEventTournamentInfoAsync(Guid multiEventTournamentId)
        {
            var key = string.Format(MultiEventTournamentNameFormat, multiEventTournamentId);
            return memoryCache.GetOrCreateAsync(key, (x) =>
            {
                x.SlidingExpiration = TimeSpan.FromHours(1);
                x.Priority = CacheItemPriority.High;
                return CreateMultiEventTournamentInfoAsync(multiEventTournamentId);
            });
        }

        public bool TryGetMultiEventTournamentInfo(Guid multiEventTournamentId, out MultiEventTournamentInfo info)
        {
            var key = string.Format(MultiEventTournamentNameFormat, multiEventTournamentId);
            return memoryCache.TryGetValue(key, out info);
        }

        public void RemoveMultiEventTournamentInfo(Guid multiEventTournamentId)
        {
            foreach (var poolId in cachedCompetitionPools)
            {
                RemoveCompetitionPoolFinishInfo(poolId);
            }

            cachedCompetitionPools.Clear();

            var key = string.Format(MultiEventTournamentNameFormat, multiEventTournamentId);
            memoryCache.Remove(key);
            logger.LogDebug($"{key} removed");
        }

        public Task<CompetitionPoolFinishInfo> GetCompetitionPoolFinishInfoAsync(Guid competitionPoolId)
        {
            var key = string.Format(CompetitionPoolFinishNameFormat, competitionPoolId);
            return memoryCache.GetOrCreateAsync(key, (x) =>
            {
                x.SlidingExpiration = TimeSpan.FromMinutes(15);
                x.Priority = CacheItemPriority.High;
                cachedCompetitionPools.Add(competitionPoolId);
                return CreateCompetitionPoolFinishInfoAsync(competitionPoolId);
            });
        }

        public bool TryGetCompetitionPoolFinishInfo(Guid competitionPoolId, out CompetitionPoolFinishInfo info)
        {
            var key = string.Format(CompetitionPoolFinishNameFormat, competitionPoolId);
            return memoryCache.TryGetValue(key, out info);
        }

        public void RemoveCompetitionPoolFinishInfo(Guid competitionPoolId)
        {
            var key = string.Format(CompetitionPoolFinishNameFormat, competitionPoolId);
            memoryCache.Remove(key);
            logger.LogDebug($"{key} removed");
        }

        private async Task<MultiEventTournamentInfo> CreateMultiEventTournamentInfoAsync(Guid multiEventTournamentId)
        {
            var model = new MultiEventTournamentInfo();

            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<TimingDb>();

                model.MultiEventTournament = await db.MultiEventTournaments
                    .AsNoTracking()
                    .SingleAsync(x => x.Id == multiEventTournamentId);

                model.CompetitionPools = await db.CompetitionPools
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                    .OrderBy(x => x.SortOrder)
                    .ToListAsync();

                model.Competitions = await db.Competitions
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                    .OrderBy(x => x.SortOrder)
                    .ToListAsync();

                model.TournamentCompetitions = await db.TournamentCompetitions
                    .Include(x => x.Tournament)
                    .AsNoTracking()
                    .Where(x => x.Tournament.MultiEventTournamentId == multiEventTournamentId)
                    .OrderByDescending(x => x.Tournament.IsMain)
                    .ThenBy(x => x.Tournament.Name)
                    .ToListAsync();

                model.FieldTitles = await db.MultiEventTournamentFields
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId && x.IsUsed)
                    .OrderBy(x => x.SortOrder)
                    .Select(x => x.Field.Title)
                    .ToArrayAsync();

                model.Reports = await db.Reports
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                    .OrderBy(x => x.Name)
                    .ToListAsync();

                model.AgeGroupSetNames = await db.AgeGroupSets
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                    .OrderBy(x => x.Name)
                    .Select(x => x.Name)
                    .ToArrayAsync();

                model.Checkpoints = await db.Checkpoints
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                    .OrderBy(x => x.SortOrder)
                    .ThenBy(x => x.Name)
                    .ToListAsync();

                model.Categories = await db.CategoryFlags
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == multiEventTournamentId)
                    .OrderBy(x => x.Id)
                    .ToListAsync();

                model.UnusedFinGroups = await db.FinGroups
                    .AsNoTracking()
                    .Where(x => x.CheckpointId == null || x.CompetitionPoolId == null)
                    .ToListAsync();

            }

            logger.LogInformation($"{nameof(CreateMultiEventTournamentInfoAsync)} for #{multiEventTournamentId} done.");

            return model;
        }

        private async Task<CompetitionPoolFinishInfo> CreateCompetitionPoolFinishInfoAsync(Guid competitionPoolId)
        {
            var model = new CompetitionPoolFinishInfo();

            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<TimingDb>();

                model.CompetitionPool = await db.CompetitionPools.AsNoTracking()
                    .SingleAsync(x => x.Id == competitionPoolId);

                var metId = model.CompetitionPool.MultiEventTournamentId;

                model.Checkpoints = await db.Checkpoints
                    .AsNoTracking()
                    .Where(x => x.MultiEventTournamentId == metId)
                    .OrderBy(x => x.SortOrder)
                    .ThenBy(x => x.Name)
                    .ToListAsync();

                model.FinGroups = await db.FinGroups
                    .AsNoTracking()
                    .Where(x => x.CompetitionPoolId == competitionPoolId)
                    .OrderBy(x => x.Name)
                    .ToListAsync();
            }

            logger.LogInformation($"{nameof(CreateCompetitionPoolFinishInfoAsync)} for #{competitionPoolId} done.");

            return model;
        }
    }
}

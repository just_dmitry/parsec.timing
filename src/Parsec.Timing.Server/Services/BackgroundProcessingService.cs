﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using RecurrentTasks;

    public class BackgroundProcessingService : IRunnable
    {
        private readonly List<IBackgroundProcessingJob> items = new List<IBackgroundProcessingJob>();

        private readonly List<Tuple<DateTimeOffset, string, bool>> processedItems = new List<Tuple<DateTimeOffset, string, bool>>(11);

        private readonly ILogger logger;

        private readonly IServiceScopeFactory serviceScopeFactory;

        private readonly ITask<BackgroundProcessingService> self;

        public BackgroundProcessingService(
            ILogger<BackgroundProcessingService> logger,
            IServiceScopeFactory serviceScopeFactory,
            ITask<BackgroundProcessingService> self)
        {
            this.logger = logger;
            this.serviceScopeFactory = serviceScopeFactory;
            this.self = self;
        }

        public IReadOnlyList<IBackgroundProcessingJob> Items
        {
            get
            {
                return items.AsReadOnly();
            }
        }

        public IReadOnlyList<Tuple<DateTimeOffset, string, bool>> ProcessedItems
        {
            get
            {
                return processedItems.AsReadOnly();
            }
        }

        public void Add(IBackgroundProcessingJob item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            items.Add(item);

            if (self.IsStarted)
            {
                self.TryRunImmediately();
            }
            else
            {
                self.Start(TimeSpan.Zero);
            }
        }

        public void Run(ITask currentTask, CancellationToken cancellationToken)
        {
            while (items.Count > 0)
            {
                var item = items[0];
                var success = true;

                try
                {
                    using (var scope = serviceScopeFactory.CreateScope())
                    {
                        item.RunAsync(scope.ServiceProvider).Wait();
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(0, ex, "Exception while running background task");
                    success = false;
                }

                items.RemoveAt(0);

                processedItems.Insert(0, Tuple.Create(DateTimeOffset.Now, item.Name, success));
                if (processedItems.Count > 10)
                {
                    processedItems.RemoveRange(10, processedItems.Count - 10);
                }
            }
        }
    }
}

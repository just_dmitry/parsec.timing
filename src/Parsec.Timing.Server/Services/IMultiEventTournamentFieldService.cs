﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Threading.Tasks;

    public interface IMultiEventTournamentFieldService
    {
        Task SetAsync(Guid multiEventTournamentId, Guid[] fields);
    }
}
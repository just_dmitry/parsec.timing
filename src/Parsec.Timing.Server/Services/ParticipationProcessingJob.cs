﻿namespace Parsec.Timing.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Model;
    using MoreLinq;

    public class ParticipationProcessingJob : IBackgroundProcessingJob
    {
        private ParticipationProcessingJob(Guid id, string name)
        {
            Name = $"Обработка старта «{name}»";
            Progress = 0;
            CompetitionPoolId = id;
        }

        public static int BatchSize { get; set; } = 500;

        public Guid CompetitionPoolId { get; }

        public string Name { get; }

        public byte Progress { get; private set; }

        public static ParticipationProcessingJob Create(CompetitionPool pool)
        {
            return new ParticipationProcessingJob(pool.Id, pool.Name);
        }

        public static ParticipationProcessingJob Create(Guid id, string name)
        {
            return new ParticipationProcessingJob(id, name);
        }

        public async Task RunAsync(IServiceProvider serviceProvider)
        {
            Progress = 0;

            var logger = serviceProvider.GetRequiredService<ILogger<ParticipationProcessingJob>>();
            var db = serviceProvider.GetRequiredService<TimingDb>();

            var pool = await db.CompetitionPools.SingleAsync(x => x.Id == CompetitionPoolId);
            Progress++; // 1

            logger.LogDebug($"Processing {nameof(CompetitionPool)} #{pool.Id} '{pool.Name}'");

            await ClearParticipantsAsync(db, logger);
            Progress++; // 2

            var overrides = await LoadNormalizedOverridesAsync(db);

            var ageGroupSetIds = await db.AgeGroupSets.AsNoTracking()
                .Where(x => x.MultiEventTournamentId == pool.MultiEventTournamentId)
                .Select(x => x.Id)
                .Distinct()
                .ToArrayAsync();

            var ageGroupSetContents = await db.AgeGroupSetContents.AsNoTracking()
                .Include(x => x.AgeGroup)
                .Where(x => ageGroupSetIds.Contains(x.AgeGroupSetId))
                .ToListAsync();

            var competitions = await db.Competitions.AsNoTracking()
                .Where(x => x.CompetitionPoolId == CompetitionPoolId)
                .ToListAsync();

            var categories = await db.CategoryFlags
                .Where(x => x.MultiEventTournamentId == pool.MultiEventTournamentId)
                .Select(x => x.Id)
                .ToArrayAsync();
            Progress++; // 3

            var usedOverrides = new HashSet<Guid>();
            foreach (var competition in competitions)
            {
                await CreateParticipantRankingsAsync(
                    db,
                    competition,
                    categories,
                    overrides,
                    ageGroupSetContents,
                    logger);
                Progress++;
            }

            var warnings = await ReportUnusedOverrides(db, overrides, usedOverrides, logger);
            Progress++;

            pool.ResultProcessingStatus = warnings == 0 ? ProcessingStatus.Success : ProcessingStatus.WithWarnings;
            pool.ResultProcessingTime = DateTimeOffset.Now;
            await db.SaveChangesAsync();
        }

        public async Task<List<ParticipantOverride>> LoadNormalizedOverridesAsync(TimingDb db)
        {
            var list = await db.ParticipantOverrides.AsNoTracking()
                .Where(x => x.CompetitionPoolId == CompetitionPoolId)
                .ToListAsync();

            return list
                .Where(x => !string.IsNullOrEmpty(x.BibString))
                .SelectMany(x => x.BibArray.Select(y =>
                    new ParticipantOverride
                    {
                        Id = Guid.NewGuid(),
                        CompetitionPoolId = x.CompetitionPoolId,
                        TournamentId = x.TournamentId,
                        CompetitionId = x.CompetitionId,
                        Gender = x.Gender,
                        BibString = y,
                        ParticipationOverride = x.ParticipationOverride,
                        AgeOverride = x.AgeOverride,
                        Comment = x.Comment,
                        InsertTime = x.InsertTime,
                    }))
                .Concat(list.Where(x => string.IsNullOrEmpty(x.BibString)))
                .ToList();
        }

        public async Task ClearParticipantsAsync(TimingDb db, ILogger logger)
        {
            var tournamentCompetitionIds = await db.TournamentCompetitions
                .Where(x => x.Competition.CompetitionPoolId == CompetitionPoolId)
                .Select(x => x.Id)
                .ToArrayAsync();

            var queries = new IQueryable[]
            {
                db.CompetitionPoolErrors.AsNoTracking().Where(x => x.CompetitionPoolId == CompetitionPoolId).Where(x => x.Type < ProcessingErrorType.ResultError),
                db.ParticipantSplitAgeRankings.AsNoTracking().Where(x => tournamentCompetitionIds.Contains(x.TournamentCompetitionId)),
                db.ParticipantSplitRankings.AsNoTracking().Where(x => tournamentCompetitionIds.Contains(x.TournamentCompetitionId)),
                db.ParticipantAgeRankings.AsNoTracking().Where(x => tournamentCompetitionIds.Contains(x.TournamentCompetitionId)),
                db.ParticipantRankings.AsNoTracking().Where(x => tournamentCompetitionIds.Contains(x.TournamentCompetitionId)),
            };

            foreach (var query in queries)
            {
                var tableName = db.Model.FindEntityType(query.ElementType.FullName).Relational().TableName;

                logger.LogDebug($"{nameof(ClearParticipantsAsync)}: Clearing: {query.ElementType.Name} from [{tableName}]");

                var tableRowsCount = 0;
                while (true)
                {
                    var items = await query.Cast<object>().Take(BatchSize).ToListAsync();
                    if (items.Count == 0)
                    {
                        break;
                    }

                    db.RemoveRange(items);
                    await db.SaveChangesAsync();

                    Progress++;
                    tableRowsCount += items.Count;
                    logger.LogDebug($"    {tableRowsCount} deleted as far");
                }
            }

            logger.LogDebug($"{nameof(ClearParticipantsAsync)}: All done.");
            Progress = 30;
        }

        public async Task<HashSet<Guid>> CreateParticipantRankingsAsync(
            TimingDb db,
            Competition competition,
            int[] categories,
            List<ParticipantOverride> normalizedOverrides,
            List<AgeGroupSetContent> ageGroupSetContents,
            ILogger logger)
        {
            var usedOverrides = new HashSet<Guid>();

            var participants = await db.Participants.AsNoTracking()
                .Include(x => x.Person)
                .Where(x => x.CompetitionId == competition.Id)
                .Select(x => new
                {
                    x.Id,
                    x.Bib,
                    x.Categories,
                    x.Person.Gender,
                    x.Person.BirthDay,
                    x.Person.BirthMonth,
                    x.Person.BirthYear,
                    x.SortOrder,
                })
                .ToListAsync();

            var tournamentCompetitions = await db.TournamentCompetitions.AsNoTracking()
                .Include(x => x.Tournament)
                .Where(x => x.CompetitionId == competition.Id)
                .ToListAsync();

            foreach (var turnComp in tournamentCompetitions)
            {
                logger.LogDebug($"{nameof(CreateParticipantRankingsAsync)}: Competition {competition.Name} / Tournament {turnComp.Tournament.Name}");

                var rankings = new List<ParticipantRanking>();

                if (normalizedOverrides.Count == 0)
                {
                    rankings = participants
                        .Select(x => new ParticipantRanking
                        {
                            ParticipantId = x.Id,
                            TournamentCompetitionId = turnComp.Id,
                            Age = AgeHelper.ComputeAge(
                                turnComp.AgeGroupReferenceDate,
                                turnComp.AgeGroupByFullBirthDate,
                                x.BirthYear,
                                x.BirthDay,
                                x.BirthMonth),
                            SortOrder = x.SortOrder,
                        })
                        .ToList();
                }
                else
                {
                    foreach (var p in participants)
                    {
                        var add = turnComp.AutoParticipation;
                        var age = AgeHelper.ComputeAge(
                            turnComp.AgeGroupReferenceDate,
                            turnComp.AgeGroupByFullBirthDate,
                            p.BirthYear,
                            p.BirthDay,
                            p.BirthMonth);

                        var matchedOverrides = normalizedOverrides
                            .Where(x => !x.CompetitionId.HasValue || x.CompetitionId.Value == competition.Id)
                            .Where(x => !x.TournamentId.HasValue || turnComp.TournamentId == x.TournamentId.Value)
                            .Where(x => x.BibArray == null || x.BibArray.Contains(p.Bib))
                            .Where(x => !x.Gender.HasValue || x.Gender.Value == p.Gender)
                            .Where(x => x.Category == 0 || ((x.Category & p.Categories) == x.Category))
                            .ToList();

                        foreach (var ovr in matchedOverrides)
                        {
                            if (ovr.ParticipationOverride.HasValue)
                            {
                                add = !turnComp.AutoParticipation;
                            }

                            if (ovr.AgeOverride.HasValue)
                            {
                                age = ovr.AgeOverride.Value;
                            }

                            usedOverrides.Add(ovr.Id);
                        }

                        if (add)
                        {
                            rankings.Add(new ParticipantRanking
                            {
                                ParticipantId = p.Id,
                                TournamentCompetitionId = turnComp.Id,
                                Age = age,
                                SortOrder = p.SortOrder,
                            });
                        }
                    }
                }

                logger.LogDebug($"Generated {rankings.Count} {nameof(ParticipantRanking)}, saving...");

                var batches = rankings.Batch(BatchSize);
                var saveCount = 0;
                foreach (var batch in batches)
                {
                    await db.ParticipantRankings.AddRangeAsync(batch);
                    saveCount += await db.SaveChangesAsync();
                    logger.LogDebug($"    {saveCount} saved OK");
                }

                if (!turnComp.AgeGroupSetId.HasValue)
                {
                    logger.LogDebug($"{nameof(turnComp.AgeGroupSetId)} is null, skipping {nameof(ParticipantAgeRanking)} generation.");
                    continue;
                }

                var groups = ageGroupSetContents
                    .Where(x => x.AgeGroupSetId == turnComp.AgeGroupSetId.Value)
                    .Select(x => x.AgeGroup)
                    .ToList();

                var ageRankings = new List<ParticipantAgeRanking>();
                foreach (var pr in rankings)
                {
                    if (pr.Age == AgeHelper.UnknownAge)
                    {
                        continue;
                    }

                    ageRankings.AddRange(groups
                        .Where(x => x.MinAge <= pr.Age && x.MaxAge >= pr.Age)
                        .Select(x => new ParticipantAgeRanking
                        {
                            ParticipantId = pr.ParticipantId,
                            AgeGroupId = x.Id,
                            TournamentCompetitionId = turnComp.Id
                        }));
                }

                logger.LogDebug($"Generated {ageRankings.Count} {nameof(ParticipantAgeRanking)}, saving...");
                var batches2 = ageRankings.Batch(BatchSize);
                var saveCount2 = 0;
                foreach (var batch in batches2)
                {
                    await db.ParticipantAgeRankings.AddRangeAsync(batch);
                    saveCount2 += await db.SaveChangesAsync();
                    logger.LogDebug($"    {saveCount2} saved OK");
                }
            }

            logger.LogDebug($"{nameof(CreateParticipantRankingsAsync)}: All done.");

            return usedOverrides;
        }

        public async Task<int> ReportUnusedOverrides(
            TimingDb db,
            List<ParticipantOverride> normalizedOverrides,
            HashSet<Guid> usedOverrides,
            ILogger logger)
        {
            var errors = normalizedOverrides
                .Where(x => !string.IsNullOrEmpty(x.BibString))
                .Where(x => !usedOverrides.Contains(x.Id))
                .Select(x => new CompetitionPoolError
                {
                    CompetitionPoolId = CompetitionPoolId,
                    Bib = x.BibString,
                    Type = ProcessingErrorType.ParticipantOverrideBibNotFound
                })
                .ToList();

            if (errors.Count > 0)
            {
                logger.LogDebug($"Generated {errors.Count} {nameof(CompetitionPoolError)}, saving...");
                await db.CompetitionPoolErrors.AddRangeAsync(errors);
                await db.SaveChangesAsync();
            }
            else
            {
                logger.LogDebug($"No {nameof(CompetitionPoolError)} created. Nice.");
            }

            return errors.Count;
        }
    }
}

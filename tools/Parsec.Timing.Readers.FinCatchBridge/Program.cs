﻿namespace Parsec.Timing.Readers.FinCatchBridge
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Parsec.Timing.Readers;

    public static class Program
    {
        private static ServiceProvider serviceProvider;

        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddOptions()
                .AddSingleton<MylapsMessageHandler>()
                .AddSingleton<ReaderListener>()
                .Configure<BridgeSettings>(configuration.GetSection("BridgeSettings"))
                .BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>()
                .AddConsole(LogLevel.Information)
                .AddFile("log.txt", LogLevel.Trace);

            var logger = loggerFactory.CreateLogger(typeof(Program));

            var settings = serviceProvider.GetRequiredService<IOptions<BridgeSettings>>().Value;

            Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            logger.LogInformation($"Ready to connect with {settings.TagMappings.Count} mappings to " + settings.FinCatchConnectionString);

            logger.LogInformation("Запуск слушателя...");

            var handler = serviceProvider.GetRequiredService<MylapsMessageHandler>();
            handler.NewTagVisits += Handler_NewTagVisits;

            var listener = serviceProvider.GetRequiredService<ReaderListener>();
            listener.Encoding = Encoding.GetEncoding(1251);
            listener.NewMessage += handler.HandleMessageAsync;
            listener.Start();

            logger.LogInformation("Всё успешно запущено!");

            var ips = ReaderListener.GetLocalIPs();
            logger.LogInformation(
                "Настройте ридер на отправку данных на порт {0} на один из адресов: {1}",
                listener.Port,
                string.Join(" или ", ips));

            logger.LogInformation("Нажмите ENTER для завершения");
            Console.ReadLine();

            logger.LogDebug("Остановка слушателя...");
            listener.Stop();

            logger.LogInformation("Всё остановлено. Можно просто закрыть данное окно.");
        }

        private static async Task Handler_NewTagVisits(MylapsMessageHandler source, NewTagVisitsEventArgs args)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILoggerFactory>().CreateLogger("Handler");
                var options = scope.ServiceProvider.GetRequiredService<IOptionsSnapshot<BridgeSettings>>().Value;

                using (var conn = new System.Data.SqlClient.SqlConnection(options.FinCatchConnectionString))
                {
                    var groupName = $"CC-{args.Name}";

                    await conn.OpenAsync();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "dbo.Post_Result";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("group", System.Data.SqlDbType.NVarChar);
                        cmd.Parameters.Add("stnum", System.Data.SqlDbType.NVarChar);
                        cmd.Parameters.Add("result", System.Data.SqlDbType.DateTime);

                        cmd.Parameters[0].Value = groupName;

                        foreach (var item in args.TagVisits)
                        {
                            options.TagMappings.TryGetValue(item.Tag, out var bib);
                            var time = item.Time;

                            time = time + options.TimeShift;
                            if (time < TimeSpan.Zero)
                            {
                                time = time.Add(TimeSpan.FromDays(1));
                            }

                            var skip = string.IsNullOrEmpty(bib) || DateTimeOffset.Now < options.SkipBefore;

                            cmd.Parameters["stnum"].Value = bib;
                            cmd.Parameters["result"].Value = new DateTime(1900, 1, 1).Add(time);

                            if (skip)
                            {
                                logger.LogWarning($"SKIP {item.Tag} {item.Time} -> {bib} {time} (group {groupName})");
                            }
                            else
                            {
                                await cmd.ExecuteNonQueryAsync();
                                logger.LogInformation($" OK  {item.Tag} {item.Time} -> {bib} {time}");
                            }
                        }
                    }
                }
            }
        }
    }
}

﻿namespace Parsec.Timing.Readers.FinCatchBridge
{
    using System;
    using System.Collections.Generic;

    public class BridgeSettings
    {
        public string FinCatchConnectionString { get; set; }

        public Dictionary<string, string> TagMappings { get; set; }

        public DateTimeOffset SkipBefore { get; set; } = new DateTimeOffset(2018, 2, 2, 22, 0, 0, TimeSpan.FromHours(3));

        public TimeSpan TimeShift { get; set; } = new TimeSpan(-22, 0, 0);
    }
}

﻿namespace DataTransfer.ParsecDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using MoreLinq;

    public abstract class ConverterBase
    {
        protected abstract string TableName { get; }

        public virtual Task CleanupAsync(SqlConnection destination)
        {
            return DeleteAllFromTableAsync(destination, TableName);
        }

        public abstract Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context);

        protected async Task DeleteAllFromTableAsync(SqlConnection destination, string tableName)
        {
            using (var cmd = destination.CreateCommand())
            {
                cmd.CommandText = $"DELETE FROM [{tableName}]";
                var count = await cmd.ExecuteNonQueryAsync();
                Console.WriteLine($"        [{tableName}] - {count}");
            }
        }

        protected async Task TruncateTableAsync(SqlConnection destination, string tableName)
        {
            using (var cmd = destination.CreateCommand())
            {
                cmd.CommandText = $"TRUNCATE TABLE [{tableName}]";
                var count = await cmd.ExecuteNonQueryAsync();
                Console.WriteLine($"        [{tableName}] - {count}");
            }
        }

        protected async Task BatchImportAsync<T>(SqlConnection destination, string tableName, IEnumerable<T> data, bool keepIdentity = false)
            where T : class
        {
            var opts = SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction;

            if (keepIdentity)
            {
                opts = opts | SqlBulkCopyOptions.KeepIdentity;
            }

            using (var sbc = new SqlBulkCopy(destination, opts, null))
            {
                sbc.DestinationTableName = tableName;
                sbc.BatchSize = 20000;
                sbc.NotifyAfter = 20000;
                sbc.SqlRowsCopied += (s, e) => Console.WriteLine($"        [{tableName}] - ...{e.RowsCopied}");

                var dt = GetDataTableFromFields(data, sbc);
                await sbc.WriteToServerAsync(dt);

                Console.WriteLine($"        [{tableName}] - {dt.Rows.Count}");
            }
        }

        protected DataTable GetDataTableFromFields<T>(IEnumerable<T> data, SqlBulkCopy sqlBulkCopy)
        {
            var dt = new DataTable();
            Type listType = typeof(T);

            foreach (PropertyInfo propertyInfo in listType.GetProperties())
            {
                var columnName = GetColumnName(propertyInfo);
                if (columnName != null)
                {
                    dt.Columns.Add(columnName, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                    sqlBulkCopy.ColumnMappings.Add(columnName, columnName);
                }
            }

            foreach (T value in data)
            {
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo propertyInfo in listType.GetProperties())
                {
                    var columnName = GetColumnName(propertyInfo);
                    if (columnName != null)
                    {
                        dr[columnName] = propertyInfo.GetValue(value, null) ?? DBNull.Value;
                    }
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }

        private string GetColumnName(PropertyInfo propertyInfo)
        {
            if (propertyInfo.GetGetMethod().IsVirtual)
            {
                return null;
            }

            if (propertyInfo.GetCustomAttribute<NotMappedAttribute>(false) != null)
            {
                return null;
            }

            var columnAttribute = propertyInfo.GetCustomAttribute<ColumnAttribute>(false);

            if (columnAttribute != null)
            {
                return columnAttribute.Name;
            }

            return propertyInfo.Name;
        }
    }
}

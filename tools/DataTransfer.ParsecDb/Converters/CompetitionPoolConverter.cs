﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class CompetitionPoolConverter : ConverterBase
    {
        protected override string TableName => "CompetitionPools";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<FinTimer>("SELECT * FROM Fin_Timer ORDER BY ID_Fin_Timer");
            var mapping = list.ToDictionary(x => x.ID_Fin_Timer, x => (Guid)SequentialGuid.NewSequentialGuid());
            var mapping2 = list.ToDictionary(x => x.ID_Fin_Timer, x => (Guid)SequentialGuid.NewSequentialGuid());

            var pools = list
                .Select(x => new CompetitionPool
                {
                    Id = mapping[x.ID_Fin_Timer],
                    MultiEventTournamentId = context.MultiEventTournaments[x.ID_Compet],
                    Name = x.NM_Fin_Timer,
                    SortOrder = 1,
                    IsLocked = false, // true,
                    ParticipationProcessingStatus = ProcessingStatus.Success,
                    ParticipationProcessingTime = x.DT_Process,
                    ResultProcessingStatus = ProcessingStatus.Success,
                    ResultProcessingTime = x.DT_Process,
                })
                .ToDictionary(x => x.Id);

            await BatchImportAsync(destination, TableName, pools.Values);

            context.CompetitionPools = mapping.ToDictionary(x => x.Key, x => pools[x.Value]);
        }

        private class FinTimer
        {
            public int ID_Fin_Timer { get; set; }

            public int ID_Compet { get; set; }

            public string NM_Fin_Timer { get; set; }

            public DateTimeOffset DT_Process { get; set; }

            public string Errors { get; set; }
        }
    }
}

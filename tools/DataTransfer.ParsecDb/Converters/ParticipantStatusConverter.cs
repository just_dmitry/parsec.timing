﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class ParticipantStatusConverter : ConverterBase
    {
        protected override string TableName => "ParticipantStatuses";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<PStatus>("SELECT * FROM S_PSTATUS");
            var mapping = list.ToDictionary(x => x.ID_PStatus, x => (Guid)SequentialGuid.NewSequentialGuid());
            var newList = list
                .Select(x => new ParticipantStatus
                {
                    Id = mapping[x.ID_PStatus],
                    Name = x.NM_PStatus,
                    PublicName = x.SNM_PStatus,
                    PublicNameInternational = x.SNM_PStatus,
                    AssignPlace = x.Assign_Place,
                    IsGlobal = x.IS_Global,
                    KeepResult = x.Keep_Result,
                    SortOrder = x.Sort_Order,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            context.ParticipantStatuses = mapping;
        }

        private class PStatus
        {
            public int ID_PStatus { get; set; }

            public string NM_PStatus { get; set; }

            public string SNM_PStatus { get; set; }

            public bool Assign_Place { get; set; }

            public bool Keep_Result { get; set; }

            public bool IS_Global { get; set; }

            public byte Sort_Order { get; set; }

            public bool IS_Active { get; set; }

            public bool IS_Always_Written { get; set; }

            public bool IS_BKK { get; set; }

            public string Comm { get; set; }
        }
    }
}

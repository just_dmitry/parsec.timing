﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ParticipantAgeRankingConverter : ConverterBase
    {
        protected override string TableName => "ParticipantAgeRankings";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<StandingAG>("SELECT * FROM STANDING_AG");

            var newList = list
                .Select(x => new ParticipantAgeRanking
                {
                    ParticipantId = context.ParticipantRanking[x.ID_Standing].Item1,
                    TournamentCompetitionId = context.ParticipantRanking[x.ID_Standing].Item2,
                    AgeGroupId = context.AgeGroups[x.ID_AGItem],
                    Rank = x.Place,
                })
                .OrderBy(x => x.TournamentCompetitionId)
                .ToList();

            await BatchImportAsync(destination, TableName, newList);
        }

        private class StandingAG
        {
            public Guid ID_Standing { get; set; }

            public int ID_AGItem { get; set; }

            public string Place { get; set; }
        }
    }
}

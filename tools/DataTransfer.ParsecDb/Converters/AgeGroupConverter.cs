﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class AgeGroupConverter : ConverterBase
    {
        protected override string TableName => throw new NotImplementedException();

        public override async Task CleanupAsync(SqlConnection destination)
        {
            await DeleteAllFromTableAsync(destination, "AgeGroupSetContents");
            await DeleteAllFromTableAsync(destination, "AgeGroups");
            await DeleteAllFromTableAsync(destination, "AgeGroupSets");
        }

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            await ConvertAgeGroups(source, destination, context);
            await ConvertAgeGroupSets(source, destination, context);
            await ConvertAgeGroupSetContents(source, destination, context);
        }

        public async Task ConvertAgeGroups(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var mapping = new Dictionary<int, Guid>();
            var created = new List<AgeGroup>();

            var list = await source.QueryAsync<AgeGroupItem>("SELECT * FROM S_AGITEM");
            foreach (var item in list)
            {
                var existing = created
                    .Where(x => x.Name == item.NM_AGItem && x.ShortName == item.SNM_AGItem)
                    .Where(x => x.MinAge == item.Min_Age && x.MaxAge == item.Max_Age)
                    .FirstOrDefault();
                if (existing == null)
                {
                    var aGroup = new AgeGroup
                    {
                        Id = SequentialGuid.NewSequentialGuid(),
                        Name = item.NM_AGItem,
                        ShortName = item.SNM_AGItem,
                        MinAge = (byte)item.Min_Age,
                        MaxAge = (byte)(item.Max_Age == 1000 ? 255 : item.Max_Age),
                    };
                    created.Add(aGroup);
                    mapping.Add(item.ID_AGItem, aGroup.Id);
                }
                else
                {
                    mapping.Add(item.ID_AGItem, existing.Id);
                }
            }

            await BatchImportAsync(destination, "AgeGroups", created);

            context.AgeGroups = mapping;
        }

        public async Task ConvertAgeGroupSets(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = (await source.QueryAsync<AGroup>("SELECT * FROM S_AGROUP")).ToDictionary(x => x.ID_AGroup);

            var list2 = await source.QueryAsync<Linking>("SELECT DISTINCT ID_AGroup, ID_Compet FROM RACE INNER JOIN RACE_EVENT ON RACE.ID_Race = RACE_EVENT.ID_Race WHERE ID_AGroup Is Not Null");

            var mapping = list.Keys
                .ToDictionary(x => x, x => list2.Where(y => y.ID_AGroup == x).ToDictionary(z => z.ID_Compet, z => (Guid)SequentialGuid.NewSequentialGuid()));

            var newList = mapping
                .SelectMany(x => x.Value.Select(y => new AgeGroupSet
                {
                    Id = y.Value,
                    Name = list[x.Key].NM_AGroup,
                    MultiEventTournamentId = context.MultiEventTournaments[y.Key],
                }))
                .ToList();

            context.AgeGroupMets = mapping;

            await BatchImportAsync(destination, "AgeGroupSets", newList);
        }

        public async Task ConvertAgeGroupSetContents(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<AgeGroupItem>("SELECT * FROM S_AGITEM");

            var newList = list
                .SelectMany(x => context.AgeGroupMets[x.ID_AGroup].Select(y => new AgeGroupSetContent
                {
                    AgeGroupSetId = y.Value,
                    AgeGroupId = context.AgeGroups[x.ID_AGItem],
                }))
                .ToList();

            await BatchImportAsync(destination, "AgeGroupSetContents", newList);
        }

        private class AgeGroupItem
        {
            public int ID_AGItem { get; set; }

            public int ID_AGroup { get; set; }

            public string NM_AGItem { get; set; }

            public string SNM_AGItem { get; set; }

            public int Min_Age { get; set; }

            public int Max_Age { get; set; }
        }

        private class AGroup
        {
            public int ID_AGroup { get; set; }

            public string NM_AGroup { get; set; }

            public bool Is_Active { get; set; }

            public string Comm { get; set; }
        }

        private class Linking
        {
            public int ID_AGroup { get; set; }

            public int ID_Compet { get; set; }
        }
    }
}

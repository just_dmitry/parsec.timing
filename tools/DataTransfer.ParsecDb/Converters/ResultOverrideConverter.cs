﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ResultOverrideConverter : ConverterBase
    {
        protected override string TableName => "ResultOverrides";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<EventPStatus>("SELECT * FROM EVENT_PSTATUS");

            var newList = list
                .GroupBy(x => new { x.ID_Event, x.ID_PStatus })
                .Select(x => new ResultOverride
                {
                    Id = SequentialGuid.NewSequentialGuid(),
                    CompetitionPoolId = context.Competitions[x.Key.ID_Event].CompetitionPoolId,
                    TournamentId = null,
                    CompetitionId = context.Competitions[x.Key.ID_Event].Id,
                    BibArray = x.Select(y => y.St_Num).ToArray(),
                    StatusIdOverride = context.ParticipantStatuses[x.Key.ID_PStatus],
                    ResultComment = x.Select(y => y.Desc_PStatus).First(),
                    Category = 0,
                })
                .ToList();

            var list2 = await source.QueryAsync<RaceEventPStatus>("SELECT * FROM RACE_EVENT_PSTATUS");

            var newList2 = list2
                .GroupBy(x => new { x.ID_Event, x.ID_Race, x.ID_PStatus })
                .Select(x => new ResultOverride
                {
                    Id = SequentialGuid.NewSequentialGuid(),
                    CompetitionPoolId = context.Competitions[x.Key.ID_Event].CompetitionPoolId,
                    TournamentId = context.Tournaments[x.Key.ID_Race].Id,
                    CompetitionId = context.Competitions[x.Key.ID_Event].Id,
                    BibArray = x.Select(y => y.St_Num).ToArray(),
                    StatusIdOverride = context.ParticipantStatuses[x.Key.ID_PStatus],
                    ResultComment = x.Select(y => y.Desc_PStatus).First(),
                })
                .ToList();

            newList.AddRange(newList2);

            var res = newList
                .SelectMany(x =>
                    x.BibArray.Batch(100, y => new ResultOverride
                    {
                        Id = SequentialGuid.NewSequentialGuid(),
                        CompetitionPoolId = x.CompetitionPoolId,
                        TournamentId = x.TournamentId,
                        CompetitionId = x.CompetitionId,
                        BibArray = y.ToArray(),
                        StatusIdOverride = x.StatusIdOverride,
                        ResultComment = x.ResultComment,
                    }))
                .ToList();

            await BatchImportAsync(destination, TableName, res);
        }

        private class EventPStatus
        {
            public int ID_Event { get; set; }

            public string St_Num { get; set; }

            public int ID_PStatus { get; set; }

            public string Desc_PStatus { get; set; }
        }

        private class RaceEventPStatus
        {
            public int ID_Race { get; set; }

            public int ID_Event { get; set; }

            public string St_Num { get; set; }

            public int ID_PStatus { get; set; }

            public string Desc_PStatus { get; set; }
        }
    }
}

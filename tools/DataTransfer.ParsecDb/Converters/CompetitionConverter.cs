﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class CompetitionConverter : ConverterBase
    {
        protected override string TableName => "Competitions";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<Event>("SELECT * FROM [Event] ORDER BY ID_Event");
            var mapping = list.ToDictionary(x => x.ID_Event, x => (Guid)SequentialGuid.NewSequentialGuid());

            var newList = list
                .Select(x => new Competition
                {
                    Id = mapping[x.ID_Event],
                    Key = x.ID_Event,
                    Name = x.NM_Event,
                    NameInternational = x.INM_Event,
                    MultiEventTournamentId = context.MultiEventTournaments[x.ID_Compet],
                    SortOrder = x.Sort_Order,
                    CompetitionPoolId = context.CompetitionPools[x.ID_Fin_Timer].Id,
                    AllowMen = x.ID_Gender == 1,
                    AllowWomen = x.ID_Gender == 0,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            context.Competitions = newList.ToDictionary(x => x.Key);
        }

        private class Event
        {
            public int ID_Event { get; set; }

            public int ID_Compet { get; set; }

            public string NM_Event { get; set; }

            public string INM_Event { get; set; }

            public int ID_Fin_Timer { get; set; }

            public byte ID_Gender { get; set; }

            public bool IS_Hiddden { get; set; }

            public decimal? Max_Result_NUM { get; set; }

            public DateTime? Max_Result_DT { get; set; }

            public DateTimeOffset? DT_Start_Process { get; set; }

            public DateTimeOffset? DT_Process { get; set; }

            public byte Sort_Order { get; set; }
        }
    }
}
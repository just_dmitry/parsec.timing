﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class CheckpointConverter : ConverterBase
    {
        protected override string TableName => "Checkpoints";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<FinGroup>("SELECT * FROM Fin_Group ORDER BY ID_Fin_Group");
            var mapping = list.ToDictionary(x => x.ID_Fin_Group, x => (Guid)SequentialGuid.NewSequentialGuid());
            var counters = list.Select(x => x.ID_Fin_Timer).Distinct().ToDictionary(x => x, x => (byte)1);

            var getOrder = new Func<int, byte>(x =>
            {
                var val = counters[x];
                counters[x] = (byte)(val + 1);
                return val;
            });

            context.FinGroupToFinTimerMappings = list.ToDictionary(x => x.ID_Fin_Group, x => x.ID_Fin_Timer);

            var items = list
                .Select(x => new Checkpoint
                {
                    Id = mapping[x.ID_Fin_Group],
                    Name = x.NM_Fin_Group,
                    MultiEventTournamentId = context.CompetitionPools[x.ID_Fin_Timer].MultiEventTournamentId,
                    SortOrder = getOrder(x.ID_Fin_Timer),
                })
                .ToList();

            await BatchImportAsync(destination, TableName, items);

            context.CheckpointLocations = mapping;

            /*
            var mapping2 = list.ToDictionary(x => x.ID_Fin_Group, x => (Guid)SequentialGuid.NewSequentialGuid());

            var items2 = list
                .Select(x => new RouteCheckpoint
                {
                    Id = mapping2[x.ID_Fin_Group],
                    RouteId = context.Routes[x.ID_Fin_Timer],
                    CheckpointLocationId = mapping[x.ID_Fin_Timer],
                    SortOrder = context.CompetitionPools[x.ID_Fin_Timer].SortOrder,
                    FullLap = true,
                })
                .ToList();

            var sql2 = $@"
INSERT INTO [RouteCheckpoints]
         ( Id,  RouteId,  CheckpointLocationId,  SortOrder,  FullLap)
  VALUES (@Id, @RouteId, @CheckpointLocationId, @SortOrder, @FullLap)
";
            var count2 = await destination.ExecuteAsync(sql2, items);

            Console.WriteLine($"        [RouteCheckpoints] - {count2}");

            context.RouteCheckpoints = mapping2;
            */
        }

        private class FinGroup
        {
            public int ID_Fin_Group { get; set; }

            public string NM_Fin_Group { get; set; }

            public int ID_Fin_Timer { get; set; }
        }
    }
}

﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class FinFromAccessConverter : ConverterBase
    {
        protected override string TableName => throw new NotImplementedException();

        public override async Task CleanupAsync(SqlConnection destination)
        {
            await DeleteAllFromTableAsync(destination, "FinMarks");
            await DeleteAllFromTableAsync(destination, "FinGroups");
        }

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var files = (await source.QueryAsync<FinFile>("SELECT ff.*, fg.ID_Fin_Timer FROM FIN_FILE ff INNER JOIN FIN_GROUP fg ON ff.ID_Fin_Group = fg.ID_Fin_Group ORDER BY 1")).ToList();

            var groups = files
                .Select(x => new FinGroup
                {
                    Id = x.ID_Fin_File,
                    Name = x.NM_Fin_File,
                    IsDisabled = x.IS_Disabled,
                    Created = x.DT_Ins,
                    LastSeen = x.DT_Ins,
                    CheckpointId = context.CheckpointLocations[x.ID_Fin_Group],
                    CompetitionPoolId = context.CompetitionPools[x.ID_Fin_Timer].Id,
                })
                .ToList();

            await BatchImportAsync(destination, "FinGroups", groups, true);

            var data = (await source.QueryAsync<FinData>("SELECT * FROM FIN_DATA ORDER BY ID_Fin_Data, St_Num")).ToList();
            var marks = data
                 .Select(x => new FinMark
                 {
                     FinGroupId = x.ID_Fin_File,
                     Bib = x.St_Num,
                     OriginalResult = x.Result_DT.HasValue ? new TimeOrLength(x.Result_DT.Value.TimeOfDay) : new TimeOrLength(x.Result_NUM ?? 0),
                     Inserted = DateTimeOffset.Now,
                     Status = FinMarkStatus.Default,
                 })
                 .ToList();

            await BatchImportAsync(destination, "FinMarks", marks);
        }

        private class FinFile
        {
            public int ID_Fin_File { get; set; }

            public string NM_Fin_File { get; set; }

            public int ID_Fin_Group { get; set; }

            public DateTime DT_Ins { get; set; }

            public bool IS_Disabled { get; set; }

            public int ID_Fin_Timer { get; set; }
        }

        private class FinData
        {
            public Guid ID_Fin_Data { get; set; }

            public int ID_Fin_File { get; set; }

            public string St_Num { get; set; }

            public DateTime? Result_DT { get; set; }

            public decimal? Result_NUM { get; set; }

            public bool IS_Disabled { get; set; }

            public bool IS_Manual { get; set; }

            public string Comm { get; set; }

            public long Result_Index { get; set; }
        }
    }
}
﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class MultiEventTournamentConverter : ConverterBase
    {
        protected override string TableName => throw new NotImplementedException();

        public override async Task CleanupAsync(SqlConnection destination)
        {
            await DeleteAllFromTableAsync(destination, "MultiEventTournamentFields");
            await DeleteAllFromTableAsync(destination, "MultiEventTournaments");
        }

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<Compet>("SELECT * FROM Compet ORDER BY ID_Compet");
            var mapping = list.ToDictionary(x => x.ID_Compet, x => (Guid)SequentialGuid.NewSequentialGuid());

            context.MultiEventTournaments = mapping;

            var newList = list
                .Select(x => new MultiEventTournament
                {
                    Id = mapping[x.ID_Compet],
                    Name = x.NM_Compet,
                    StartDate = x.DT_Compet,
                    EndDate = x.DT_Compet,
                    Location = (string)null,
                    Website = (string)null,
                    ServicingTeamId = context.ServicingTeams[x.ID_Computing],
                    IsLocked = false, // true,
                    Comment = "Конвертация из Access", // x.Comm, // always null
                })
                .ToList();

            await BatchImportAsync(destination, "MultiEventTournaments", newList);

            var fields = new[]
            {
                "45dd06a2-147c-46fc-bf3a-cc741c5e2b26", // city
                "c0c51d02-6c94-4fb7-87d3-b134484e9bc5", // region
                "4e2c2a7f-ebdf-447e-bbb2-109164291187", // country
                "b8c4a338-d43d-4ff5-b277-e680075ee124", // team
                "6e424020-664f-4240-b18f-420d24953513", // category
                "451f9557-5919-46db-95b4-65f8a7ccb6ce", // comm
            };

            var fields2 = newList
                .SelectMany(x => fields.Select((y, z) => new MultiEventTournamentField
                {
                    MultiEventTournamentId = x.Id,
                    FieldId = Guid.Parse(y),
                    IsUsed = true,
                    SortOrder = (byte)z,
                }))
                .ToList();

            await BatchImportAsync(destination, "MultiEventTournamentFields", fields2);
        }

        private class Compet
        {
            public int ID_Compet { get; set; }

            public DateTime DT_Compet { get; set; }

            public string NM_Compet { get; set; }

            public bool IS_Locked { get; set; }

            public int ID_Computing { get; set; }

            public string Comm { get; set; }
        }
    }
}

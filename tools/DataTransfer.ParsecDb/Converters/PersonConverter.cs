﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class PersonConverter : ConverterBase
    {
        protected override string TableName => "Persons";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var mapping = new Dictionary<Guid, Guid>();
            var created = new List<Person>();

            var list = await source.QueryAsync<PersonInfo>("SELECT * FROM PERSON_INFO ORDER BY DT_Ins");
            foreach (var item in list)
            {
                var existing = created
                    .Where(x => x.LastName == item.Family && x.FirstName == item.FName && x.MiddleName == item.MName)
                    .Where(x => x.LastNameInternational == item.IFamily && x.FirstNameInternational == item.IFName)
                    .Where(x => x.BirthDay == item.B_Day && x.BirthMonth == item.B_Month && x.BirthYear == item.B_Year)
                    .Where(x => x.Gender == (item.ID_Gender ? Gender.Male : Gender.Female))
                    .FirstOrDefault();
                if (existing == null)
                {
                    var person = new Person
                    {
                        Id = (Guid)SequentialGuid.NewSequentialGuid(),
                        LastName = item.Family,
                        FirstName = item.FName,
                        MiddleName = item.MName,
                        LastNameInternational = item.IFamily,
                        FirstNameInternational = item.IFName,
                        Gender = item.ID_Gender ? Gender.Male : Gender.Female,
                        BirthDay = item.B_Day,
                        BirthMonth = item.B_Month,
                        BirthYear = item.B_Year,
                        InsertTime = new DateTimeOffset(item.DT_Ins, TimeSpan.FromHours(3)),
                    };
                    person.HashValue = person.ComputeHash();
                    created.Add(person);
                    mapping.Add(item.ID_Person_Info, person.Id);
                }
                else
                {
                    mapping.Add(item.ID_Person_Info, existing.Id);
                }
            }

            await BatchImportAsync(destination, TableName, created);

            context.Persons = mapping;
        }

        private class PersonInfo
        {
            public Guid ID_Person_Info { get; set; }

            public string Family { get; set; }

            public string FName { get; set; }

            public string MName { get; set; }

            public string IFamily { get; set; }

            public string IFName { get; set; }

            public bool ID_Gender { get; set; }

            public byte? B_Day { get; set; }

            public byte? B_Month { get; set; }

            public short? B_Year { get; set; }

            public DateTime DT_Ins { get; set; }

            public string Comm { get; set; }
        }
    }
}

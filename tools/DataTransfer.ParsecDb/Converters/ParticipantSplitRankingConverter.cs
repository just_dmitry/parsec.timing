﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class ParticipantSplitRankingConverter : ConverterBase
    {
        protected override string TableName => "ParticipantSplitRankings";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<SubdistStanding>("SELECT * FROM SUBDIST_STANDING ORDER BY ID_Event_Dist");

            var newList = list
                .Select(x => new ParticipantSplitRanking
                {
                    ParticipantId = context.ParticipantRanking[x.ID_Standing].Item1,
                    TournamentCompetitionId = context.ParticipantRanking[x.ID_Standing].Item2,
                    SplitId = context.Splits[x.ID_Event_Dist],
                    StatusId = context.ParticipantStatuses[x.ID_PStatus],
                    ResultText = x.Result_STR,
                    Rank = x.Place,
                    SortOrder = x.Sort_Order,
                    DiffResult = x.Diff_DT.HasValue ? new TimeOrLength(x.Diff_DT.Value.TimeOfDay) : new TimeOrLength(x.Diff_NUM ?? 0),
                    DiffResultText = x.Diff_STR,
                    DiffRank = x.Place_Diff,
                    DiffSortOrder = x.Sort_Order_Diff ?? 0,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);
        }

        private class SubdistStanding
        {
            public Guid ID_Standing { get; set; }

            public int ID_Event_Dist { get; set; }

            public DateTime? Diff_DT { get; set; }

            public decimal? Diff_NUM { get; set; }

            public string Diff_STR { get; set; }

            public string Result_STR { get; set; }

            public int Sort_Order { get; set; }

            public int? Sort_Order_Diff { get; set; }

            public int ID_PStatus { get; set; }

            public string Place { get; set; }

            public string Place_Diff { get; set; }
        }
    }
}

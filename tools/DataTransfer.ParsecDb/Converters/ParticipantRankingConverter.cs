﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ParticipantRankingConverter : ConverterBase
    {
        protected override string TableName => "ParticipantRankings";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = (await source.QueryAsync<Standing>("SELECT * FROM STANDING ORDER BY ID_Race, ID_Event, Sort_Order")).ToList();

            var newList = list
                .Select(x => new ParticipantRanking
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    TournamentCompetitionId = context.TournamentCompetitions[x.ID_Race + "_" + x.ID_Event],
                    Age = (byte)(x.Age <= 0 ? 255 : (x.Age >= 100 ? 255 : x.Age)),
                    StatusId = context.ParticipantStatuses[x.ID_PStatus],
                    FinalSplitId = x.ID_Event_Dist == null ? null : (Guid?)context.Splits[x.ID_Event_Dist.Value],
                    ResultText = x.Result_STR,
                    Rank = x.Place,
                    ResultComment = x.Desc_PStatus,
                    SortOrder = x.Sort_Order,
                })
                .ToList();

            context.ParticipantRanking = list
                .Select(x => new
                {
                    x.ID_Standing,
                    ParticipantId = context.Participants[x.ID_Particip],
                    TournamentCompetitionId = context.TournamentCompetitions[x.ID_Race + "_" + x.ID_Event],
                })
                .ToDictionary(x => x.ID_Standing, x => Tuple.Create(x.ParticipantId, x.TournamentCompetitionId));

            await BatchImportAsync(destination, TableName, newList);
        }

        public class NewStanding
        {
            public long Id { get; set; }

            public Guid ParticipantId { get; set; }

            public Guid TournamentCompetitionId { get; set; }
        }

        private class Standing
        {
            public Guid ID_Standing { get; set; }

            public int ID_Race { get; set; }

            public Guid ID_Particip { get; set; }

            public int ID_Event { get; set; }

            public int Age { get; set; }

            public int ID_PStatus { get; set; }

            public int? ID_Event_Dist { get; set; }

            public string Result_STR { get; set; }

            public string Place { get; set; }

            public string Desc_PStatus { get; set; }

            public int Sort_Order { get; set; }

            public int Score { get; set; }

            public int ID_Sport_Razr { get; set; }
        }
    }
}

﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class TournamentSeriesConverter : ConverterBase
    {
        protected override string TableName => "TournamentSeries";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<TRace>("SELECT * FROM S_TRace");
            var mapping = list.ToDictionary(x => x.ID_TRace, x => (Guid)SequentialGuid.NewSequentialGuid());

            var newList = list
                .Select(x => new TournamentSeries
                {
                    Id = mapping[x.ID_TRace],
                    Name = x.NM_TRace,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            context.TournamentSeries = mapping;
        }

        private class TRace
        {
            public int ID_TRace { get; set; }

            public string NM_TRace { get; set; }
        }
    }
}
﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class SplitConverter : ConverterBase
    {
        protected override string TableName => "Splits";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = (await source.QueryAsync<EventDist>("SELECT ed.*, d.Len_Dist, d.Time_Dist FROM EVENT_DIST ed INNER JOIN S_DIST d on ed.ID_DIST = d.ID_DIST ORDER BY ID_Event_Dist")).ToList();
            var mapping = list.ToDictionary(x => x.ID_Event_Dist, x => (Guid)SequentialGuid.NewSequentialGuid());
            var items = list
                .Select(x => new Split
                {
                    Id = mapping[x.ID_Event_Dist],
                    CompetitionId = context.Competitions[x.ID_Event].Id,
                    Name = x.NM_Event_Dist,
                    NameInternational = x.INM_Event_Dist,
                    SortOrder = x.Sort_Order,
                    Result = string.IsNullOrEmpty(x.Time_Dist)
                        ? new TimeOrLength((decimal)x.Len_Dist)
                        : new TimeOrLength(TimeSpan.Parse(x.Time_Dist)),
                    InstantArrivalCheckpointId = null,
                })
                .ToList();

            var counter = 1;
            foreach (var item in items)
            {
                if (item.Name == ".")
                {
                    item.Name += counter;
                    counter++;
                }
            }

            await BatchImportAsync(destination, TableName, items);

            context.Splits = mapping;
        }

        private class EventDist
        {
            public int ID_Event_Dist { get; set; }

            public int ID_Event { get; set; }

            public int ID_Dist { get; set; }

            public int ID_Fin_Group { get; set; }

            public string NM_Event_Dist { get; set; }

            public string INM_Event_Dist { get; set; }

            public byte Sort_Order { get; set; }

            public byte Shift_Mask { get; set; }

            public int Len_Dist { get; set; }

            public string Time_Dist { get; set; }
        }
    }
}

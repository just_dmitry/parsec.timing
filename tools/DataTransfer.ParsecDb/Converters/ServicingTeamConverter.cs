﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class ServicingTeamConverter : ConverterBase
    {
        protected override string TableName => "ServicingTeams";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<Computing>("SELECT * FROM S_Computing");
            var mapping = list.ToDictionary(x => x.ID_Computing, x => (Guid)SequentialGuid.NewSequentialGuid());
            var newList = list
                .Select(x => new ServicingTeam
                {
                    Id = mapping[x.ID_Computing],
                    Name = x.SNM_Computing,
                    MarketingText = x.NM_Computing,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            context.ServicingTeams = mapping;
        }

        private class Computing
        {
            public int ID_Computing { get; set; }

            public string NM_Computing { get; set; }

            public string SNM_Computing { get; set; }
        }
    }
}

﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class TournamentCompetitionConverter : ConverterBase
    {
        protected override string TableName => "TournamentCompetitions";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var sql1 = @"
SELECT re.*, r.DT_RACE, r.NM_Race, e.NM_Event
  FROM (RACE_EVENT re
    INNER JOIN RACE r ON re.ID_Race = r.ID_Race)
    INNER JOIN [Event] e on re.ID_Event = e.ID_Event
ORDER BY re.ID_Race, re.ID_Event";
            var list = await source.QueryAsync<RaceEvent>(sql1);
            var mapping = list.ToDictionary(x => x.ID_Race + "_" + x.ID_Event, x => (Guid)SequentialGuid.NewSequentialGuid());

            var reverseCompetMetMap = context.MultiEventTournaments.ToDictionary(x => x.Value, x => x.Key);

            var startGuns = list
                .Select(x => new TournamentCompetition
                {
                    Id = mapping[x.ID_Race + "_" + x.ID_Event],
                    TournamentId = context.Tournaments[x.ID_Race].Id,
                    CompetitionId = context.Competitions[x.ID_Event].Id,
                    AutoParticipation = x.Is_Particip_Def,
                    AbsolutePositions = x.Allocate_Places_Abs,
                    AgeGroupSetId = x.ID_AGroup == null
                    ? null :
                    (Guid?)context.AgeGroupMets[x.ID_AGroup.Value][reverseCompetMetMap[context.Tournaments[x.ID_Race].MultiEventTournamentId]],
                    AgeGroupByFullBirthDate = x.IS_AG_By_Date,
                    AgeGroupReferenceDate = x.DT_Race,
                    DefaultParticipantStatusId = context.ParticipantStatuses[x.ID_PStatus_NoFin],
                })
                .ToList();

            await BatchImportAsync(destination, TableName, startGuns);

            context.TournamentCompetitions = mapping;
        }

        private class RaceEvent
        {
            public int ID_Race { get; set; }

            public int ID_Event { get; set; }

            public int? ID_AGroup { get; set; }

            public bool IS_AG_By_Date { get; set; }

            public bool IS_Start_Ok { get; set; }

            public bool IS_Fin_Ok { get; set; }

            public bool Is_Particip_Def { get; set; }

            public int ID_PStatus_NoFin { get; set; }

            public bool Allocate_Places_Abs { get; set; }

            public bool Allocate_Places_AG { get; set; }

            public int? ID_Score_Group { get; set; }

            public int? ID_Score_Group_AG { get; set; }

            public int? ID_S_Razr_Apply_Group { get; set; }

            public DateTime DT_Race { get; set; }

            public string NM_Race { get; set; }

            public string NM_Event { get; set; }

            public string Errors { get; set; }
        }
    }
}

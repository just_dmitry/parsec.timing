﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class TournamentCompetitionSplitConverter : ConverterBase
    {
        protected override string TableName => "TournamentCompetitionSplits";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<RaceEventDist>("SELECT * FROM RACE_EVENT_DIST ORDER BY ID_Race, ID_Event");

            var mapping = list.ToDictionary(x => x.ID_Race + "_" + x.ID_Event + "_" + x.ID_Event_Dist, x => (Guid)SequentialGuid.NewSequentialGuid());

            var newList = list
                .Select(x => new TournamentCompetitionSplit
                {
                    Id = mapping[x.ID_Race + "_" + x.ID_Event + "_" + x.ID_Event_Dist],
                    TournamentCompetitionId = context.TournamentCompetitions[x.ID_Race + "_" + x.ID_Event],
                    SplitId = context.Splits[x.ID_Event_Dist],
                    IsEnabled = x.IS_In_Use,
                    IsFinishable = x.IS_Finishable,
                    IsHidden = x.IS_Hidden,
                    PlacesGroupIndex = x.Fin_Places_Index,
                    AllocateSplitPlacesByLapTime = x.Allocate_Subdist_Places_By_Diff,
                    AllocateSplitPlacesByResult = x.Allocate_Subdist_Places_By_Result,
                    AllocatePlaces = x.Allocate_Places,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            context.TournamentCompetitionSplits = mapping;
        }

        private class RaceEventDist
        {
            public int ID_Event_Dist { get; set; }

            public int ID_Race { get; set; }

            public int ID_Event { get; set; }

            public bool IS_In_Use { get; set; }

            public bool IS_Finishable { get; set; }

            public bool IS_Hidden { get; set; }

            public bool Allocate_Places { get; set; }

            public byte Fin_Places_Index { get; set; }

            public bool Allocate_Subdist_Places_By_Result { get; set; }

            public bool Allocate_Subdist_Places_By_Diff { get; set; }
        }
    }
}

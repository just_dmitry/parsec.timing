﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Parsec.Timing.Model;

    public class TournamentConverter : ConverterBase
    {
        protected override string TableName => throw new NotImplementedException();

        public override async Task CleanupAsync(SqlConnection destination)
        {
            await DeleteAllFromTableAsync(destination, "TournamentOfficials");
            await DeleteAllFromTableAsync(destination, "Tournaments");
        }

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list1 = await source.QueryAsync<Race>("SELECT * FROM Race ORDER BY ID_Race");
            var mapping1 = list1.ToDictionary(x => x.ID_Race, x => (Guid)SequentialGuid.NewSequentialGuid());

            var newList1 = list1
                .Select(x => new Tournament
                {
                    Id = mapping1[x.ID_Race],
                    Name = x.NM_Race,
                    MultiEventTournamentId = context.MultiEventTournaments[x.ID_Compet],
                    TitleLocal = x.Title,
                    TitleInternational = x.ITitle,
                    TournamentSeriesId = x.ID_TRace == null ? (Guid?)null : context.TournamentSeries[x.ID_TRace.Value],
                    IsMain = x.IS_Default,
                })
                .ToList();

            await BatchImportAsync(destination, "Tournaments", newList1);

            var sql2 = $@"
SELECT RACE.ID_Race, 1 AS [Sequence], [Family], [FName], 'Главный судья' AS JobTitle, S_REF_CATEG.NM_Ref_Categ AS RefCateg
FROM S_REF_CATEG RIGHT JOIN (PERSON_INFO RIGHT JOIN RACE ON PERSON_INFO.ID_Person_Info = RACE.ID_Person_Info_MJudje) ON S_REF_CATEG.ID_Ref_Categ = RACE.ID_RCateg_MJudje
UNION ALL
SELECT RACE.ID_Race, 2 AS [Sequence], [Family], [FName], 'Главный секретарь' AS JobTitle, S_REF_CATEG.NM_Ref_Categ AS RefCateg
FROM S_REF_CATEG RIGHT JOIN (PERSON_INFO RIGHT JOIN RACE ON PERSON_INFO.ID_Person_Info = RACE.ID_Person_Info_MSecr) ON S_REF_CATEG.ID_Ref_Categ = RACE.ID_RCateg_MSecr
            ";
            var list2 = await source.QueryAsync<Judge>(sql2);

            var newList2 = list2
                .Select(x => new TournamentOfficial
                {
                    Id = SequentialGuid.NewSequentialGuid(),
                    TournamentId = mapping1[x.ID_Race],
                    SortOrder = x.Sequence,
                    Name = x.Family + " " + x.FName,
                    JobTitle = x.JobTitle,
                    Grade = x.RefCateg,
                })
                .ToList();

            await BatchImportAsync(destination, "TournamentOfficials", newList2);

            var mapping3 = newList1.ToDictionary(x => x.Id);

            context.Tournaments = mapping1.ToDictionary(x => x.Key, x => mapping3[x.Value]);
        }

        private class Race
        {
            public int ID_Race { get; set; }

            public string NM_Race { get; set; }

            public int ID_Compet { get; set; }

            public DateTime DT_Race { get; set; }

            public string Title { get; set; }

            public string ITitle { get; set; }

            public bool IS_Default { get; set; }

            public int? ID_TRace { get; set; }
        }

        private class Judge
        {
            public int ID_Race { get; set; }

            public short Sequence { get; set; }

            public string Family { get; set; }

            public string FName { get; set; }

            public string JobTitle { get; set; }

            public string RefCateg { get; set; }
        }
    }
}
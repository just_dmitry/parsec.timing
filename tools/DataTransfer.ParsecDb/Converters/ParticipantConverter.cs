﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ParticipantConverter : ConverterBase
    {
        protected override string TableName => "Participants";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<Particip>("SELECT * FROM PARTICIP ORDER BY ID_Event");

            var mapping = list.ToDictionary(x => x.ID_Particip, x => (Guid)SequentialGuid.NewSequentialGuid());

            var newList = list
                .Select(x => new Participant
                {
                    Id = mapping[x.ID_Particip],
                    PersonId = context.Persons[x.ID_Person_Info],
                    CompetitionId = context.Competitions[x.ID_Event].Id,
                    Bib = x.St_Num,
                    SortOrder = int.Parse(x.St_Num?.Replace("_", string.Empty).Replace("?", string.Empty) ?? "0"),
                    InsertTime = new DateTimeOffset(x.DT_Insert, TimeSpan.FromHours(3)),
                    Categories = 0,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            context.Participants = mapping;
        }

        private class Particip
        {
            public Guid ID_Particip { get; set; }

            public Guid ID_Person_Info { get; set; }

            public int ID_Event { get; set; }

            public string St_Num { get; set; }

            public DateTime DT_Insert { get; set; }
        }
    }
}

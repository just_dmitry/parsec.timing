﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ParticipantSplitAgeRankingConverter : ConverterBase
    {
        protected override string TableName => "ParticipantSplitAgeRankings";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<SubdistStandingAG>("SELECT * FROM SUBDIST_STANDING_AG ORDER BY ID_Event_Dist");

            var newList = list
                .Select(x => new ParticipantSplitAgeRanking
                {
                    ParticipantId = context.ParticipantRanking[x.ID_Standing].Item1,
                    TournamentCompetitionId = context.ParticipantRanking[x.ID_Standing].Item2,
                    AgeGroupId = context.AgeGroups[x.ID_AGItem],
                    SplitId = context.Splits[x.ID_Event_Dist],
                    Rank = x.Place,
                    DiffRank = x.Place_Diff,
                })
                .OrderBy(x => x.TournamentCompetitionId)
                .ToList();

            await BatchImportAsync(destination, TableName, newList);
        }

        private class SubdistStandingAG
        {
            public Guid ID_Standing { get; set; }

            public int ID_Event_Dist { get; set; }

            public int ID_AGItem { get; set; }

            public string Place { get; set; }

            public string Place_Diff { get; set; }
        }
    }
}

﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ParticipantFieldConverter : ConverterBase
    {
        protected override string TableName => "ParticipantFields";

        public override Task CleanupAsync(SqlConnection destination)
        {
            return TruncateTableAsync(destination, TableName);
        }

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<Particip>("SELECT * FROM PARTICIP");

            var citiesQuery = @"
SELECT
      r.ID_Region AS ID
    , ([r].[NM_Region] & ' ' & [tr].[SNM_TRegion]) AS NM
    , r.ID_Country
FROM S_TREGION AS tr
   INNER JOIN (S_COUNTRY AS c INNER JOIN S_REGION AS r ON c.ID_Country = r.ID_Country)
     ON tr.ID_TRegion = r.ID_TRegion";

            var cities = (await source.QueryAsync<Region>(citiesQuery)).ToDictionary(x => x.ID);

            var countries = (await source.QueryAsync<Country>("SELECT * FROM S_COUNTRY")).ToDictionary(x => x.ID_Country);

            var teams = (await source.QueryAsync<Team>("SELECT * FROM TEAM")).ToDictionary(x => x.ID_Team);

            var sportRazr = (await source.QueryAsync<SportRazr>("SELECT * FROM S_SPORT_RAZR")).ToDictionary(x => x.ID_Sport_Razr);

            var metFields = await destination.QueryAsync<MultiEventTournamentField>("SELECT * FROM [MultiEventTournamentFields]");
            var competitionPools = (await destination.QueryAsync<CompetitionPool>("SELECT * FROM [CompetitionPools]"))
                .ToDictionary(x => x.Id, x => x.MultiEventTournamentId);

            var newList = new List<ParticipantField>(1000);

            var cityFields = metFields.Where(x => x.FieldId == new Guid("45dd06a2-147c-46fc-bf3a-cc741c5e2b26")).ToList();
            newList.AddRange(list
                .Where(x => x.ID_City != null)
                .Select(x => new ParticipantField
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    CompetitionKey = context.Competitions[x.ID_Event].Key,
                    MultiEventTournamentFieldId = cityFields.First(y => y.MultiEventTournamentId == competitionPools[context.Competitions[x.ID_Event].CompetitionPoolId]).Id,
                    Value = cities[x.ID_City.Value].NM.Trim(),
                }));

            var regionFields = metFields.Where(x => x.FieldId == new Guid("c0c51d02-6c94-4fb7-87d3-b134484e9bc5")).ToList();
            newList.AddRange(list
                .Where(x => x.ID_Region != null)
                .Select(x => new ParticipantField
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    CompetitionKey = context.Competitions[x.ID_Event].Key,
                    MultiEventTournamentFieldId = regionFields.First(y => y.MultiEventTournamentId == competitionPools[context.Competitions[x.ID_Event].CompetitionPoolId]).Id,
                    Value = cities[x.ID_Region.Value].NM.Trim(),
                }));

            var countryFields = metFields.Where(x => x.FieldId == new Guid("4e2c2a7f-ebdf-447e-bbb2-109164291187")).ToList();
            newList.AddRange(list
                .Where(x => x.ID_Region != null)
                .Select(x => new ParticipantField
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    CompetitionKey = context.Competitions[x.ID_Event].Key,
                    MultiEventTournamentFieldId = countryFields.First(y => y.MultiEventTournamentId == competitionPools[context.Competitions[x.ID_Event].CompetitionPoolId]).Id,
                    Value = countries[cities[x.ID_Region.Value].ID_Country].SNM_Country,
                }));

            var teamFields = metFields.Where(x => x.FieldId == new Guid("b8c4a338-d43d-4ff5-b277-e680075ee124")).ToList();
            var buildTeamName = new Func<int, int?, int?, string>((idTeam, idCityFor, idCity) =>
            {
                var teamName = teams[idTeam].NM_Team;

                // если уже есть город - заканчиваем
                if (teamName.Contains("("))
                {
                    return teamName;
                }

                var city = idCityFor ?? idCity;
                if (city == null)
                {
                    return teamName;
                }

                return teamName + " (" + cities[city.Value].NM.Trim() + ")";
            });

            newList.AddRange(list
                .Where(x => x.ID_Team != null)
                .Select(x => new ParticipantField
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    CompetitionKey = context.Competitions[x.ID_Event].Key,
                    MultiEventTournamentFieldId = teamFields.First(y => y.MultiEventTournamentId == competitionPools[context.Competitions[x.ID_Event].CompetitionPoolId]).Id,
                    Value = buildTeamName(x.ID_Team.Value, x.ID_City_For, x.ID_City),
                }));

            var sportRazrFields = metFields.Where(x => x.FieldId == new Guid("6e424020-664f-4240-b18f-420d24953513")).ToList();
            newList.AddRange(list
                .Where(x => x.ID_Sport_Razr != null)
                .Select(x => new ParticipantField
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    CompetitionKey = context.Competitions[x.ID_Event].Key,
                    MultiEventTournamentFieldId = sportRazrFields.First(y => y.MultiEventTournamentId == competitionPools[context.Competitions[x.ID_Event].CompetitionPoolId]).Id,
                    Value = sportRazr[x.ID_Sport_Razr.Value].SNM_Sport_Razr,
                }));

            var commentFields = metFields.Where(x => x.FieldId == new Guid("451f9557-5919-46db-95b4-65f8a7ccb6ce")).ToList();
            newList.AddRange(list
                .Where(x => !string.IsNullOrWhiteSpace(x.Comm))
                .Select(x => new ParticipantField
                {
                    ParticipantId = context.Participants[x.ID_Particip],
                    CompetitionKey = context.Competitions[x.ID_Event].Key,
                    MultiEventTournamentFieldId = commentFields.First(y => y.MultiEventTournamentId == competitionPools[context.Competitions[x.ID_Event].CompetitionPoolId]).Id,
                    Value = x.Comm,
                }));

            await BatchImportAsync(destination, TableName, newList);
        }

        private class Particip
        {
            public Guid ID_Particip { get; set; }

            public int ID_Event { get; set; }

            public int? ID_City { get; set; }

            public int? ID_Region { get; set; }

            public int? ID_Team { get; set; }

            public int? ID_City_For { get; set; }

            public int? ID_Sport_Razr { get; set; }

            public string Comm { get; set; }
        }

        private class Region
        {
            public int ID { get; set; }

            public string NM { get; set; }

            public int ID_Country { get; set; }
        }

        private class Country
        {
            public int ID_Country { get; set; }

            public string SNM_Country { get; set; }
        }

        private class Team
        {
            public int ID_Team { get; set; }

            public string NM_Team { get; set; }
        }

        private class SportRazr
        {
            public int ID_Sport_Razr { get; set; }

            public string SNM_Sport_Razr { get; set; }
        }
    }
}

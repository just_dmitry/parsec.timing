﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class FinFromFinCatchConverter : ConverterBase
    {
        protected override string TableName => throw new NotImplementedException();

        public override Task CleanupAsync(SqlConnection destination)
        {
            return Task.CompletedTask;
        }

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var files = (await source.QueryAsync<FinCatchGroup>("SELECT * FROM FIN_GROUP ORDER BY 1")).ToList();

            var finGeneratorMappings = files
                .Where(x => x.ID_Event > 0 && x.ID_Event < 1000)
                .Where(x => x.ID_Event != 422 && x.ID_Event != 423 && x.ID_Event != 424 && x.ID_Event != 425 && x.ID_Event != 426 && x.ID_Event != 427)
                .Where(x => x.ID_Event != 444 && x.ID_Event != 480)
                .ToDictionary(x => x.ID_Fin_Group, x => (Guid)SequentialGuid.NewSequentialGuid());

            const int shift = 2000;

            var groups = files
                .Where(x => finGeneratorMappings.ContainsKey(x.ID_Fin_Group))
                .Select(x => new FinGroup
                {
                    Id = x.ID_Fin_Group + shift,
                    Name = string.IsNullOrEmpty(x.Name) ? "-manual-" : x.Name,
                    Created = x.DT_Insert,
                    LastSeen = x.DT_Insert,
                    IsDisabled = true,
                    CheckpointId = context.CheckpointLocations[x.ID_Event],
                    CompetitionPoolId = context.CompetitionPools[context.FinGroupToFinTimerMappings[x.ID_Event]].Id,
                })
                .ToList();

            await BatchImportAsync(destination, "FinGroups", groups, true);

            var data = (await source.QueryAsync<FinCatchFin>("SELECT * FROM FIN")).ToList();

            var marks = data
                .Where(x => finGeneratorMappings.ContainsKey(x.ID_Fin_Group))
                .Select(x => new FinMark
                {
                    FinGroupId = x.ID_Fin_Group + shift,
                    Bib = x.StNum,
                    OriginalResult = new TimeOrLength(x.Result?.TimeOfDay ?? TimeSpan.Zero),
                    Inserted = DateTimeOffset.Now,
                    Status = FinMarkStatus.Default,
                })
                .ToList();

            await BatchImportAsync(destination, "FinMarks", marks);
        }

        private class FinCatchGroup
        {
            public int ID_Fin_Group { get; set; }

            public int ID_Event { get; set; }

            public string Name { get; set; }

            public DateTime DT_Insert { get; set; }
        }

        private class FinCatchFin
        {
            public long ID_Fin { get; set; }

            public int ID_Fin_Group { get; set; }

            public int ID_Event { get; set; }

            public string StNum { get; set; }

            public DateTime? Result { get; set; }

            public bool IS_Lap { get; set; }

            public bool IS_Copy { get; set; }
        }
    }
}
﻿namespace DataTransfer.ParsecDb.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using MoreLinq;
    using Parsec.Timing.Model;

    public class ParticipantOverrideConverter : ConverterBase
    {
        protected override string TableName => "ParticipantOverrides";

        public override async Task ConvertAsync(IDbConnection source, SqlConnection destination, ConversionContext context)
        {
            var list = await source.QueryAsync<RaceEventExcept>("SELECT * FROM RACE_EVENT_EXCEPT");

            var newList = list
                .ToList()
                .GroupBy(x => new { x.ID_Event, x.ID_Race })
                .Select(x => new ParticipantOverride
                {
                    Id = SequentialGuid.NewSequentialGuid(),
                    CompetitionPoolId = context.Competitions[x.Key.ID_Event].CompetitionPoolId,
                    TournamentId = context.Tournaments[x.Key.ID_Race].Id,
                    CompetitionId = context.Competitions[x.Key.ID_Event].Id,
                    BibArray = x.Select(y => y.St_Num).ToArray(),
                    ParticipationOverride = true,
                    Category = 0,
                })
                .ToList();

            var list2 = await source.QueryAsync<RaceEventAgExcept>("SELECT * FROM RACE_EVENT_AG_EXCEPT");

            var newList2 = list2
                .ToList()
                .GroupBy(x => new { x.ID_Event, x.ID_Race, x.Age })
                .Select(x => new ParticipantOverride
                {
                    Id = SequentialGuid.NewSequentialGuid(),
                    CompetitionPoolId = context.Competitions[x.Key.ID_Event].CompetitionPoolId,
                    TournamentId = context.Tournaments[x.Key.ID_Race].Id,
                    CompetitionId = context.Competitions[x.Key.ID_Event].Id,
                    BibArray = x.Select(y => y.St_Num).ToArray(),
                    AgeOverride = (byte?)x.Key.Age,
                    InsertTime = DateTimeOffset.Now,
                    Category = 0,
                })
                .ToList();

            await BatchImportAsync(destination, TableName, newList);

            newList.AddRange(newList2);
        }

        private class RaceEventExcept
        {
            public int ID_Race { get; set; }

            public int ID_Event { get; set; }

            public string St_Num { get; set; }
        }

        private class RaceEventAgExcept
        {
            public int ID_Race { get; set; }

            public int ID_Event { get; set; }

            public string St_Num { get; set; }

            public int Age { get; set; }
        }
    }
}

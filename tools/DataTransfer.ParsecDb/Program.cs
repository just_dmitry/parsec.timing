﻿namespace DataTransfer.ParsecDb
{
    using System;
    using System.Collections.Generic;
    using System.Data.OleDb;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using DataTransfer.ParsecDb.Converters;
    using Microsoft.Extensions.Configuration;

    public static class Program
    {
        public static void Main(string[] args)
        {
            Run().Wait();
        }

        public static async Task Run()
        {
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Environment.CurrentDirectory)
               .AddJsonFile("appsettings.json")
               .Build();

            var timer = new Stopwatch();
            timer.Start();

            var converters = new List<ConverterBase>()
            {
                new ParticipantStatusConverter(),
                new ServicingTeamConverter(),
                new MultiEventTournamentConverter(),
                new AgeGroupConverter(),
                new TournamentSeriesConverter(),
                new TournamentConverter(),
                new CompetitionPoolConverter(),
                new CompetitionConverter(),
                new TournamentCompetitionConverter(),
                new CheckpointConverter(),
                new SplitConverter(),
                new TournamentCompetitionSplitConverter(),
                new ResultOverrideConverter(),
                new PersonConverter(),
                new ParticipantConverter(),
                new ParticipantFieldConverter(),
                new ParticipantOverrideConverter(),
                new ParticipantRankingConverter(),
                new ParticipantSplitRankingConverter(),
                new ParticipantAgeRankingConverter(),
                new ParticipantSplitAgeRankingConverter(),
                new FinFromAccessConverter(),
                new FinAdjustmentConverter(),
            };

            var converters2 = new List<ConverterBase>()
            {
                new FinFromFinCatchConverter(),
            };

            var source = configuration.GetValue<string>("SourceConnectionString");
            var source2 = configuration.GetValue<string>("SourceConnectionString2");
            var destination = configuration.GetValue<string>("DestinationConnectionString");

            using (var connSource = new OleDbConnection(source))
            {
                using (var connDest = new SqlConnection(destination))
                {
                    Console.WriteLine("Opening source connection...");
                    await connSource.OpenAsync();

                    Console.WriteLine("Opening destination connection...");
                    await connDest.OpenAsync();

                    Console.WriteLine("Connected Ok.");

                    Console.WriteLine();

                    Console.WriteLine("Cleaning DB:");

                    for (var i = converters2.Count - 1; i >= 0; i--)
                    {
                        Console.WriteLine("    " + converters2[i].GetType().Name);
                        await converters2[i].CleanupAsync(connDest);
                    }

                    for (var i = converters.Count - 1; i >= 0; i--)
                    {
                        Console.WriteLine("    " + converters[i].GetType().Name);
                        await converters[i].CleanupAsync(connDest);
                    }

                    Console.WriteLine($"Clean Ok (elapsed {timer.Elapsed})");

                    Console.WriteLine();

                    Console.WriteLine("Moving data:");
                    var context = new ConversionContext();

                    for (var i = 0; i < converters.Count; i++)
                    {
                        Console.WriteLine($"    {converters[i].GetType().Name} ({timer.Elapsed} from start)");
                        await converters[i].ConvertAsync(connSource, connDest, context);
                    }

                    using (var connSource2 = new SqlConnection(source2))
                    {
                        Console.WriteLine("Opening source connection 2...");
                        await connSource2.OpenAsync();

                        for (var i = 0; i < converters2.Count; i++)
                        {
                            Console.WriteLine($"    {converters2[i].GetType().Name} ({timer.Elapsed} from start)");
                            await converters2[i].ConvertAsync(connSource2, connDest, context);
                        }
                    }

                    using (var cmd = connDest.CreateCommand())
                    {
                        Console.WriteLine($"    Updating FinGroups.MarksCount... ({timer.Elapsed} from start)");
                        cmd.CommandText = "update fingroups set MarksCount = (select count(*) from finmarks where fingroupid = fingroups.id)";
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine(timer.Elapsed);
            Console.WriteLine("Done.");
        }
    }
}

﻿namespace DataTransfer.ParsecDb
{
    using System;
    using System.Collections.Generic;
    using Parsec.Timing.Model;

    public class ConversionContext
    {
        public Dictionary<int, Guid> ParticipantStatuses { get; set; }

        public Dictionary<int, Guid> ServicingTeams { get; set; }

        public Dictionary<int, Guid> MultiEventTournaments { get; set; }

        public Dictionary<int, Dictionary<int, Guid>> AgeGroupMets { get; set; }

        public Dictionary<int, Guid> AgeGroups { get; set; }

        public Dictionary<int, Guid> TournamentSeries { get; set; }

        public Dictionary<int, Tournament> Tournaments { get; set; }

        public Dictionary<int, CompetitionPool> CompetitionPools { get; set; }

        public Dictionary<int, Competition> Competitions { get; set; }

        public Dictionary<int, Guid> CheckpointLocations { get; set; }

        public Dictionary<int, int> FinGroupToFinTimerMappings { get; set; }

        public Dictionary<int, Guid> Splits { get; set; }

        public Dictionary<string, Guid> TournamentCompetitions { get; set; }

        public Dictionary<string, Guid> TournamentCompetitionSplits { get; set; }

        public Dictionary<Guid, Guid> Persons { get; set; }

        public Dictionary<Guid, Guid> Participants { get; set; }

        /// <summary>
        /// Key: ID_Standing; Value: {ID_Particip, TournamentCompetitionId}
        /// </summary>
        public Dictionary<Guid, Tuple<Guid, Guid>> ParticipantRanking { get; set; }
    }
}

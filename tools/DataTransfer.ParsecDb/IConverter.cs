﻿namespace DataTransfer.ParsecDb
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;

    public interface IConverter
    {
        Task Cleanup(IDbConnection destination);

        Task Convert(IDbConnection source, IDbConnection destination, ConversionContext context);
    }
}
